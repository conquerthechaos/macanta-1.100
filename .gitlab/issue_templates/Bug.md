<!--- Please provide a clear and concise, general summary of the issue in the Title above -->

### Description
<!--- OPTIONAL - provide a more detailed introduction to the-->
<!--- issue itself, and why you consider it to be a bug -->

### Expected Behavior
<!--- Tell us what should happen -->

### Actual Behavior
<!--- Tell us what happens instead -->

### Steps to Reproduce
<!--- Provide an unambiguous set of steps to reproduce this bug -->
1.
2.
3.
4.

### Possible Fix
<!--- OPTIONAL - suggest a fix or reason for the bug -->

### Context
<!--- OPTIONAL - how has this bug affected you? What were you trying to accomplish? -->

### Documentation
<!--- Screenshots and/or a useloom.com video of you experiencing the bug greatly aid our -->
<!--- ability to diagnose and resolve issues -->

### Your Environment
<!--- Include as many relevant details about the environment in which you experienced the bug -->
* Version used: 
* Browser Name and version: 
* Operating System and version (desktop or mobile): 
* Link to your Macanta login page: 