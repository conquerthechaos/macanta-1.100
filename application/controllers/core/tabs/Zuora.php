<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Zuora extends MY_Controller
{
    private $Contact;
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public function __construct($Prams){
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller

    }

    public function index($Data)
    {

        $Params = array();
        $HTML = $this->load->view('core/tab_sbsm', $Params, true);
        return $HTML;
    }
    public function getAccounts($data){
        $Contacts = $data['Contacts'];
        $ZuoraAccounts = [];
        foreach ($Contacts as $Contact){
            $ZuoraContactId = $Contact['Id'];
            $ZuoraAccounts[$ZuoraContactId]=[];
            $getAccounts = zuora_queryData('BillToId',$ZuoraContactId, "Account");
            foreach ($getAccounts as $getAccount){
                $AccountDetails = $getAccount->getVar();
                $Invoices = zuora_queryData('AccountId', $AccountDetails['Id'],"Invoice");
                $Payments = zuora_queryData('AccountId', $AccountDetails['Id'], "Payment");
                $AccountDetails['Payments']=[];
                foreach ($Payments as $Payment){
                    $PaymentDetails = $Payment->getVar();
                    $InvoicePayments = zuora_queryData('PaymentId', $PaymentDetails['Id'], "InvoicePayment");
                    $InvoicePaymentDetails = $InvoicePayments[0]->getVar();
                    $PaymentDetails['InvoicePayments'] = $InvoicePaymentDetails;
                    $AccountDetails['Payments'][] = $PaymentDetails;
                }
                $AccountDetails['Invoices']=[];
                foreach ($Invoices as $Invoice){
                    $InvoiceDetails = $Invoice->getVar();
                    $InvoiceItems = zuora_queryData('InvoiceId', $InvoiceDetails['Id'], "InvoiceItem");

                    $InvoiceItemDetails = [];
                    foreach ($InvoiceItems as $InvoiceItem){
                        $InvoiceItemVar = $InvoiceItem->getVar();
                        $RatePlanCharge = zuora_queryData('Id', $InvoiceItemVar['RatePlanChargeId'], "RatePlanCharge");
                        $Subscription = zuora_queryData('Id', $InvoiceItemVar['SubscriptionId'], "Subscription");
                        if(sizeof($Subscription) >= 1){
                            $InvoiceItemVar['Subscription'] = $Subscription[0]->getVar();
                        }else{
                            $InvoiceItemVar['Subscription'] = [];
                        }
                        if(sizeof($RatePlanCharge) >= 1){
                            $RatePlanChargeVar = $RatePlanCharge[0]->getVar();
                            $RatePlan = zuora_queryData('Id', $RatePlanChargeVar['RatePlanId'], "RatePlan");
                            if(sizeof($RatePlan) >= 0) {
                                $InvoiceItemVar['RatePlan'] = $RatePlan[0]->getVar();
                            }else{
                                $InvoiceItemVar['RatePlan'] = [];
                            }
                        }else{
                            $RatePlanChargeVar = [];
                        }
                        $InvoiceItemVar['RatePlanCharge'] = $RatePlanChargeVar;
                        $InvoiceItemDetails[] = $InvoiceItemVar;
                    }
                    $InvoiceDetails['InvoiceItems'] = $InvoiceItemDetails;
                    $AccountDetails['Invoices'][] = $InvoiceDetails;
                }
                $ZuoraAccounts[$ZuoraContactId][] = $AccountDetails;
            }
        }
        $this->ajaxResults['ZuoraAccounts'] = $ZuoraAccounts;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }

}
