<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Twilio extends MY_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }
    public function index(){
        // create a test query to rucksack lib to testquickly token expire
    }

    public function clientTwiml(){
        header('Content-type: text/xml');
        $CallerId = $this->config->item('macanta_caller_id');
        if (isset($_GET['From']) || isset($_POST['From'])) {
            $From = isset($_GET['From']) ? $_GET['From'] : $_POST['From'];
            $CallerId = trim($From) != "" ? $From:$CallerId ;
        }
        if (isset($_GET['PhoneNumber']) || isset($_POST['PhoneNumber'])) {
            $request = isset($_GET['PhoneNumber']) ? $_GET['PhoneNumber'] : $_POST['PhoneNumber'];
            $number = htmlspecialchars($request);
            file_put_contents('php://stderr', print_r($number, TRUE));
        } else {
            file_put_contents('php://stderr', print_r("PhoneNumber not set", TRUE));
            // $number="+442476276203";
        }

        if (preg_match("/^[\d\+\-\(\) ]+$/", $number)) {
            $numberOrClient = "<Number>" . $number . "</Number>";
        } else {
            $numberOrClient = "<Client>" . $number . "</Client>";
        }
        echo '<Response><Dial record="record-from-ringing" callerId="'.$CallerId.'">'.$numberOrClient.'</Dial></Response>';
    }

    public function call(){
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        header("Content-Type: text/plain");
        echo "Calling...";

        $client = new Twilio\Rest\Client("AC60a487d1a6b0d138a56d417993809d9c", "1cbe552d416cfe6d1bdac13edf6d1ded");//uk319 credentials
        $CallerId = "+442476276203";
        $FirstDevice = "+639988640885";
        $SecondDevice = "+63949993396";
        $Phone = array(
            "CallerId"  => $CallerId,
            "FirstDevice"    => $FirstDevice,
            "SecondDevice"    => $SecondDevice
        );
        $Param = str_replace("=","",base64_encode(json_encode($Phone)));
        echo $Param."\n";
        //$call = $client->account->calls->create( $CallerId,$FirstDevice, array("url" => "https://uk319.macanta.org/voice/".$Param));
        $call = $client->account->calls->create( $CallerId,$FirstDevice, "https://uk319.macanta.org/voice/".$Param, array(
            "Method" => "GET",
            "StatusCallback" => "https://uk319.macanta.org/events/",
            "StatusCallbackMethod" => "POST",
            "StatusCallbackEvent" => array("queued", "initiated", "ringing", "answered", "completed"),
        ));

        $call = $client->calls->create(
            "+14155551212", "+14158675310",
            array("url" => "http://demo.twilio.com/docs/voice.xml")
        );
        print_r($call);
        //echo $call->sid;
    }

    public function connect($data){
        header('Content-Type: application/json');
        $to = $data["PhoneField"];
        $from = $data["device"];
        $callerId = $data["callerId"];
        $FirstName = $data["FirstName"];
        $LastName = $data["LastName"];
        if($callerId == ""){

            $callerId = $this->config->item('macanta_caller_id');
        }
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');
        $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
        $Phone = array(
            "CallerId"  => $callerId,
            "FirstDevice"    => $from,
            "SecondDevice"    => $to,
            "FirstName"     => $FirstName,
            "LastName"      => $LastName

        );
        $Param = str_replace("=","",base64_encode(json_encode($Phone)));
        $call = $client->calls->create(
            $from, $callerId,
            array(
                "url" => $this->config->item('base_url')."voice/".$Param,
                "method" => "GET",
                "statusCallback" => $this->config->item('base_url')."events/",
                "statusCallbackMethod" => "POST",
                "statusCallbackEvent" =>  array("queued", "ringing", "in-progress", "busy", "failed", "no-answer", "canceled", "answered", "completed")
                )
        );
        $return = array("sid"=>$call->sid);
        echo json_encode($return);
    }

    public function voice($Param){
        header('Content-type: text/xml');
        $Param = json_decode(base64_decode($Param),true);
        echo '<Response><Say voice="alice">Connecting outbound call to '.$Param['FirstName'].' '.$Param['LastName'].'. Please wait.</Say><Dial record="record-from-ringing" callerId="'.$Param['CallerId'].'">'.$Param['SecondDevice'].'</Dial></Response>';
    }
    public function getStatus($data){
        header('Content-Type: application/json');
        $table = "twilio_call";
        $sid = $data["SID"];
        $this->db->where('CallSid',$sid);
        $query = $this->db->get($table);
        $row = $query->row();
        if (isset($row))
        {

            $return = array("status"=>$row->CallStatus, "duration"=>$row->Duration);
        }else{
            $return = array("message"=>"No record for $sid");
        }
        echo json_encode($return);
    }
    public function events(){
        $table = "twilio_call";
        $fields = array(
            'CallSid' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'CallStatus' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'Duration' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            )
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($table, true);

        if(isset($_REQUEST['CallSid'])){
            $sid = $_REQUEST['CallSid'];
            $this->db->where('CallSid',$sid);
            $query = $this->db->get($table);
            $row = $query->row();
            if (isset($row))
            {
                $this->db->where('CallSid',$sid);
                $DBdata['CallStatus'] = $_REQUEST['CallStatus'];
                $DBdata['Duration'] = isset($_REQUEST['CallDuration']) ? $_REQUEST['CallDuration']:0;
                $this->db->update($table, $DBdata);
            }else{
                $DBdata['CallSid'] = $sid;
                $DBdata['CallStatus'] = $_REQUEST['CallStatus'];
                $DBdata['Duration'] = 0;
                $this->db->insert($table, $DBdata);
                }
        }
    }
}
