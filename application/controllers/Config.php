<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Config extends REST_Controller {

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

    }
    public function index_get(){
        $returned = $this->processConfigRequest($this->get());
        $this->response($returned, REST_Controller::HTTP_OK);
    }
    public function index_post(){
        $returned = $this->processConfigRequest($this->post());
        //$this->response($returned, REST_Controller::HTTP_OK); has error
        echo $returned;
    }
    public function processConfigRequest($Request){
        if(isset($Request['action'])){
            if($Request['action'] == 'update'){
                //$Request['data'] should be available
                if(isset($Request['data'])){
                    foreach($Request['data'] as $fieldName => $fieldValue){
                        $update_data[] = array($fieldName => $fieldValue);
                    }
                }else{
                    return 'Update Failed : Data required!';
                }
                $success = $this->Siteconfig->update_config($update_data);
                if($success){
                    return 'Update Success!';
                }else{
                    return 'Update Failed!';
                }
            }

            if($Request['action'] == 'get'){
                if(isset($Request['item'])){
                    $configItem =  $this->config->item($Request['item']);
                    return $configItem;
                }else{
                    return "Error: Missing config item";
                }

            }
            if($Request['action'] == 'add'){
                $configItem =  $this->Siteconfig->add($Request['item'], $Request['value']);
                return $configItem;
            }
            if($Request['action'] == 'getall'){
                $getAll = $this->Siteconfig->get_all()->result();
                return $getAll;

            }
        }

        return "no actions";
    }

}
