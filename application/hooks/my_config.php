<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 02/03/16
 * Time: 6:56 PM
 */
function load_config()
{
    //Loads configuration from database into global CI config
    //todo: exclude large data config
    $ExcludedKeys = ['macanta_custom_logo','connected_info','custom_tabs'];
    $CI =& get_instance();

    foreach($CI->Siteconfig->get_all()->result() as $site_config)
    {
        if(in_array($site_config->key,$ExcludedKeys)) continue;
        $CI->config->set_item($site_config->key,$site_config->value);
    }
}