<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 02/06/16
 * Time: 12:11 PM
 */
if (!function_exists('_loadMacantaController'))
{
    function _loadMacantaController($CI, $Controller, $Prams  = [],$session_data=[]){
        $controller = '';
        $path = '';
        if (($last_slash = strrpos($Controller, '/')) !== FALSE)
        {
            $path = substr($Controller, 0, $last_slash + 1);
            $controller = substr($Controller, $last_slash + 1);
        }
        $name = $controller = strtolower($controller);
        $controller = ucfirst($controller);
        if(include(APPPATH.'controllers/'.$path.$controller.'.php')){
            $CI->$name = new $controller($Prams,$session_data);
        }else{
            $name = "Error: Missing Controller ".$path.$controller.'.php';
        }
        return $name;
    }
}
if (!function_exists('_loadJSCSS'))
{
    function _loadJSCSS($Prams  = array()){
        $Codes = '';
        $Codes .= '$.getScript( "ajax/test.js", function( data, textStatus, jqxhr ) {
          console.log( data ); // Data returned
          console.log( textStatus ); // Success
          console.log( jqxhr.status ); // 200
          console.log( "Load was performed." );
        });';
        return $Codes;
    }
}
if (!function_exists('is_hosted'))
{
    function is_hosted(){
        if(strpos($_SERVER["HTTP_HOST"], 'macanta.'.EXT) !== false){
            return true;
        }
        return false;
    }
}
if (!function_exists('is_base64_encoded')) {
    function is_base64_encoded($data)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
if (!function_exists('note_divider')) {
    function note_divider()
    {
        return "== macanta data: do not edit below this line ==";
    }
}
function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}

function getCurrentUserCallerId($session_data=[]){
    $CI =& get_instance();
    $CI->db->where('user_id',$session_data['InfusionsoftID']);
    $CI->db->where('meta_key','caller_id_default');
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        return $row->meta_value;
    }
    return false;
}
function isDeletedContact($Id){
    $CI =& get_instance();
    $CI->db->where('user_id',$Id);
    $CI->db->where('meta_key','deleted');
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        return true;
    }
    return false;
}
function unMarkDeletedContact($Id){
    $CI =& get_instance();
    $CI->db->where('user_id',$Id);
    $CI->db->where('meta_key','deleted');
    return $CI->db->delete('users_meta');
}
function getCurrentUserCallerIds($session_data=[]){
    $CI =& get_instance();
    $CI->db->where('user_id',$session_data['InfusionsoftID']);
    $CI->db->where('meta_key','caller_id');
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        $Values =  json_decode($row->meta_value, true);
        return $Values;
    }
    return false;

}
function getCurrentUserOutboundDevices($session_data=[]){
    $CI =& get_instance();
    $CI->db->where('user_id',$session_data['InfusionsoftID']);
    $CI->db->where('meta_key','outbound_devices');
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        return $row->meta_value;
    }
    return false;
}
function getCurrentUserOutboundDevice($session_data = []){
    $CI =& get_instance();
    $CI->db->where('user_id',$session_data['InfusionsoftID']);
    $CI->db->where('meta_key','outbound_device_default');
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        return $row->meta_value != "" ? $row->meta_value:false;
    }
    return false;
}
function getUsersCallerIds(){
    $CI =& get_instance();
    $CI->db->where('meta_key','caller_id');
    $query = $CI->db->get('users_meta');
    $UsersCallerIds = [];
    foreach ($query->result() as $row)
    {
        $UsersCallerIds[$row->user_id] = json_decode($row->meta_value, true);
    }
    return $UsersCallerIds;
}
function cdValueCheck($Value,$ContactId){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    if(isJson($Value)){
        $Decoded = json_decode($Value, true);
        return $Decoded[$ContactId];
    }else{
        return $Value;
    }
}
function applyFn(){
    $CI =& get_instance();
    /*$INIFIle = SHAREDPATH."ini/global_config.ini";
    if(!is_file($INIFIle)) return false;
        $GlobalConfig = parse_ini_file($INIFIle, true);*/

    $numargs = func_num_args();
    $arg_list = func_get_args();
    $varArr = [];
    for ($i = 1; $i < $numargs; $i++) {
        $var = 'Param'.$i;
        $varArr[] = '$'.$var;
        $$var = $arg_list[$i];
    }
    $StrPram = sizeof($varArr) > 0 ? implode(',',$varArr):'';
    $start = time();
    $result = eval('return '.$arg_list[0].'('.$StrPram.');');
    $duration = time() - $start;

    $Parameters = '';
    $FnName = $arg_list[0];
    unset($arg_list[0]);
    if(isset($GlobalConfig['debug_section']['show_parameters']))
        $Parameters = $GlobalConfig['debug_section']['show_parameters'] == 'On' ? "\nParameters:".json_encode($arg_list):"";
    $Results = '';
    if(isset($GlobalConfig['debug_section']['show_results']))
        $Results = $GlobalConfig['debug_section']['show_results'] == 'On' ? "\nResults:".json_encode($result):"";
    $Backtrace = '';
    if(isset($GlobalConfig['debug_section']['show_backtrace']))
        $Backtrace = $GlobalConfig['debug_section']['show_backtrace'] == 'On' ? "\nBacktrace: ".get_caller_info():"";

    if(isset($GlobalConfig['debug_section']['show_runtime']) && $GlobalConfig['debug_section']['show_runtime'] == 'On')
        file_put_contents(
            FCPATH."DEBUG_LOGS From ".$_SERVER['REMOTE_ADDR'].".txt",
            'DateTime: '.date('Y-m-d H:i:s')."\n$FnName, Runtime: ".$duration.
            $Parameters.
            $Results.
            $Backtrace.
            "\n-------------------\n",
            FILE_APPEND);

    return $result;
}
function get_caller_info() {
    $c = '';
    $file = '';
    $func = '';
    $CosestFunc = '';
    $class = '';
    $trace = debug_backtrace();
    $traceStr = '';
    if (isset($trace[3])) {
        $class = isset($trace[3]['class']) ? $trace[3]['class']."->":"";
        $func = $trace[3]['function'];
        $file = $trace[3]['file'];
        if ($file != '') $file = basename($file);
        $traceStr .= " ".$file." ".$class.$func. "()";
    }
    if (isset($trace[2])) {
        $class = isset($trace[2]['class']) ? $trace[2]['class']."->":"";
        $func = $trace[2]['function'];
        $file = $trace[2]['file'];
        if ($file != '') $file = basename($file);
        $traceStr .= " | ".$file." ".$class.$func. "() |";
    }
    return($traceStr);
}
function getPipelineName($Stagename){
    $CI =& get_instance();
    $OpportunityPipelines = $CI->config->item('OpportunityPipeline');
    if($OpportunityPipelines && $OpportunityPipelines !== '[]' && $OpportunityPipelines !== ''){
        $OpportunityPipelinesArr = json_decode($OpportunityPipelines, true);
        foreach ($OpportunityPipelinesArr as $OpportunityPipeline){
            $PipelineCode = $OpportunityPipeline['PipelineCode'];
            preg_match("/".$PipelineCode."\d{2}/", $Stagename, $output_array);
            //file_put_contents(dirname(__FILE__).'/result.txt', $PipelineCode." ".json_encode($output_array), FILE_APPEND );
            if(sizeof($output_array) > 0){
                return $OpportunityPipeline['PipelineName'];
            }
        }
    }else{
        return 'Default';
    }
    return 'Default';
}
function getAllwedMove($Stagename,$Stages){
    $CI =& get_instance();
    $Allowed = [];
    $OpportunityPipelines = $CI->config->item('OpportunityPipeline');
    if($OpportunityPipelines && $OpportunityPipelines !== '[]' && $OpportunityPipelines !== ''){
        $OpportunityPipelinesArr = json_decode($OpportunityPipelines, true);
        foreach ($OpportunityPipelinesArr as $OpportunityPipeline){
            $PipelineCode = $OpportunityPipeline['PipelineCode'];
            preg_match("/".$PipelineCode."\d{2}/", $Stagename, $output_array);
            if(sizeof($output_array) > 0){
                $Stage = $output_array[0];
                foreach ($OpportunityPipeline['Stages'] as $StagePair){
                    if($StagePair['stagePrefix'] == $Stage){
                        $AllowedCodesArr = explode(',', $StagePair['stageMoveTo']);
                        foreach ($AllowedCodesArr as $Code){
                            foreach ($Stages as $StageArr){
                                if(strpos($StageArr->StageName,trim($Code)) !== false){
                                    $Allowed[] = $StageArr->Id;
                                }
                            }
                        }
                        if(sizeof($Allowed) > 0){
                            $AllowedStr = implode(',',$Allowed);
                            return '{"id":['.$AllowedStr.']}';
                        }else{
                            foreach ($Stages as $StageArr){
                                preg_match("/".$PipelineCode."\d{2}/", $StageArr->StageName, $output_array2);
                                if(sizeof($output_array2) > 0){
                                    $Allowed[] = $StageArr->Id;
                                }
                            }
                            if(sizeof($Allowed) > 0){
                                $AllowedStr = implode(',',$Allowed);
                                return '{"id":['.$AllowedStr.']}';
                            }else{
                                return '{"id":[]}';
                            }
                        }
                        break;
                    }
                }
                break;

            }
        }
    }else{
        return '{"id":["ALL"]}';
    }
    return '{"id":["ALL"]}';
}
function macanta_api_get_connected_info($QueryParam)
{

    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $Return = [];
    $ConnectedDataReturn = [];
    $GroupFieldsMap =  macanta_get_connected_info_group_fields_map('','', true, false);
    //reorganize the array
    $GroupFieldsMapNew = [];
    foreach ($GroupFieldsMap as $Groups){
        $GroupFieldsMapNew[] = $Groups;
    }
    $RelationshipMap = macanta_get_connected_info_relationships_map(false);
    if(sizeof($GroupFieldsMap) == 0){
        $Return['message']= "Error: No Connected Info Available.";
    }elseif (isset($GroupFieldsMap['message'])){
        $Return['message']= $GroupFieldsMap['message'];
    }else{
        $ConnectedData = macanta_get_connected_info($QueryParam['ContactId'],$QueryParam['GroupId']);

        foreach ($ConnectedData as $ConnectedDataGroupId=>$Items){
            //get group name
            foreach ($GroupFieldsMapNew as $GroupDetails){
                if($GroupDetails['id'] == $ConnectedDataGroupId){
                    if($QueryParam['Group'] != false){
                        if(strtolower($QueryParam['Group']) != strtolower($GroupDetails['title'])) continue;
                    }
                    $GroupName = $GroupDetails['title'];
                    $ConnectedDataReturn[$ConnectedDataGroupId]['group_name'] = $GroupName;
                    $ConnectedDataReturn[$ConnectedDataGroupId]['items'] = [];
                    $tempItem = [];
                    foreach ($Items as $ItemId=>$ItemDetails){
                        $tempItem['data'] = [];
                        $tempItem['attachments'] = $ItemDetails['file_attachments'];
                        $tempItem['meta'] = $ItemDetails['meta'];
                        $FieldFound = $QueryParam['Field'] == false ? true:false;
                        foreach ($ItemDetails['value'] as $FieldId=>$FieldValue){
                            $tempItem['data'][$FieldId] = [
                                'name'=>$GroupDetails['fields'][$FieldId]['title'],
                                'type'=>$GroupDetails['fields'][$FieldId]['type'],
                                'custom-field'=>$GroupDetails['fields'][$FieldId]['custom-field'],
                                'value'=>$FieldValue
                            ];
                            if($FieldFound == false){
                                if( strtolower($QueryParam['Field']) == strtolower($GroupDetails['fields'][$FieldId]['title']) &&
                                    strtolower($QueryParam['Value']) == strtolower($FieldValue)) $FieldFound = true;
                            }
                        }
                        if($FieldFound == false) continue;

                        $tempItem['connected_contacts'] = [];
                        foreach ($ItemDetails['connected_contact'] as $ContactId=>$ContactDetails){
                            $tempItem['connected_contacts'][$ContactId]=[
                                'FirstName'=>$ContactDetails['FirstName'],
                                'LastName'=>$ContactDetails['LastName'],
                                'Email'=>$ContactDetails['Email'],

                            ];
                            foreach ($ContactDetails['relationships'] as $relationship){
                                $tempItem['connected_contacts'][$ContactId]['Relationships'][$relationship] = $RelationshipMap[$relationship];
                            }

                        }
                        $ConnectedDataReturn[$ConnectedDataGroupId]['items'][$ItemId] = $tempItem;
                    }


                    break;
                }
            }


        }


    }
    unset($ConnectedData);
    unset($RelationshipMap);
    unset($GroupFieldsMap);
    $Return['message']= $ConnectedDataReturn;
    return $Return;



}
function macanta_api_beautify_connected_info($ConnectedData, $GroupName = '')
{

    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $Return = [];
    $ConnectedDataReturn = [];
    $GroupFieldsMap =  macanta_get_connected_info_group_fields_map();
    //reorganize the array
    $GroupFieldsMapNew = [];
    foreach ($GroupFieldsMap as $Groups){
        $GroupFieldsMapNew[] = $Groups;
    }
    $RelationshipMap = macanta_get_connected_info_relationships_map();
    if(sizeof($GroupFieldsMap) == 0){
        $Return['message']= "Error: No Connected Info Available.";
    }elseif (isset($GroupFieldsMap['message'])){
        $Return['message']= $GroupFieldsMap['message'];
    }else{
        foreach ($ConnectedData as $ConnectedDataGroupId=>$Items){
            //get group name
            foreach ($GroupFieldsMapNew as $GroupDetails){
                if($GroupDetails['id'] == $ConnectedDataGroupId){

                    $GroupName = $GroupDetails['title'];
                    $ConnectedDataReturn[$ConnectedDataGroupId]['group_name'] = $GroupName;
                    $ConnectedDataReturn[$ConnectedDataGroupId]['items'] = [];
                    $tempItem = [];
                    foreach ($Items as $ItemId=>$ItemDetails){
                        $tempItem['data'] = [];
                        if(isset($ItemDetails['meta']))
                            $tempItem['meta'] = $ItemDetails['meta'];
                        foreach ($ItemDetails['value'] as $FieldId=>$FieldValue){
                            $tempItem['data'][$FieldId] = [
                                'name'=>$GroupDetails['fields'][$FieldId]['title'],
                                'type'=>$GroupDetails['fields'][$FieldId]['type'],
                                'custom-field'=>$GroupDetails['fields'][$FieldId]['custom-field'],
                                'value'=>$FieldValue
                            ];
                        }

                        $tempItem['connected_contacts'] = [];
                        foreach ($ItemDetails['connected_contact'] as $ContactId=>$ContactDetails){
                            $tempItem['connected_contacts'][$ContactId]=[
                                'FirstName'=>$ContactDetails['FirstName'],
                                'LastName'=>$ContactDetails['LastName'],
                                'Email'=>$ContactDetails['Email'],

                            ];
                            foreach ($ContactDetails['relationships'] as $relationship){
                                $tempItem['connected_contacts'][$ContactId]['Relationships'][$relationship] = $RelationshipMap[$relationship];
                            }

                        }
                        $ConnectedDataReturn[$ConnectedDataGroupId]['items'][$ItemId] = $tempItem;
                    }


                    break;
                }
            }


        }

    }
    $Return['message']= $ConnectedDataReturn;
    return $Return;



}
function macanta_migrate_connected_info($connected_info, $ContactInfo){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    foreach ($connected_info as $GroupName => $Items){
        foreach ($Items as $ItemName => $FieldValuePairs){
            $history["update_history"]= array();
            $history["connection_history"]= array();
            $DBdata = array(
                'id' => $ItemName,
                'group' => $GroupName,
                'value' => json_encode($FieldValuePairs),
                'connected_contact' => json_encode(array($ContactInfo->Id=>array("ContactId"=>$ContactInfo->Id,"FirstName"=>$ContactInfo->FirstName,"LastName"=>$ContactInfo->LastName,"Email"=>$ContactInfo->Email,"relationships"=>[]))),
                'history' => json_encode($history),
                'meta' => '{"editable":"yes","searchable":"yes","multiple_link":"yes"}',
                'created' => date("Y-m-d H:i:s"),
                'updated' => time(),
                'status' => 'active'
            );
            $CI->db->insert('connected_data', $DBdata);
        }
    }
    $CI->db->where('meta_key','connected_info');
    $CI->db->where('user_id',$ContactInfo->Id);
    $CI->db->delete('users_meta');

}
function macanta_check_contact_sequence($ContactId,$CampaignFirstSequence){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $ConnectedData = macanta_get_connected_info($ContactId);
    $InSequence = false;
    foreach ($ConnectedData as $GroupId=>$Data){
        foreach ($Data as $ItemId=>$ItemDetails){
            if(
                isset($ItemDetails['connected_contact'][$ContactId]['Sequence'][$CampaignFirstSequence]['Status']) &&
                $ItemDetails['connected_contact'][$ContactId]['Sequence'][$CampaignFirstSequence]['Status'] == 'running'
            ){
                unset($ConnectedData);
                return true;
            }
        }
    }
    unset($ConnectedData);
    return $InSequence;
}
function macanta_get_connected_data_automations($QueryType = ''){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    macanta_check_cd_automation_table();
    if ($QueryType != '') $CI->db->like('QueryType', $QueryType);
    $query = $CI->db->get('ConnectedDataAutomation');
    $DataAutomation = [];
    foreach ($query->result() as $row) {
        $DataAutomation[$row->QueryType][$row->QueryId]=[
            'QueryLabel' => $row->QueryLabel,
            'ConnectedDataGroupName' => $row->ConnectedDataGroupName,
            'Data' => json_decode($row->Data, true),
            'Status' => $row->Status,
        ];

    }
    return $DataAutomation;
}

function macanta_get_user_data()
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    macanta_check_user_table();
    $query = $CI->db->get('MacantaUser');
    $DataMacantaUsermanagement = array();
    foreach ($query->result() as $row) {

        //get FirstName of Infusionsoftcontact using Id
        $ContactId = $row->ContactId;
        $CI->db->select('FirstName');
        $CI->db->where('Id', $ContactId);
        $contatcnamequery = $CI->db->get('InfusionsoftContact')->result_object();
        $contactName = $contatcnamequery[0]->FirstName;

        //get templateName of MacantaUserPermissionTemplates using TemplateId of MacantaUser
        $TemplateId =  $row->TemplateId;
        $CI->db->select('TemplateName');
        $CI->db->where('TemplateId', $TemplateId);
        $PermissionTemplatequery = $CI->db->get('MacantaUserPermissionTemplates')->result_object();
        $templateName = $PermissionTemplatequery[0]->TemplateName;

        $DataMacantaUsermanagement[]=[
            'TemplateId' => $TemplateId,
            'ContactId' => $ContactId,
            'ContactName' => $contactName,
            'ContactLevel' => $row->ContactLevel,
            'TemplateName' => $templateName,
            'Status' => $row->Status,
        ];
    }
    return $DataMacantaUsermanagement;
}
function macanta_get_usertemplate_data()
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    macanta_check_userpermissiontemplate_table();
    $query = $CI->db->get('MacantaUserPermissionTemplates');
    $DataUserTemplatemanagement = array();
    foreach ($query->result() as $row) {

        $DataUserTemplatemanagement[] = [
            'TemplateId' => $row->TemplateId,
            'TemplateName' => $row->TemplateName,
            'TemplateDescription' => $row->TemplateDescription,
            'TabAccess' => $row->TabAccess,
            'SectionAccess' => $row->SectionAccess,
            'MQBAccess' => $row->MQBAccess,
            'Status' => $row->Status,
        ];
    }
    return $DataUserTemplatemanagement;
}
function macanta_get_connected_info(
    $ContactId = '',
    $GroupId = false,
    $QueryField='',
    $QueryValue='',
    $like=true,
    $ItemId=false,
    $history=false,
    $meta=false)
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    if ($GroupId != false) $CI->db->like('group', $GroupId);
    if ($ContactId !== '') $CI->db->like('connected_contact', '"' . $ContactId . '":{');
    if ($QueryField !== '' && $QueryValue !== ''){
        if($like == true)
            $CI->db->like($QueryField, $QueryValue);

        if($like == false)
            $CI->db->not_like($QueryField, $QueryValue);
    }
    if ($ItemId != false) $CI->db->where('id', $ItemId);
    $query = $CI->db->get('connected_data');
    $UserConnectedInfo = [];
    foreach ($query->result() as $row) {
        $UserConnectedInfo[$row->group][$row->id] = [
            'value' => json_decode($row->value, true),
            'meta' => json_decode($row->meta, true),
            'connected_contact' => json_decode($row->connected_contact, true),
            'file_attachments' => macanta_get_connected_data_file_attachements($row->id)
        ];
        if($history) $UserConnectedInfo[$row->group][$row->id]['history'] =  json_decode($row->history, true);
        if($meta) $UserConnectedInfo[$row->group][$row->id]['meta'] =  json_decode($row->meta, true);
    }
    unset($query);
    return $UserConnectedInfo;
}
function macanta_get_connected_data_file_attachements($ItemId){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    macanta_create_table_connected_data_file_attachment();
    $CI->db->where('item_id',$ItemId);
    $CI->db->order_by('filename', 'ASC');
    $query = $CI->db->get('connected_data_file_attachment');
    $Attachments = [];
    if (sizeof($query->result()) > 0){
        foreach ($query->result() as $row) {
            $Attachments[]= $row;
        }
    }
    return $Attachments;
}
//todo: finish this function to prevent spaghetti code
function macanta_cd_generate_db_query($Operator,$KeyValuePairs,$GroupFieldsMap,$ItemId, $CI){
    $LogicalOperators = [
        ">" => ["greater than", "more than", "larger than", "higher than", "exceeds", "over", "older than", "bigger than"],
        "<" => ["less than", "fewer than", "lower than", "younger than"],
        "within" => ["within"],
        "between" => ["between"],
        "is not" => ["is not"],
        "is null" => ["is null"],
        "not null" => ["not null"]
    ];
    if(sizeof($KeyValuePairs) > 0){
        foreach ($KeyValuePairs as $FieldName => $Value) {
            foreach ($GroupFieldsMap['fields'] as $fieldId=>$fieldDetails){
                if (strtolower($fieldDetails['title']) == strtolower($FieldName)){
                    foreach ($LogicalOperators as $OpSymbol => $OpSynonyms) {
                        foreach ($OpSynonyms as $OpSynonym) {
                            if (strpos($Value, $OpSynonym) !== false) {
                                switch ($OpSymbol) {
                                    case ">" :
                                        $CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                        break 4;
                                    case "<":
                                        $CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                        break 4;
                                    case "within":
                                        $CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                        break 4;
                                    case "between":
                                        $CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                        break 4;
                                    case "is null":
                                        $CI->db->like('value', '"'.$fieldId.'":%""','both',false);
                                        break 4;
                                    case "not null":
                                        $CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                        break 4;
                                }
                            }
                        }
                    }
                    $CI->db->like('value', '"'.$fieldId.'":%'.$Value,'both',false);
                    break;
                }
            }
        }
    }
    if($ItemId != ''){
        $CI->db->where('id', $ItemId);
    }
    $query = $CI->db->get('connected_data');
}
function macanta_get_connected_info_by_groupname($GroupName = '', $ContactId = '',$GroupId = '', $ItemId = '', $ParamValues = [], $QueryId='', $ParamValuesB = [], $ContactRelationships=[], $Limit = 0, $Offset = 0, $FromService = false, $Logging = false)
{

    $GroupName = strtolower(trim($GroupName));
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $Return = [];
    $GroupFieldsMap =  macanta_get_connected_info_group_fields_map($GroupName, $GroupId);
    $GroupName = $GroupName !== '' ? $GroupName:$GroupFieldsMap['title'];
    $RelationshipMap = macanta_get_connected_info_relationships_map();

    if(sizeof($GroupFieldsMap) == 0){
        //$Return['message']= "Error: No Connected Info Named: ".$GroupName;
    }elseif (isset($GroupFieldsMap['message'])){
        //$Return['message']= $GroupFieldsMap['message'];
    }
    else{
        if($FromService) $CI->db->start_cache();
        if($ItemId != ''){
            $CI->db->where('id', $ItemId);
        }else{
            if($GroupName != ''){
                $CI->db->where('group', $GroupFieldsMap['id']);
            }
            if($ContactId != ''){
                $CI->db->like('connected_contact', '"' . $ContactId . '":{','both',false);
            }
            if(sizeof($ContactRelationships) > 0){
                $RelationshipORCondition = [];
                $RelationshipANDCondition = [];
                foreach ($RelationshipMap as $RelationshipId => $RelationshipName)
                {
                    if(sizeof($ContactRelationships['or']) > 0){
                        if(in_array($RelationshipName,$ContactRelationships['or'])){
                            $RelationshipORCondition[] = $RelationshipId;
                        }
                    }
                    if(sizeof($ContactRelationships['and']) > 0){
                        if(in_array($RelationshipName,$ContactRelationships['and'])){
                            $RelationshipANDCondition[] = $RelationshipId;
                        }
                    }
                }
                if(sizeof($RelationshipORCondition) > 0){
                    if(sizeof($ContactRelationships) > 1) $CI->db->group_start();
                        $prefix = "";
                        foreach ($RelationshipORCondition as $RelationId){
                            $method =  $prefix."like";
                            $CI->db->$method('connected_contact', '"' . $RelationId . '"','both',false);
                            $prefix = "or_";
                        }
                    if(sizeof($ContactRelationships) > 1) $CI->db->group_end();
                }
                if(sizeof($RelationshipANDCondition) > 0){
                    foreach ($RelationshipANDCondition as $RelationId){
                        $CI->db->like('connected_contact', '"' . $RelationId . '"','both',false);
                    }
                }
            }
            if(sizeof($ParamValues) > 0){
                $FieldsCondition = [];
                foreach ($ParamValues as $QueryCDField){
                    if(is_object($QueryCDField)) $QueryCDField = (array) $QueryCDField;
                    foreach ($GroupFieldsMap['fields'] as $fieldId=>$fieldDetails){
                        if (strtolower($fieldDetails['title']) == strtolower($QueryCDField['queryCDFieldName'])){
                            $Logic = $QueryCDField['queryCDFieldLogic'] == "and" ? "and":"or";
                            if(isset($QueryCDField['queryCDFieldValues'])){
                                $FieldsCondition[$Logic][] = [
                                    'operator'=>$QueryCDField['queryCDFieldOperator'],
                                    'type'=>$fieldDetails['type'],
                                    'data' => [$fieldId=>$QueryCDField['queryCDFieldValues']]
                                ];
                            }
                            else{
                                $queryCDFieldValue = str_replace('/','%',$QueryCDField['queryCDFieldValue']);
                                if (strpos($queryCDFieldValue, ' and ') !== false) {
                                    $queryCDFieldValueArr =  explode(' and ', $queryCDFieldValue);
                                    $FieldsCondition[$Logic][] = [
                                        'operator'=>$QueryCDField['queryCDFieldOperator'],
                                        'type'=>$fieldDetails['type'],
                                        'sublogic' => 'and',
                                        'data' => [$fieldId=>$queryCDFieldValueArr]
                                    ];
                                }
                                elseif(strpos($queryCDFieldValue, ' or ') !== false){
                                    $queryCDFieldValueArr =  explode(' or ', $queryCDFieldValue);
                                    $FieldsCondition[$Logic][] = [
                                        'operator'=>$QueryCDField['queryCDFieldOperator'],
                                        'type'=>$fieldDetails['type'],
                                        'sublogic' => 'or',
                                        'data' => [$fieldId=>$queryCDFieldValueArr]
                                    ];
                                }
                                else{
                                    $FieldsCondition[$Logic][] = [
                                        'operator'=>$QueryCDField['queryCDFieldOperator'],
                                        'type'=>$fieldDetails['type'],
                                        'data' => [$fieldId=>$queryCDFieldValue]
                                    ];
                                }
                            }
                            break;
                        }
                    }
                }
                $AvailableLogic = ['or','and'];
                $CI->db->group_start();
                $InitGroup = 'group_start';
                foreach ($AvailableLogic as $theLogic){
                    if(isset($FieldsCondition[$theLogic]) && is_array($FieldsCondition[$theLogic]) && sizeof($FieldsCondition[$theLogic]) > 0){
                        $prefix = "";
                        if(isset($FieldsCondition['or']) && isset($FieldsCondition['and'])) $CI->db->$InitGroup();
                        foreach ($FieldsCondition[$theLogic] as $FieldsORCondition){
                            $operator = $FieldsORCondition['operator'];
                            $data = $FieldsORCondition['data'];
                            $type = $FieldsORCondition['type'];
                            $LikeMethod  = $prefix."like";
                            $NotLikeMethod  = $prefix."not_like";
                            foreach ($data as $fieldId => $theFieldValue){
                                if(is_array($theFieldValue)){
                                    if(isset($FieldsORCondition['sublogic']) && $FieldsORCondition['sublogic'] == 'and') $prefix = '';
                                    $sub_group_start = $FieldsORCondition['sublogic'] == 'and' ? 'group_start':'or_group_start';
                                    if(sizeof($theFieldValue) > 1) $CI->db->$sub_group_start();
                                    foreach ($theFieldValue as $theFieldValueItem){
                                        $LikeMethod  = $prefix."like";
                                        $NotLikeMethod  = $prefix."not_like";
                                        // Process Operator here
                                        switch ($operator){
                                            case "":
                                                if($type == 'Date' || $type == 'DateTime'){
                                                    $CI->db->$NotLikeMethod('value', '"'.$fieldId.'":%""','both',false);
                                                    $CI->db->or_not_like('value', '"'.$fieldId.'":""','both',false);
                                                }else{
                                                    $CI->db->$LikeMethod('value', '"'.$fieldId.'":%"'.$theFieldValueItem,'both',false);
                                                    $CI->db->or_like('value', '"'.$fieldId.'":"'.$theFieldValueItem,'both',false);
                                                }
                                                break;
                                            case "not null":
                                            case "greater than":
                                            case "less than":
                                                $CI->db->$NotLikeMethod('value', '"'.$fieldId.'":%""','both',false);
                                                $CI->db->or_not_like('value', '"'.$fieldId.'":""','both',false);
                                                break;
                                            case "is not":
                                                $CI->db->$NotLikeMethod('value', '"'.$fieldId.'":%'.$theFieldValueItem,'both',false);
                                                $CI->db->or_not_like('value', '"'.$fieldId.'":"'.$theFieldValueItem,'both',false);
                                                break;
                                            case "is null":
                                                $CI->db->$LikeMethod('value', '"'.$fieldId.'":%""','both',false);
                                                $CI->db->or_like('value', '"'.$fieldId.'":""','both',false);
                                                break;

                                        }
                                        $prefix = "or_";
                                    }
                                    if(sizeof($theFieldValue) > 1) $CI->db->group_end();
                                }
                                else{
                                    // Process Operator here
                                    switch ($operator){
                                        case "":
                                            if($type == 'Date' || $type == 'DateTime'){
                                                $CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                                $CI->db->or_not_like('value', '"'.$fieldId.'":""','both',false);
                                            }else{
                                                $CI->db->$LikeMethod('value', '"'.$fieldId.'":%"'.$theFieldValue,'both',false);
                                                $CI->db->or_like('value', '"'.$fieldId.'":"'.$theFieldValue,'both',false);
                                            }
                                            break;
                                        case "not null":
                                        case "greater than":
                                        case "less than":
                                            $CI->db->$NotLikeMethod('value', '"'.$fieldId.'":%""','both',false);
                                            $CI->db->or_not_like('value', '"'.$fieldId.'":""','both',false);
                                            break;
                                        case "is not":
                                            $CI->db->$NotLikeMethod('value', '"'.$fieldId.'":%'.$theFieldValue,'both',false);
                                            $CI->db->or_not_like('value', '"'.$fieldId.'":"'.$theFieldValue,'both',false);
                                            break;
                                        case "is null":
                                            $CI->db->$LikeMethod('value', '"'.$fieldId.'":%""','both',false);
                                            $CI->db->or_like('value', '"'.$fieldId.'":""','both',false);
                                            break;

                                    }
                                }
                            }
                            $prefix = $theLogic == 'and' ? "":"or_";
                        }
                        $InitGroup = $theLogic == 'and' ? 'group_start':'or_group_start';
                        if(isset($FieldsCondition['or']) && isset($FieldsCondition['and'])) $CI->db->group_end();
                    }
                }
                $CI->db->group_end();
            }
            //Use for start campaign
            if(sizeof($ParamValuesB) > 0){
                $Operators = [
                    ">" => ["greater than", "more than", "larger than", "higher than", "exceeds", "over", "older than", "bigger than"],
                    "<" => ["less than", "fewer than", "lower than", "younger than"],
                    "within" => ["within"],
                    "between" => ["between"],
                    "is null" => ["is null"],
                    "not null" => ["not null"]
                ];
                foreach ($ParamValuesB as $FieldName => $Value) {
                    $Value = str_replace(['/',' and '],'%',$Value);
                    foreach ($GroupFieldsMap['fields'] as $fieldId=>$fieldDetails){
                        if (strtolower($fieldDetails['title']) == strtolower($FieldName)){
                            foreach ($Operators as $OpSymbol => $OpSynonyms) {
                                foreach ($OpSynonyms as $OpSynonym) {
                                    if (strpos($Value, $OpSynonym) !== false) {
                                        switch ($OpSymbol) {
                                            case ">" :
                                            case "<":
                                            case "within":
                                            case "between":
                                            case "not null":
                                                //$CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                                $CI->db->not_like('value', '"'.$fieldId.'":""','both',false);
                                                break 4;
                                            case "is null":
                                                //$CI->db->like('value', '"'.$fieldId.'":%""','both',false);
                                                $CI->db->or_like('value', '"'.$fieldId.'":""','both',false);
                                                break 4;

                                        }
                                    }
                                }
                            }
                            if($fieldDetails['type'] == 'Date' || $fieldDetails['type'] == 'DateTime'){
                                //$CI->db->not_like('value', '"'.$fieldId.'":%""','both',false);
                                $CI->db->not_like('value', '"'.$fieldId.'":""','both',false);
                            }else{
                                //$CI->db->like('value', '"'.$fieldId.'":%"'.$Value,'both',false);
                                $CI->db->like('value', '"'.$fieldId.'":"%'.$Value."%",'both',false);
                            }
                            break;
                        }
                    }
                }
            }
        }
        if($FromService) $CI->db->stop_cache();
        if($CI->config->item('IS_App_Name') == 'martinneely' ) $CI->db->where('updated >', 1588932000);//2020-05-08 12:00:00
        if($Limit != 0){
            $CI->db->limit($Limit, $Offset);
        }
        $CI->db->order_by('updated', 'DESC');
        $query = $CI->db->get('connected_data');
        $num_rows = $query ? $query->num_rows():'false';
        $Message = "Limit: $Limit Offset: $Offset NumRows: {$num_rows} \nGroup: $GroupName Trigger: $QueryId Items Returned: ".sizeof($Return[$GroupName])."\n". "Last Query: \n" . $CI->db->last_query()."\n". "Error: ".json_encode($CI->db->error())."\n"
        ;
        if($query){
            foreach ($query->result() as $row) {
                $values = json_decode($row->value, true);
                $theVal = [];
                foreach ($GroupFieldsMap['fields'] as $fieldId=>$fieldDetails){
                    if(!isset($values[$fieldId]) || empty($values[$fieldId])){
                        $values[$fieldId] = $fieldDetails['default-value'];
                    }
                    $theVal[$fieldDetails['title']] = ['id'=>$fieldId,'value'=>$values[$fieldId],'field-type'=>$fieldDetails['type'],'custom-field'=>$fieldDetails['custom-field'],'default-value'=>$fieldDetails['default-value']];
                }
                $theVal['item_id_custom_field'] = [
                    'id'=>'item_id_custom_field',
                    'value'=>$row->id,
                    'field-type'=>'text',
                    'custom-field'=>$GroupFieldsMap['item_id_custom_field']
                ];
                $connected_contact = [];
                foreach (json_decode($row->connected_contact, true) as $ContactId => $Details){
                    $theRelation = [];
                    foreach ($Details['relationships'] as $relationships){
                        $theRelation[] = $RelationshipMap[$relationships];
                    }
                    $Details['relationships'] = $theRelation;
                    $connected_contact[$ContactId] = $Details;
                }
                $Return[$GroupName][$row->id] = [
                    'fields' => $theVal,
                    'connected_contact' => $connected_contact,
                    'meta' => json_decode($row->meta, true)
                ];
            }
        }
        $PriorityMessage = "\n";
        if($FromService && $Offset != 0){
            $CI->db->where('updated >', strtotime("-15 min"));
            $CI->db->limit(50, 0);
            $CI->db->order_by('updated', 'DESC');
            $queryPriority = $CI->db->get('connected_data');
            if($queryPriority){
                foreach ($queryPriority->result() as $row) {
                    $values = json_decode($row->value, true);
                    $theVal = [];
                    foreach ($GroupFieldsMap['fields'] as $fieldId=>$fieldDetails){
                        if(!isset($values[$fieldId]) || empty($values[$fieldId])){
                            $values[$fieldId] = $fieldDetails['default-value'];
                        }
                        $theVal[$fieldDetails['title']] = ['id'=>$fieldId,'value'=>$values[$fieldId],'field-type'=>$fieldDetails['type'],'custom-field'=>$fieldDetails['custom-field'],'default-value'=>$fieldDetails['default-value']];
                    }
                    //Add CD Item ID and its CustomField
                    $theVal['item_id_custom_field'] = [
                        'id'=>'item_id_custom_field',
                        'value'=>$row->id,
                        'field-type'=>'text',
                        'custom-field'=>$GroupFieldsMap['item_id_custom_field']
                    ];
                    $connected_contact = [];
                    foreach (json_decode($row->connected_contact, true) as $ContactId => $Details){
                        $theRelation = [];
                        foreach ($Details['relationships'] as $relationships){
                            $theRelation[] = $RelationshipMap[$relationships];
                        }
                        $Details['relationships'] = $theRelation;
                        $connected_contact[$ContactId] = $Details;
                    }
                    $Return[$GroupName][$row->id] = [
                        'fields' => $theVal,
                        'connected_contact' => $connected_contact,
                        'meta' => json_decode($row->meta, true),
                        'priority' => true
                    ];
                }
            }
            $num_rows = $queryPriority ? $queryPriority->num_rows():'false';
            $PriorityMessage = "======ADD Priority=======\n Limit: $Limit Offset: $Offset  NumRows: {$num_rows}  \nGroup: $GroupName  Trigger: $QueryId  Items Returned: ".sizeof($Return[$GroupName])."\n". "Last Query: \n" .$CI->db->last_query()."\n\n". "Error: ".json_encode($CI->db->error())."\n"
            ;
        }
        if($Logging) macanta_log_file("CD_query_",$Message.$PriorityMessage);
        $CI->db->flush_cache();
    }
    unset($GroupFieldsMap);
    unset($RelationshipMap);
    unset($query);
    return $Return;



}
function macanta_get_connected_info_group_fields_map($GroupName = '',$GroupId = '', $verbose = true, $changeCase=true){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $Return = [];
    $ConnectedInfoSettings = macanta_get_config('connected_info');
    if($ConnectedInfoSettings){
        $ConnectedInfoSettings = json_decode($ConnectedInfoSettings);
        foreach ($ConnectedInfoSettings as $ConnectedInfoSetting){
            $title = strtolower($ConnectedInfoSetting->title);
            if($GroupName === '' && $GroupId === ''){
                if($verbose == false){
                    $Return[$ConnectedInfoSetting->title] = [
                        'id'=>$ConnectedInfoSetting->id,
                        'item_id_custom_field'=> isset($ConnectedInfoSetting->item_id_custom_field) ? $ConnectedInfoSetting->item_id_custom_field:"",
                        'fields' => []
                    ];
                }else{
                    $Return[$title] = [
                        'id'=>$ConnectedInfoSetting->id,
                        'item_id_custom_field'=> isset($ConnectedInfoSetting->item_id_custom_field) ? $ConnectedInfoSetting->item_id_custom_field:"",
                        'title'=>$ConnectedInfoSetting->title,
                        'fields' => []
                    ];
                }

                foreach ($ConnectedInfoSetting->fields as $fields){
                    $fieldChoices = str_replace("\r",'',$fields->fieldChoices);
                    if($verbose == false){
                        $Return[$ConnectedInfoSetting->title]['fields'][$fields->fieldLabel] = [
                            'id'=> $fields->fieldId,
                            'type'=> $fields->fieldType,
                            'contact_specific'=> isset($fields->contactspecificField) ? $fields->contactspecificField:"no",
                            'custom-field'=>$fields->infusionsoftCustomField,
                            'section-tag-id'=> isset($fields->sectionTagId) ? $fields->sectionTagId:"",
                            'field-choices'=> isset($fields->fieldChoices) ? str_replace("\n",',',$fieldChoices):""

                        ];
                    }else{
                        $Return[$title]['fields'][$fields->fieldId] = [
                            'id'=> $fields->fieldId,
                            'type'=> $fields->fieldType,
                            'contact_specific'=> isset($fields->contactspecificField) ? $fields->contactspecificField:"no",
                            'title'=> $changeCase == false ? $fields->fieldLabel:strtolower($fields->fieldLabel),
                            'custom-field'=>$fields->infusionsoftCustomField,
                            'section-tag-id'=> isset($fields->sectionTagId) ? $fields->sectionTagId:"",
                            'field-choices'=> isset($fields->fieldChoices) ? str_replace("\n",',',$fieldChoices):""

                        ];
                    }

                }
            }
            elseif($GroupName !== ''){
                $GroupName = strtolower($GroupName);

                if($title == $GroupName){
                    if($verbose == false){
                        $Return = [
                            'id'=>$ConnectedInfoSetting->id,
                            'item_id_custom_field'=> isset($ConnectedInfoSetting->item_id_custom_field) ? $ConnectedInfoSetting->item_id_custom_field:"",
                            'fields' => []
                        ];
                    }else{
                        $Return = [
                            'id'=>$ConnectedInfoSetting->id,
                            'item_id_custom_field'=> isset($ConnectedInfoSetting->item_id_custom_field) ? $ConnectedInfoSetting->item_id_custom_field:"",
                            'title'=>$ConnectedInfoSetting->title,
                            'fields' => []
                        ];
                    }
                    foreach ($ConnectedInfoSetting->fields as $fields){
                        $fieldChoices = isset($fields->fieldChoices) ? str_replace("\r",'',$fields->fieldChoices):"";
                        if($verbose == false){
                            $Return['fields'][$fields->fieldLabel] = [
                                'id'=> $fields->fieldId,
                                'type'=> $fields->fieldType,
                                'contact_specific'=> isset($fields->contactspecificField) ? $fields->contactspecificField:"no",
                                'custom-field'=>$fields->infusionsoftCustomField,
                                'default-value'=>$fields->defaultValue,
                                'section-tag-id'=> isset($fields->sectionTagId) ? $fields->sectionTagId:"",
                                'field-choices'=> isset($fields->fieldChoices) ? str_replace("\n",',',$fieldChoices):""

                            ];
                        }else{
                            $Return['fields'][$fields->fieldId] = [
                                'id'=> $fields->fieldId,
                                'type'=> $fields->fieldType,
                                'contact_specific'=> isset($fields->contactspecificField) ? $fields->contactspecificField:"no",
                                'title'=>$changeCase == false ? $fields->fieldLabel:strtolower($fields->fieldLabel),
                                'custom-field'=>$fields->infusionsoftCustomField,
                                'default-value'=>$fields->defaultValue,
                                'section-tag-id'=> isset($fields->sectionTagId) ? $fields->sectionTagId:"",
                                'field-choices'=> isset($fields->fieldChoices) ? str_replace("\n",',',$fieldChoices):""

                            ];
                        }
                    }
                    break;
                }

            }elseif($GroupId !== ''){
                if($ConnectedInfoSetting->id == $GroupId){
                    if($verbose == false){
                        $Return = [
                            'id'=>$ConnectedInfoSetting->id,
                            'item_id_custom_field'=> isset($ConnectedInfoSetting->item_id_custom_field) ? $ConnectedInfoSetting->item_id_custom_field:"",
                            'fields' => []
                        ];
                    }else{
                        $Return = [
                            'id'=>$ConnectedInfoSetting->id,
                            'item_id_custom_field'=> isset($ConnectedInfoSetting->item_id_custom_field) ? $ConnectedInfoSetting->item_id_custom_field:"",
                            'title'=>$ConnectedInfoSetting->title,
                            'fields' => []
                        ];
                    }
                    foreach ($ConnectedInfoSetting->fields as $fields){
                        $fieldChoices = str_replace("\r",'',$fields->fieldChoices);
                        if($verbose == false){
                            $Return['fields'][$fields->fieldLabel] = [
                                'id'=> $fields->fieldId,
                                'type'=> $fields->fieldType,
                                'contact_specific'=> isset($fields->contactspecificField) ? $fields->contactspecificField:"no",
                                'custom-field'=>$fields->infusionsoftCustomField,
                                'default-value'=>$fields->defaultValue,
                                'section-tag-id'=> isset($fields->sectionTagId) ? $fields->sectionTagId:"",
                                'field-choices'=> isset($fields->fieldChoices) ? str_replace("\n",',',$fieldChoices):""

                            ];
                        }else{
                            $Return['fields'][$fields->fieldId] = [
                                'id'=> $fields->fieldId,
                                'type'=> $fields->fieldType,
                                'contact_specific'=> isset($fields->contactspecificField) ? $fields->contactspecificField:"no",
                                'title'=>$changeCase == false ? $fields->fieldLabel:strtolower($fields->fieldLabel),
                                'custom-field'=>$fields->infusionsoftCustomField,
                                'default-value'=>$fields->defaultValue,
                                'section-tag-id'=> isset($fields->sectionTagId) ? $fields->sectionTagId:"",
                                'field-choices'=> isset($fields->fieldChoices) ? str_replace("\n",',',$fieldChoices):""

                            ];
                        }
                    }
                    break;
                }

            }

        }
    }else{
        $Return['message']= "Error: ".$CI->config->item('IS_App_Name'). " No Connected Info Setup.";
    }
    unset($ConnectedInfoSettings);
    return $Return;
}
function macanta_get_connected_info_relationships_map($toLowerCase = true){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $Return = [];
    $ConnectorRelationships = json_decode($CI->config->item('ConnectorRelationship'));
    if($ConnectorRelationships){
        foreach ($ConnectorRelationships as $ConnectorRelationship){
            $RelationshipName = $toLowerCase == true ? strtolower($ConnectorRelationship->RelationshipName):$ConnectorRelationship->RelationshipName;
            $Return[$ConnectorRelationship->Id] = trim($RelationshipName);
        }
    }else{
        $Return['message']= "Error: ".$CI->config->item('IS_App_Name'). " No Relationship Setup.";
    }
    return $Return;
}
function macanta_get_connected_info_by_group_id($GroupId,$ExcludedCCId=false, $SearchStr = '')
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->db->where('group', $GroupId);
    if($ExcludedCCId != false) $CI->db->not_like('connected_contact', '"'.$ExcludedCCId.'":{');
    if($SearchStr != '')  {
        $CI->db->like('value', $SearchStr);
        //$CI->db->or_like('connected_contact', $SearchStr);
    }
    $query = $CI->db->get('connected_data');
    $UserConnectedInfo = [];
    foreach ($query->result() as $row) {
        $UserConnectedInfo[$row->group][$row->id] = [
            'value' => json_decode($row->value, true),
            'connected_contact' => json_decode($row->connected_contact, true),
            'history' => json_decode($row->history, true),
            'meta' => json_decode($row->meta, true),
            'status' => $row->status
        ];
    }
    unset($query);
    return $UserConnectedInfo;
}
function macanta_update_connected_data($contactId, $group_id, $group_name="", $values=false,$connected_contacts=false, $meta=false, $fieldId=false, $fieldVal=false, $update_fields = true, $item_id_custom_field='',$item_id="", $FromAdd=false, $ModifyUpdateDate = true){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $DBData = [];
    $FieldsToUpdate = $DataModification = $DataOld = $DataNew = $OldContactArr = $ContactsNew = $ContactsModification = [];
    $DataModified = $ContactsModified = false;
    $Results = '';
    $Update_result = '';
    if($item_id == ""){
        $CI->db->where('group',$group_id);
        if ($fieldId != false && $fieldVal != ""){
            if(is_numeric($fieldVal)){
                $CI->db->like('value', "\"$fieldId\":$fieldVal,");
                $CI->db->or_like('value', "\"$fieldId\":$fieldVal}");
                $CI->db->or_like('value', "\"$fieldId\":\"$fieldVal\"");
            }else{
                $CI->db->like('value', "\"$fieldId\":\"$fieldVal\"");
            }
        }
    }else{
        $CI->db->where('id',$item_id);
    }

    $query = $CI->db->get('connected_data');
    if (sizeof($query->result()) > 0){
        foreach ($query->result() as $row) {
            $GroupId = $row->group;
            if($group_name != "" && isset($GLOBALS['group_details_cache'][$group_name]) ){
                $group_details = $GLOBALS['group_details_cache'][$group_name];
            }else{
                $group_details = macanta_get_connected_info_group_fields_map('',$GroupId);
            }
            $ItemId = $row->id;
            $DataOld = json_decode($row->value, true);
            if($values && is_array($values)){
                $values = array_change_key_case($values,CASE_LOWER);
                foreach ($group_details['fields'] as $field_id => $field_details ){
                    foreach ($values as $title=>$value){
                        $value = $value === null ? "":html_entity_decode(trim($value));
                        if($title == strtolower($field_details['title'])){
                            if( $field_details['contact_specific'] != 'yes' ){
                                if(isset($DataOld[$field_id]) && $DataOld[$field_id] != $value){
                                    $DataModification[$field_id]['from']=$DataOld[$field_id];
                                    $DataModification[$field_id]['to']=$value;
                                    $DataOld[$field_id] =  $value;
                                }
                                else{
                                    $DataModification[$field_id]['from']='';
                                    $DataModification[$field_id]['to']=$value;
                                    $DataOld[$field_id] =  $value;
                                }
                            }else{
                                if($contactId != false){
                                    if(isset($DataOld[$field_id]["id_" . (string)$contactId]) &&
                                        $DataOld[$field_id]["id_" . (string)$contactId] != $value)
                                    {
                                        $DataModification[$field_id]['from']["id_" . (string)$contactId]=$DataOld[$field_id]["id_" . (string)$contactId];
                                        $DataModification[$field_id]['to']["id_" . (string)$contactId]=$value;
                                        $DataOld[$field_id]["id_" . (string)$contactId] =  $value;
                                    }else{
                                        $DataModification[$field_id]['from']["id_" . (string)$contactId]='';
                                        $DataModification[$field_id]['to']["id_" . (string)$contactId]=$value;
                                        $DataOld[$field_id]["id_" . (string)$contactId] =  $value;
                                    }
                                }
                            }
                        }
                    }
                }
                if(sizeof($DataModification) > 0) {
                    $DBData['value'] = json_encode($DataOld);
                    macanta_cd_record_history($ItemId, $DataModification, 'connected_data');
                }
            }

            if($connected_contacts && is_array($connected_contacts)  &&  sizeof($connected_contacts) > 0){

                $relationships_map = macanta_get_connected_info_relationships_map();
                $NewConnectedContactInfo = $OldContactArr = json_decode($row->connected_contact, true);
                $ContactRelationship = [];
                $ContactUnlink = [];
                foreach ($connected_contacts as $contact_id=>$relation_name){
                    if($relation_name == 'unlink'){
                        $ContactUnlink[] = $contact_id;
                        unset($NewConnectedContactInfo[$contact_id]);
                        unset($OldContactArr[$contact_id]);
                        continue;
                    }elseif (!empty($relation_name)){
                        if($item_id_custom_field != ""){
                            $data = [];
                            $data[] = ['name' => '_'.$item_id_custom_field, 'value' => $ItemId];
                            $Update_result = infusionsoft_update_contact($data, $contact_id);
                        }else{
                            $Update_result = "No Custom Field Setup";
                        }
                        $Contact = infusionsoft_get_contact_by_id_simple($contact_id);
                        $relation_id_arr = [];
                        $relation_name_arr = explode(',', $relation_name);
                        foreach ($relation_name_arr as $relation){
                            $relationship_id = array_search(strtolower(trim($relation)),$relationships_map);
                            if($relationship_id !== false){
                                $relation_id_arr[] = $relationship_id;
                            }else{
                                //create relationship if not existing and return the id
                                $NewId = macanta_create_relationship($relation,'',$GroupId);
                                $relation_id_arr[] = $NewId;
                                $relationships_map[$NewId] = $relation;
                            }
                        }
                        if(sizeof($relation_id_arr) > 0){
                            $ContactRelationship[$contact_id] = $relation_id_arr;
                            $NewConnectedContactInfo[$contact_id]['relationships']=$relation_id_arr;
                            $NewConnectedContactInfo[$contact_id]['ContactId']=$contact_id;
                            $NewConnectedContactInfo[$contact_id]['FirstName']=$Contact[0]->FirstName;
                            $NewConnectedContactInfo[$contact_id]['LastName']=$Contact[0]->LastName;
                            $NewConnectedContactInfo[$contact_id]['Email']=$Contact[0]->Email;
                        }
                    }

                }
                $ContactsNew = macanta_array_update($NewConnectedContactInfo, $OldContactArr, $ContactsModification, $ContactsModified);
                if($NewConnectedContactInfo!=$OldContactArr && $ContactsModified === true) {
                    //Update relationship; don't merge if its comming from connecteddata/edit
                    if ($FromAdd == false && isset($ContactsNew[$contactId])){
                        $ContactsNew[$contactId]['relationships'] = $ContactRelationship[$contactId];
                    }
                    $DBData['connected_contact'] = json_encode($NewConnectedContactInfo);
                    macanta_cd_record_history($ItemId, $ContactsModification, 'connected_contact');
                }elseif (sizeof($ContactUnlink)>0){
                    $DBData['connected_contact'] = json_encode($NewConnectedContactInfo);
                    macanta_cd_record_history($ItemId, ['contact_removed'=>$ContactUnlink], 'connected_contact');
                }
            }
            if($meta && is_array($meta)){
                $Old = json_decode($row->meta, true);
                $modified = false;
                $modification = [];
                $New = macanta_array_update($meta, $Old, $modification, $modified);
                if($New!=$Old && $modified === true) {
                    $DBData['meta'] = json_encode($New);
                    macanta_cd_record_history($ItemId, $modification, 'meta');
                }
            }
            if(sizeof($DBData) > 0){
                if($ModifyUpdateDate)
                    $DBData['updated'] = time();

                $CI->db->where('id',$ItemId);
                $Results = $CI->db->update('connected_data', $DBData);
                $ToLogs = [];
                if(sizeof($DataModification) > 0){

                    if (isset($GLOBALS['EditThread'])){
                        if(isset($GLOBALS['EditThread'][$item_id]['Details']["TimeToEnterCDAmend"])){
                            $GLOBALS['EditThread'][$item_id]['Nested'][] = [
                                $ItemId => [
                                "TimeToEnterCDAmend"=>date("H:i:s"),
                                "DataModification" => $DataModification
                                ]
                            ];
                        }else{
                            $GLOBALS['EditThread'][$item_id]['Details']["TimeToEnterCDAmend"] = date("H:i:s");
                            $GLOBALS['EditThread'][$item_id]['Details']["DataModification"] = $DataModification;
                        }

                    }
                    $queryConnectedDataType =  $group_details['title'];
                    foreach ($DataModification as $FieldId => $DataMods){

                        $queryCDFieldName = $group_details['fields'][$FieldId]['title'];//lowercase
                        if(isset($DataMods['from']) and isset($DataMods['to'])){
                            if(is_array($DataMods['from']) && is_array($DataMods['to'])){
                                foreach ($DataMods['from'] as $Contact => $FieldVal){
                                    if(isset($DataMods['to'][$Contact]) && $FieldVal != $DataMods['to'][$Contact]){
                                        //trigger this
                                        $ContactId = str_replace('id_','',$Contact);
                                        $ToLogs[$queryCDFieldName] = macanta_trigger_automation_cd_amended($queryConnectedDataType,$queryCDFieldName,$ContactId,$ItemId);
                                        macanta_logger('SystemCDAmendedAction',json_encode($ToLogs));

                                    }
                                }
                            }
                            elseif (!is_array($DataMods['from']) && !is_array($DataMods['to'])){
                                if($DataMods['from'] != $DataMods['to']){
                                    //trigger this
                                    $ToLogs[$queryCDFieldName] = macanta_trigger_automation_cd_amended($queryConnectedDataType,$queryCDFieldName,$contactId,$ItemId);
                                    macanta_logger('SystemCDAmendedAction',json_encode($ToLogs));

                                }
                            }
                        }
                    }
                    if (isset($GLOBALS['EditThread'])){
                        if(!isset($GLOBALS['EditThread'][$item_id]['Details']["TimeToEndCDAmend"])){
                            $GLOBALS['EditThread'][$item_id]['Details']["TimeToEndCDAmend"] = date("H:i:s");
                        }

                    }

                }

            }
        }
        unset($group_details);
        unset($relationships_map);
        unset($DataNew);
        unset($New);
        unset($Contact);
        unset($ContactsNew);
        $DBData['id'] = $ItemId;
        //Check for Trigger Action for cd_amended

        return ["DBData"=>$DBData, "QueryResult"=>$Results, "Update_result"=>$Update_result, "toLogs"=>$ToLogs];
    }
    return false;

}
function macanta_trigger_automation_cd_amended($queryConnectedDataType,$queryCDFieldName,$contactId,$ItemId){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Logs = [];
    $Logs['queryConnectedDataType'] = $queryConnectedDataType;
    $Logs['fields'][]=$queryCDFieldName;
    $CI->db->where('QueryType','TriggerConditions');
    $CI->db->where('ConnectedDataGroupName',$queryConnectedDataType);
    $CI->db->like('Data', '"queryCDFieldName":"'.$queryCDFieldName.'","queryCDFieldOperator":"cd_amended"');
    $query = $CI->db->get('ConnectedDataAutomation');
    $Logs['lastQuery'] = $CI->db->last_query();
    $Logs['numRows'] = $query->num_rows();
    $Logs['results'] = '';
    foreach ($query->result() as $row) {
        $TriggerConditionId = $row->QueryId;
        $Logs['QueryIds'][] = $TriggerConditionId;
        $ValidTriggers = macanta_get_actions_by_condition_id($TriggerConditionId,$ItemId,$contactId, true);
        $Logs['results'] =  macanta_execute_valid_triggers($ValidTriggers);
    }
    return $Logs;
}
function macanta_get_actions_by_condition_id($TriggerConditionId,$ItemId,$contactId, $WithWait = false){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->db->where('QueryType','AutomationGroups');
    $CI->db->like('Data', $TriggerConditionId);
    $query = $CI->db->get('ConnectedDataAutomation');
    $IfThens = [];
    foreach ($query->result() as $row) {
        $Data = json_decode($row->Data,true);
        foreach ($Data['IfCondition'] as $index => $IfConditionQueryId){
            if($Data['IfThenStatus'][$index] == 'inactive' || $IfConditionQueryId != $TriggerConditionId) continue; // skip if not active
            $ThenWait = isset($Data['ThenWait']) ? $Data['ThenWait'][$index]:'';
            $IfThens[$IfConditionQueryId][] = [
                'ThenAction'=>$Data['ThenAction'][$index],
                'ThenWait' => $ThenWait
            ];;
        }
    }
    $ToExecute = [];
    foreach ($IfThens as $IfQueryId => $ThenIds){
        foreach ($ThenIds as $ThenId){
            $CI->db->where('QueryId',$ThenId['ThenAction']);
            $query = $CI->db->get('ConnectedDataAutomation');
            $return = $query->row();
            if (isset($return)) {
                $ThenQuery = json_decode($return->Data);
                $ExecuteDetail = ['ItemId'=>$ItemId,'contactId'=>$contactId,'action'=>$ThenQuery];
                if($WithWait == true){
                    $WaitId = $IfQueryId.':'.$ThenQuery->queryId.':'.$ItemId.':'.$contactId;
                    if(macanta_check_automation_wait_time($WaitId, $ExecuteDetail, $ThenId['ThenWait']) == false) continue;
                }
                $ToExecute[$IfQueryId][] = $ExecuteDetail;
            }
        }
    }
    return $ToExecute;
}
function macanta_add_connected_data($group_id, $group_details, $values, $connected_contacts, $meta=false,$item_id_custom_field='', $FromLocal = false){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $relationships_map = macanta_get_connected_info_relationships_map();
    $DBData = [];
    $OldValuesArr = [];
    $OldContactArr = [];
    $history_to = [];
    $DefaultMeta = ["editable"=>"yes","searchable"=>"yes","multiple_link"=>"yes"];
    $meta = $meta == false ? $DefaultMeta:array_merge($DefaultMeta, $meta);
    $ItemId = macanta_generate_key('item_');
    foreach ($connected_contacts as $contact_id=>$relation_name){
        $ContactId = $contact_id;
        $RefCustomField = $group_details['item_id_custom_field'];
        if(!empty($RefCustomField)){
            $data[] = ['name' => '_'.$RefCustomField, 'value' => $ItemId];
            foreach ($data as $isField) {
                $fieldsArr[] = '"' . $isField['name'] . '":' . json_encode($isField['value']);
            }
            /*Rucksack Request To update IS contact*/
            $fields = '{' . implode(',', $fieldsArr) . '}';
            $action = "update_is";
            $action_details = '{"table":"Contact","id":"' . $ContactId . '","fields":' . $fields . '}';
            applyFn('rucksack_request',$action, $action_details, false);
        }
        foreach ($group_details['fields'] as $field_id => $field_details ){
            $field_details['default_value'] = isset($field_details['default_value']) ? $field_details['default_value']:"";
            // this version is for filling all the fields
            if($values[strtolower($field_details['title'])]){
                $value = html_entity_decode($values[strtolower($field_details['title'])]);
            }else{
                $value = $field_details['default_value'];
            }
            if( $field_details['contact_specific'] == 'yes' ){
                $OldValuesArr[$field_id]["id_" . (string)$ContactId] = $value;
            }else{
                $OldValuesArr[$field_id] = $value;
            }

        }
        $Contact = infusionsoft_get_contact_by_id_simple($contact_id,array('"FirstName"','"LastName"','"Email"'),$FromLocal);
        //if(isset($Contact[0]->Email)){
            $relation_id_arr = [];
            $relation_name_arr = explode(',', $relation_name);
            foreach ($relation_name_arr as $relation){
                $relationship_id = array_search(strtolower(trim($relation)),$relationships_map);
                if($relationship_id !== false){
                    $relation_id_arr[] = $relationship_id;
                }else{
                    //create relationship if not existing and return the id
                    $NewId = macanta_create_relationship($relation,'',$group_id);
                    $relation_id_arr[] = $NewId;
                    $relationships_map[$NewId] = $relation;
                }
            }
            $OldContactArr[$contact_id]=[
                "relationships"=>$relation_id_arr,
                "ContactId"=>$contact_id,
                "FirstName"=>$Contact[0]->FirstName,
                "LastName"=>$Contact[0]->LastName,
                "Email"=>$Contact[0]->Email

            ];
            $history_to[$contact_id] = [
                "Email"=>$Contact[0]->Email,
                "FirstName"=>$Contact[0]->FirstName,
                "LastName"=>$Contact[0]->LastName,
                "relationships"=>$relation_id_arr
            ];

        //}

    }

    $Update_result = "";
    $DBData['id'] = $ItemId;
    if($item_id_custom_field != "" && $FromLocal == false){
        $data = [];
        $data[] = ['name' => '_'.$item_id_custom_field, 'value' => $DBData['id']];
        $Update_result = infusionsoft_update_contact($data, $contact_id);
    }else{
        $Update_result = "No Custom Field Setup";
    }
    $DBData['group'] = $group_id;
    $DBData['value'] = json_encode($OldValuesArr);
    $DBData['connected_contact'] = json_encode($OldContactArr);
    $DBData['history'] = '{"update_history":[],"connection_history":[{"date":"'.date('Y-m-d H:i:s').'","to":'.json_encode($history_to).'}]}';
    $DBData['meta'] = json_encode($meta);
    $DBData['created'] = date('Y-m-d H:i:s');
    $DBData['updated'] = time();
    $DBData['status'] = 'active';
    $DBResults = $CI->db->insert('connected_data', $DBData);
    return ["DBData"=>$DBData, "QueryResult"=>$DBResults, "Update_result"=>$Update_result];
}
function macanta_add_update_connected_data($contactId=false, $group_id, $group_name, $values, $connected_contacts,$meta=false, $duplicate_option=false, $update_fields = true, $ItemId = "", $FromAdd = false, $FromLocal = false){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Results = '';
    $AllItemIds = [];
    $Updated = 'N/A';
    $Added = 'N/A';
    if($group_id == false || !empty($group_name)){

        if(!isset($GLOBALS['group_details_cache'][$group_name])){
            $group_details = macanta_get_connected_info_group_fields_map($group_name);
            $GLOBALS['group_details_cache'][$group_name] = $group_details;
        }else{
            $group_details = $GLOBALS['group_details_cache'][$group_name];
        }
        $group_id = isset($group_details['id']) ? $group_details['id']:false;
    }else{
        $group_details = macanta_get_connected_info_group_fields_map('',$group_id);
    }
    if($group_id === false) return false;

    if(isset($group_details['message'])) return $group_details['message'];

    $item_id_custom_field = $group_details['item_id_custom_field'];
    $data['values'] = [];
    $values = array_change_key_case($values,CASE_LOWER);

    if($duplicate_option !== false){
        if($ItemId != ""){
            $Updated = macanta_update_connected_data($contactId, $group_id, '', $values, $connected_contacts,$meta, false, false, $update_fields,$item_id_custom_field, $ItemId, false);
        }
        else{
            $check_field_name = strtolower($duplicate_option);

            $FieldId = false;
            if(isset($values[$check_field_name]) && $values[$check_field_name] != ""){
                $FieldValue = $values[$check_field_name];
                foreach ($group_details['fields'] as $field_id => $field_details ){

                    if(strtolower($check_field_name) == $field_details['title']){
                        $FieldId = $field_id;
                        break;
                    }
                }
                $Updated = macanta_update_connected_data($contactId, $group_id, "",  $values, $connected_contacts,$meta, $FieldId, $FieldValue, $update_fields,$item_id_custom_field, $ItemId, $FromAdd);
                if ($Updated == false) {
                    // Add Connected Data
                    $Added = macanta_add_connected_data($group_id, $group_details, $values, $connected_contacts, $meta,$item_id_custom_field);
                }
            }else{
                $Added = macanta_add_connected_data($group_id, $group_details, $values, $connected_contacts, $meta,$item_id_custom_field);
            }
        }

    }else{
        //Always Add if $duplicate_option is not set
        $Added = macanta_add_connected_data($group_id, $group_details, $values, $connected_contacts,$meta,$item_id_custom_field, $FromLocal);
    }
    $Results = ['Updated'=>$Updated, 'Added'=>$Added];


    return $Results;
}
function macanta_record_api_call($APICalled, $Input, $session_data=[], $Post = false){

    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $DataPassed = [
        'AppName' => $CI->config->item('IS_App_Name'),
        'APICalled' => $APICalled,
        'APIInput' => $Input,
        'session_data' => $session_data
    ];
    $PostData = http_build_query($DataPassed);
    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                "Content-type: application/x-www-form-urlencoded\r\n"
        ,
            'content' => $PostData
        )
    );
    if($Post == false){
        $DBdata = array(
            'user_id' => 0,
            'meta_key' => 'macanta_record_api_call',
            'meta_value' => json_encode($DataPassed)
        );
        //$CI->db->insert('users_meta', $DBdata);
    }else{
        $context  = stream_context_create($opts);
        return file_get_contents('https://getmacanta.org/payments/api_tracking.php', false, $context);
    }

}
function macanta_remove_country($text){
    $countries = array
    (
        'AF' => 'Afghanistan',
        'AX' => 'Aland Islands',
        'AL' => 'Albania',
        'DZ' => 'Algeria',
        'AS' => 'American Samoa',
        'AD' => 'Andorra',
        'AO' => 'Angola',
        'AI' => 'Anguilla',
        'AQ' => 'Antarctica',
        'AG' => 'Antigua And Barbuda',
        'AR' => 'Argentina',
        'AM' => 'Armenia',
        'AW' => 'Aruba',
        'AU' => 'Australia',
        'AT' => 'Austria',
        'AZ' => 'Azerbaijan',
        'BS' => 'Bahamas',
        'BH' => 'Bahrain',
        'BD' => 'Bangladesh',
        'BB' => 'Barbados',
        'BY' => 'Belarus',
        'BE' => 'Belgium',
        'BZ' => 'Belize',
        'BJ' => 'Benin',
        'BM' => 'Bermuda',
        'BT' => 'Bhutan',
        'BO' => 'Bolivia',
        'BA' => 'Bosnia And Herzegovina',
        'BW' => 'Botswana',
        'BV' => 'Bouvet Island',
        'BR' => 'Brazil',
        'IO' => 'British Indian Ocean Territory',
        'BN' => 'Brunei Darussalam',
        'BG' => 'Bulgaria',
        'BF' => 'Burkina Faso',
        'BI' => 'Burundi',
        'KH' => 'Cambodia',
        'CM' => 'Cameroon',
        'CA' => 'Canada',
        'CV' => 'Cape Verde',
        'KY' => 'Cayman Islands',
        'CF' => 'Central African Republic',
        'TD' => 'Chad',
        'CL' => 'Chile',
        'CN' => 'China',
        'CX' => 'Christmas Island',
        'CC' => 'Cocos (Keeling) Islands',
        'CO' => 'Colombia',
        'KM' => 'Comoros',
        'CG' => 'Congo',
        'CD' => 'Congo, Democratic Republic',
        'CK' => 'Cook Islands',
        'CR' => 'Costa Rica',
        'CI' => 'Cote D\'Ivoire',
        'HR' => 'Croatia',
        'CU' => 'Cuba',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
        'DJ' => 'Djibouti',
        'DM' => 'Dominica',
        'DO' => 'Dominican Republic',
        'EC' => 'Ecuador',
        'EG' => 'Egypt',
        'SV' => 'El Salvador',
        'GQ' => 'Equatorial Guinea',
        'ER' => 'Eritrea',
        'EE' => 'Estonia',
        'ET' => 'Ethiopia',
        'FK' => 'Falkland Islands (Malvinas)',
        'FO' => 'Faroe Islands',
        'FJ' => 'Fiji',
        'FI' => 'Finland',
        'FR' => 'France',
        'GF' => 'French Guiana',
        'PF' => 'French Polynesia',
        'TF' => 'French Southern Territories',
        'GA' => 'Gabon',
        'GM' => 'Gambia',
        'GE' => 'Georgia',
        'DE' => 'Germany',
        'GH' => 'Ghana',
        'GI' => 'Gibraltar',
        'GR' => 'Greece',
        'GL' => 'Greenland',
        'GD' => 'Grenada',
        'GP' => 'Guadeloupe',
        'GU' => 'Guam',
        'GT' => 'Guatemala',
        'GG' => 'Guernsey',
        'GN' => 'Guinea',
        'GW' => 'Guinea-Bissau',
        'GY' => 'Guyana',
        'HT' => 'Haiti',
        'HM' => 'Heard Island & Mcdonald Islands',
        'VA' => 'Holy See (Vatican City State)',
        'HN' => 'Honduras',
        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
        'IS' => 'Iceland',
        'IN' => 'India',
        'ID' => 'Indonesia',
        'IR' => 'Iran, Islamic Republic Of',
        'IQ' => 'Iraq',
        'IE' => 'Ireland',
        'IM' => 'Isle Of Man',
        'IL' => 'Israel',
        'IT' => 'Italy',
        'JM' => 'Jamaica',
        'JP' => 'Japan',
        'JE' => 'Jersey',
        'JO' => 'Jordan',
        'KZ' => 'Kazakhstan',
        'KE' => 'Kenya',
        'KI' => 'Kiribati',
        'KR' => 'Korea',
        'KW' => 'Kuwait',
        'KG' => 'Kyrgyzstan',
        'LA' => 'Lao People\'s Democratic Republic',
        'LV' => 'Latvia',
        'LB' => 'Lebanon',
        'LS' => 'Lesotho',
        'LR' => 'Liberia',
        'LY' => 'Libyan Arab Jamahiriya',
        'LI' => 'Liechtenstein',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
        'MO' => 'Macao',
        'MK' => 'Macedonia',
        'MG' => 'Madagascar',
        'MW' => 'Malawi',
        'MY' => 'Malaysia',
        'MV' => 'Maldives',
        'ML' => 'Mali',
        'MT' => 'Malta',
        'MH' => 'Marshall Islands',
        'MQ' => 'Martinique',
        'MR' => 'Mauritania',
        'MU' => 'Mauritius',
        'YT' => 'Mayotte',
        'MX' => 'Mexico',
        'FM' => 'Micronesia, Federated States Of',
        'MD' => 'Moldova',
        'MC' => 'Monaco',
        'MN' => 'Mongolia',
        'ME' => 'Montenegro',
        'MS' => 'Montserrat',
        'MA' => 'Morocco',
        'MZ' => 'Mozambique',
        'MM' => 'Myanmar',
        'NA' => 'Namibia',
        'NR' => 'Nauru',
        'NP' => 'Nepal',
        'NL' => 'Netherlands',
        'AN' => 'Netherlands Antilles',
        'NC' => 'New Caledonia',
        'NZ' => 'New Zealand',
        'NI' => 'Nicaragua',
        'NE' => 'Niger',
        'NG' => 'Nigeria',
        'NU' => 'Niue',
        'NF' => 'Norfolk Island',
        'MP' => 'Northern Mariana Islands',
        'NO' => 'Norway',
        'OM' => 'Oman',
        'PK' => 'Pakistan',
        'PW' => 'Palau',
        'PS' => 'Palestinian Territory, Occupied',
        'PA' => 'Panama',
        'PG' => 'Papua New Guinea',
        'PY' => 'Paraguay',
        'PE' => 'Peru',
        'PH' => 'Philippines',
        'PN' => 'Pitcairn',
        'PL' => 'Poland',
        'PT' => 'Portugal',
        'PR' => 'Puerto Rico',
        'QA' => 'Qatar',
        'RE' => 'Reunion',
        'RO' => 'Romania',
        'RU' => 'Russian Federation',
        'RW' => 'Rwanda',
        'BL' => 'Saint Barthelemy',
        'SH' => 'Saint Helena',
        'KN' => 'Saint Kitts And Nevis',
        'LC' => 'Saint Lucia',
        'MF' => 'Saint Martin',
        'PM' => 'Saint Pierre And Miquelon',
        'VC' => 'Saint Vincent And Grenadines',
        'WS' => 'Samoa',
        'SM' => 'San Marino',
        'ST' => 'Sao Tome And Principe',
        'SA' => 'Saudi Arabia',
        'SN' => 'Senegal',
        'RS' => 'Serbia',
        'SC' => 'Seychelles',
        'SL' => 'Sierra Leone',
        'SG' => 'Singapore',
        'SK' => 'Slovakia',
        'SI' => 'Slovenia',
        'SB' => 'Solomon Islands',
        'SO' => 'Somalia',
        'ZA' => 'South Africa',
        'GS' => 'South Georgia And Sandwich Isl.',
        'ES' => 'Spain',
        'LK' => 'Sri Lanka',
        'SD' => 'Sudan',
        'SR' => 'Suriname',
        'SJ' => 'Svalbard And Jan Mayen',
        'SZ' => 'Swaziland',
        'SE' => 'Sweden',
        'CH' => 'Switzerland',
        'SY' => 'Syrian Arab Republic',
        'TW' => 'Taiwan',
        'TJ' => 'Tajikistan',
        'TZ' => 'Tanzania',
        'TH' => 'Thailand',
        'TL' => 'Timor-Leste',
        'TG' => 'Togo',
        'TK' => 'Tokelau',
        'TO' => 'Tonga',
        'TT' => 'Trinidad And Tobago',
        'TN' => 'Tunisia',
        'TR' => 'Turkey',
        'TM' => 'Turkmenistan',
        'TC' => 'Turks And Caicos Islands',
        'TV' => 'Tuvalu',
        'UG' => 'Uganda',
        'UA' => 'Ukraine',
        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
        'US' => 'United States',
        'UM' => 'United States Outlying Islands',
        'UY' => 'Uruguay',
        'UZ' => 'Uzbekistan',
        'VU' => 'Vanuatu',
        'VE' => 'Venezuela',
        'VN' => 'Viet Nam',
        'VG' => 'Virgin Islands, British',
        'VI' => 'Virgin Islands, U.S.',
        'WF' => 'Wallis And Futuna',
        'EH' => 'Western Sahara',
        'YE' => 'Yemen',
        'ZM' => 'Zambia',
        'ZW' => 'Zimbabwe',
    );
    foreach ($countries as $ISO2 => $Country){
        $text = str_replace($Country,'',$text);
    }
    return $text;
}
function macanta_make_api_call_phone_verification($Phone,$CountryFullName=""){
    $Key = $_SERVER['LOQATE_API_KEY'];
    $countries = array
    (
        'AF' => 'Afghanistan',
        'AX' => 'Aland Islands',
        'AL' => 'Albania',
        'DZ' => 'Algeria',
        'AS' => 'American Samoa',
        'AD' => 'Andorra',
        'AO' => 'Angola',
        'AI' => 'Anguilla',
        'AQ' => 'Antarctica',
        'AG' => 'Antigua And Barbuda',
        'AR' => 'Argentina',
        'AM' => 'Armenia',
        'AW' => 'Aruba',
        'AU' => 'Australia',
        'AT' => 'Austria',
        'AZ' => 'Azerbaijan',
        'BS' => 'Bahamas',
        'BH' => 'Bahrain',
        'BD' => 'Bangladesh',
        'BB' => 'Barbados',
        'BY' => 'Belarus',
        'BE' => 'Belgium',
        'BZ' => 'Belize',
        'BJ' => 'Benin',
        'BM' => 'Bermuda',
        'BT' => 'Bhutan',
        'BO' => 'Bolivia',
        'BA' => 'Bosnia And Herzegovina',
        'BW' => 'Botswana',
        'BV' => 'Bouvet Island',
        'BR' => 'Brazil',
        'IO' => 'British Indian Ocean Territory',
        'BN' => 'Brunei Darussalam',
        'BG' => 'Bulgaria',
        'BF' => 'Burkina Faso',
        'BI' => 'Burundi',
        'KH' => 'Cambodia',
        'CM' => 'Cameroon',
        'CA' => 'Canada',
        'CV' => 'Cape Verde',
        'KY' => 'Cayman Islands',
        'CF' => 'Central African Republic',
        'TD' => 'Chad',
        'CL' => 'Chile',
        'CN' => 'China',
        'CX' => 'Christmas Island',
        'CC' => 'Cocos (Keeling) Islands',
        'CO' => 'Colombia',
        'KM' => 'Comoros',
        'CG' => 'Congo',
        'CD' => 'Congo, Democratic Republic',
        'CK' => 'Cook Islands',
        'CR' => 'Costa Rica',
        'CI' => 'Cote D\'Ivoire',
        'HR' => 'Croatia',
        'CU' => 'Cuba',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
        'DJ' => 'Djibouti',
        'DM' => 'Dominica',
        'DO' => 'Dominican Republic',
        'EC' => 'Ecuador',
        'EG' => 'Egypt',
        'SV' => 'El Salvador',
        'GQ' => 'Equatorial Guinea',
        'ER' => 'Eritrea',
        'EE' => 'Estonia',
        'ET' => 'Ethiopia',
        'FK' => 'Falkland Islands (Malvinas)',
        'FO' => 'Faroe Islands',
        'FJ' => 'Fiji',
        'FI' => 'Finland',
        'FR' => 'France',
        'GF' => 'French Guiana',
        'PF' => 'French Polynesia',
        'TF' => 'French Southern Territories',
        'GA' => 'Gabon',
        'GM' => 'Gambia',
        'GE' => 'Georgia',
        'DE' => 'Germany',
        'GH' => 'Ghana',
        'GI' => 'Gibraltar',
        'GR' => 'Greece',
        'GL' => 'Greenland',
        'GD' => 'Grenada',
        'GP' => 'Guadeloupe',
        'GU' => 'Guam',
        'GT' => 'Guatemala',
        'GG' => 'Guernsey',
        'GN' => 'Guinea',
        'GW' => 'Guinea-Bissau',
        'GY' => 'Guyana',
        'HT' => 'Haiti',
        'HM' => 'Heard Island & Mcdonald Islands',
        'VA' => 'Holy See (Vatican City State)',
        'HN' => 'Honduras',
        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
        'IS' => 'Iceland',
        'IN' => 'India',
        'ID' => 'Indonesia',
        'IR' => 'Iran, Islamic Republic Of',
        'IQ' => 'Iraq',
        'IE' => 'Ireland',
        'IM' => 'Isle Of Man',
        'IL' => 'Israel',
        'IT' => 'Italy',
        'JM' => 'Jamaica',
        'JP' => 'Japan',
        'JE' => 'Jersey',
        'JO' => 'Jordan',
        'KZ' => 'Kazakhstan',
        'KE' => 'Kenya',
        'KI' => 'Kiribati',
        'KR' => 'Korea',
        'KW' => 'Kuwait',
        'KG' => 'Kyrgyzstan',
        'LA' => 'Lao People\'s Democratic Republic',
        'LV' => 'Latvia',
        'LB' => 'Lebanon',
        'LS' => 'Lesotho',
        'LR' => 'Liberia',
        'LY' => 'Libyan Arab Jamahiriya',
        'LI' => 'Liechtenstein',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
        'MO' => 'Macao',
        'MK' => 'Macedonia',
        'MG' => 'Madagascar',
        'MW' => 'Malawi',
        'MY' => 'Malaysia',
        'MV' => 'Maldives',
        'ML' => 'Mali',
        'MT' => 'Malta',
        'MH' => 'Marshall Islands',
        'MQ' => 'Martinique',
        'MR' => 'Mauritania',
        'MU' => 'Mauritius',
        'YT' => 'Mayotte',
        'MX' => 'Mexico',
        'FM' => 'Micronesia, Federated States Of',
        'MD' => 'Moldova',
        'MC' => 'Monaco',
        'MN' => 'Mongolia',
        'ME' => 'Montenegro',
        'MS' => 'Montserrat',
        'MA' => 'Morocco',
        'MZ' => 'Mozambique',
        'MM' => 'Myanmar',
        'NA' => 'Namibia',
        'NR' => 'Nauru',
        'NP' => 'Nepal',
        'NL' => 'Netherlands',
        'AN' => 'Netherlands Antilles',
        'NC' => 'New Caledonia',
        'NZ' => 'New Zealand',
        'NI' => 'Nicaragua',
        'NE' => 'Niger',
        'NG' => 'Nigeria',
        'NU' => 'Niue',
        'NF' => 'Norfolk Island',
        'MP' => 'Northern Mariana Islands',
        'NO' => 'Norway',
        'OM' => 'Oman',
        'PK' => 'Pakistan',
        'PW' => 'Palau',
        'PS' => 'Palestinian Territory, Occupied',
        'PA' => 'Panama',
        'PG' => 'Papua New Guinea',
        'PY' => 'Paraguay',
        'PE' => 'Peru',
        'PH' => 'Philippines',
        'PN' => 'Pitcairn',
        'PL' => 'Poland',
        'PT' => 'Portugal',
        'PR' => 'Puerto Rico',
        'QA' => 'Qatar',
        'RE' => 'Reunion',
        'RO' => 'Romania',
        'RU' => 'Russian Federation',
        'RW' => 'Rwanda',
        'BL' => 'Saint Barthelemy',
        'SH' => 'Saint Helena',
        'KN' => 'Saint Kitts And Nevis',
        'LC' => 'Saint Lucia',
        'MF' => 'Saint Martin',
        'PM' => 'Saint Pierre And Miquelon',
        'VC' => 'Saint Vincent And Grenadines',
        'WS' => 'Samoa',
        'SM' => 'San Marino',
        'ST' => 'Sao Tome And Principe',
        'SA' => 'Saudi Arabia',
        'SN' => 'Senegal',
        'RS' => 'Serbia',
        'SC' => 'Seychelles',
        'SL' => 'Sierra Leone',
        'SG' => 'Singapore',
        'SK' => 'Slovakia',
        'SI' => 'Slovenia',
        'SB' => 'Solomon Islands',
        'SO' => 'Somalia',
        'ZA' => 'South Africa',
        'GS' => 'South Georgia And Sandwich Isl.',
        'ES' => 'Spain',
        'LK' => 'Sri Lanka',
        'SD' => 'Sudan',
        'SR' => 'Suriname',
        'SJ' => 'Svalbard And Jan Mayen',
        'SZ' => 'Swaziland',
        'SE' => 'Sweden',
        'CH' => 'Switzerland',
        'SY' => 'Syrian Arab Republic',
        'TW' => 'Taiwan',
        'TJ' => 'Tajikistan',
        'TZ' => 'Tanzania',
        'TH' => 'Thailand',
        'TL' => 'Timor-Leste',
        'TG' => 'Togo',
        'TK' => 'Tokelau',
        'TO' => 'Tonga',
        'TT' => 'Trinidad And Tobago',
        'TN' => 'Tunisia',
        'TR' => 'Turkey',
        'TM' => 'Turkmenistan',
        'TC' => 'Turks And Caicos Islands',
        'TV' => 'Tuvalu',
        'UG' => 'Uganda',
        'UA' => 'Ukraine',
        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
        'US' => 'United States',
        'UM' => 'United States Outlying Islands',
        'UY' => 'Uruguay',
        'UZ' => 'Uzbekistan',
        'VU' => 'Vanuatu',
        'VE' => 'Venezuela',
        'VN' => 'Viet Nam',
        'VG' => 'Virgin Islands, British',
        'VI' => 'Virgin Islands, U.S.',
        'WF' => 'Wallis And Futuna',
        'EH' => 'Western Sahara',
        'YE' => 'Yemen',
        'ZM' => 'Zambia',
        'ZW' => 'Zimbabwe',
    );
    //Get ISO2 from Country FullName
    $ISO2 = array_search(trim($CountryFullName), $countries); // $key = 2;
    $ISO2 = $ISO2 !== false ? $ISO2:"";
    $pa = new PhoneNumberValidation_Interactive_Validate_v2_20 ($Key,$Phone,$ISO2);



    try {
        $pa->MakeRequest();
    } catch (Exception $e) {
        $Error = $e->getMessage();
        return ["Error"=>$Error];
    }

    if ($pa->HasData())
    {
        $data = $pa->HasData();
        $data = json_decode(json_encode($data[0]), true);
        return [
            "PhoneNumberPassed"=>$Phone,
            "PhoneNumber"=>$data['PhoneNumber'][0],
            "IsValid"=>$data['IsValid'][0],
            "NetworkCode"=>$data['NetworkCode'][0],
            "NetworkName"=>$data['NetworkName'][0],
            "NetworkCountry"=>$data['NetworkCountry'][0],
            "NationalFormat"=>$data['NationalFormat'][0],
            "CountryPrefix"=>$data['CountryPrefix'][0],
            "NumberType"=>$data['NumberType'][0]
        ];
    }else{
        return ["IsValid"=>'NoData'];
    }
}
function macanta_make_api_call_email_verification($Email){
    $Key = $_SERVER['MB_API_KEY'];
    $url = "http://apilayer.net/api/check?";
    $url .= "access_key=" . $Key;
    $url .= "&email=" . $Email;
    $url .= "&smtp=1";
    $url .= "&format=1";
    $json = file_get_contents($url);
    return json_decode($json, true);
}
function macanta_make_api_call_full_contact($SearchInput,$InputType='email'){
    $action = 'https://api.fullcontact.com/v3/person.enrich';
    $api_key = $_SERVER['FC_API_KEY'];
    $Data = [$InputType=>$SearchInput];
    $ch = curl_init($action);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($Data));
    $headers = [
        'Authorization: Bearer '.$api_key
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $Results = curl_exec($ch);
    curl_close($ch);
    return json_decode($Results, true);
}
function macanta_full_contact($ContactId, $SearchInput, $session_data = []){
    $CI =& get_instance();
    //Check if DVE Enabled
    $SearchInput = trim($SearchInput);
    $FullContactRecord = [];
    $FC_expiration = ' +30 days';
    $DVE = "enabled"; // Data should always pull
    if($DVE && $DVE == "enabled"){
        //Check if this PhoneNumber is Already Validated
        $CI->db->where('user_id',0);
        $CI->db->where('meta_key','FullContactRecord');
        $CI->db->like('meta_value', '{"' . $SearchInput . '":{');
        $query = $CI->db->get('users_meta');
        $row = $query->row();
        $DBdata = [];
        if (isset($row))
        {
            $id = $row->id;
            $FullContactRecord = json_decode($row->meta_value,true);
            //check last updated
            $LastUpdated = $FullContactRecord[$SearchInput]['updated'];
            $Today = time();
            //if(strtotime($LastUpdated.$FC_expiration) > $Today){
                return $FullContactRecord[$SearchInput];
            /*}else{
                $FullContactResults =   macanta_make_api_call_full_contact($SearchInput);
                macanta_record_api_call("full_contact",$SearchInput,$session_data);
                $FullContactRecord[$SearchInput] = $FullContactResults;
                $DBdata['meta_value'] = json_encode($FullContactRecord);
                $CI->db->where('id',$id);
                $CI->db->update('users_meta', $DBdata);
                return $FullContactRecord[$SearchInput];
            }*/

        }else{
            return [
                "IsValid"=>'Unable'
            ];
            /*$FullContactResults =   macanta_make_api_call_full_contact($SearchInput);
            macanta_record_api_call("full_contact",$SearchInput,$session_data);
            if(isset($FullContactResults['fullName'])){
                $FullContactRecord[$SearchInput] = $FullContactResults;
                //Insert into Database
                $DBdata['user_id'] = 0;
                $DBdata['meta_key'] = 'FullContactRecord';
                $DBdata['meta_value'] = json_encode($FullContactRecord);
                $CI->db->insert('users_meta', $DBdata);
                return $FullContactRecord[$SearchInput];
            }*/

        }


    }else{
        return [
            "IsValid"=>'Unable'
        ];
    }


}
function macanta_validate_email($Email, $session_data = []){
    $CI =& get_instance();
    //Check if DVE Enabled
    $Email = trim($Email);
    $ContactId = "0"; // for global settings
    //$DVE = macanta_validate_feature('DVE');

    $AppsName = $CI->config->item('IS_App_Name');
    $DisabledApps=['al347'];

    $DVE = 'enabled';
    $ValidatedEmails = [];
    $APIExpiration = ' +30 days';
    if($DVE && $DVE == "enabled"){

        //Check if this PhoneNumber is Already Validated
        $CI->db->where('user_id',0);
        $CI->db->where('meta_key','ValidatedEmails');
        $CI->db->like('meta_value', '{"' . $Email . '":{');
        $query = $CI->db->get('users_meta');
        $row = $query->row();
        $DBdata = [];
        if (isset($row))
        {
            $id = $row->id;
            $ValidatedEmails = json_decode($row->meta_value,true);
            $LastUpdated = $ValidatedEmails[$Email]['updated'];
            $Today = time();
            if((strtotime($LastUpdated.$APIExpiration) > $Today || in_array($AppsName,$DisabledApps)) && !isset($ValidatedEmails[$Email]['error'])){
                return $ValidatedEmails[$Email];

            }else{
                $ValidateResults =   macanta_make_api_call_email_verification($Email);
                macanta_record_api_call("email_validation",$Email,$session_data);
                $ValidateResults['updated'] = date('Y-m-d');
                $ValidatedEmails[$Email] = $ValidateResults;
                $DBdata['meta_value'] = json_encode($ValidatedEmails);
                $CI->db->where('id',$id);
                $CI->db->update('users_meta', $DBdata);
                return $ValidatedEmails[$Email];
            }

        }elseif(in_array($AppsName,$DisabledApps)){
            return [
                "format_valid"=>'Unable'
            ];
        }else{
            $ValidateResults =   macanta_make_api_call_email_verification($Email);
            macanta_record_api_call("email_validation",$Email,$session_data);
            $ValidateResults['updated'] = date('Y-m-d');
            $ValidatedEmails[$Email] = $ValidateResults;
            $DBdata['user_id'] = 0;
            $DBdata['meta_key'] = 'ValidatedEmails';
            $DBdata['meta_value'] = json_encode($ValidatedEmails);
            $CI->db->insert('users_meta', $DBdata);
            return $ValidatedEmails[$Email];

        }


    }else{
        return [
            "format_valid"=>'Unable'
        ];
    }
}
function macanta_validate_phone_number($ContactId, $Phone,$CountryFullName="", $session_data = []){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');

    $AppsName = $CI->config->item('IS_App_Name');
    $DisabledApps=['al347'];

    //Check if DVE Enabled
    /* Sanitize Pone*/
    if ($CountryFullName) {
        $d_code = getCountryCode($CountryFullName);
    }
    elseif($CI->config->item('country_code')) {
        $d_code = $CI->config->item('country_code');
    }else{
        $AppCountryCode = infusionsoft_get_app_account_profile()->message->address->country_code;
        $d_code = getCountryCode($AppCountryCode, false, true);
    }
    $PhoneInfo = SanitizePhone($Phone);
    $APIExpiration = ' +30 days';
    $TheCode = $PhoneInfo["Code"] != '' ? $PhoneInfo["Code"] : $d_code;
    $Phone = $TheCode . $PhoneInfo["Phone"];
    //$DVE = macanta_validate_feature('DVE');
    $DVE = "enabled"; // set to always enable;
    $ValidataPhones = [];
    $DBdata = [];
    if($DVE && $DVE == "enabled"){
        //Check if this PhoneNumber is Already Validated
        $CI->db->where('user_id',0);
        $CI->db->where('meta_key','ValidatedPhones');
        $CI->db->like('meta_value', '{"' . $Phone . '":{');
        $query = $CI->db->get('users_meta');
        //print_r($CI->db->last_query());
        //print_r($CI->db->error());
        $row = $query->row();
        if (isset($row))
        {
            $id = $row->id;
            $ValidataPhones = json_decode($row->meta_value,true);
            $LastUpdated = $ValidataPhones[$Phone]['updated'];
            $Today = time();

            if(strtotime($LastUpdated.$APIExpiration) > $Today || in_array($AppsName,$DisabledApps)){
                return $ValidataPhones[$Phone];
            }else{
                $ValidateResults =   macanta_make_api_call_phone_verification($Phone,$CountryFullName);
                if(!isset($ValidateResults['Error'])){
                    macanta_record_api_call("phone_validation",$Phone,$session_data);
                    $ValidateResults['updated'] = date('Y-m-d');
                    $ValidataPhones[$Phone] = $ValidateResults;
                    $DBdata['meta_value'] = json_encode($ValidataPhones);
                    $CI->db->where('id',$id);
                    $CI->db->update('users_meta', $DBdata);
                    return $ValidataPhones[$Phone];
                }else{
                    return [
                        "IsValid"=>'Unable',
                        "Error"=>$ValidateResults['Error']
                    ];
                }

            }

        }elseif(in_array($AppsName,$DisabledApps)){
            return [
                "IsValid"=>'Unable'
            ];
        }
        else{
            $ValidateResults =   macanta_make_api_call_phone_verification($Phone,$CountryFullName);
            if(!isset($ValidateResults['Error'])){
                macanta_record_api_call("phone_validation",$Phone,$session_data);
                $ValidateResults['updated'] = date('Y-m-d');
                $ValidataPhones[$Phone] = $ValidateResults;
                $DBdata['user_id'] = 0;
                $DBdata['meta_key'] = 'ValidatedPhones';
                $DBdata['meta_value'] = json_encode($ValidataPhones);
                $CI->db->insert('users_meta', $DBdata);
                return $ValidataPhones[$Phone];
            }else{
                return [
                    "IsValid"=>'Unable',
                    "Error"=>$ValidateResults['Error']
                ];
            }


        }


    }else{
        return [
            "IsValid"=>'Unable'
        ];
    }


}
function macanta_get_verified_address($Text=""){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    //$AppCountryCode = infusionsoft_get_app_account_profile()->message->address->country_code;
    $AppCountryCode = ''; // disabled default country
    $Key = $_SERVER['LOQATE_API_KEY'];
    if($Text !== ""){
        $pa = new Capture_Interactive_Find_v1_00 ($Key,$Text,"","","$AppCountryCode","","");
        try {
            $pa->MakeRequest();
        } catch (Exception $e) {
            $Error = $e->getMessage();
            return ["Error"=>$Error];
        }
        $Addresses = '<ul class="addressList">';
        if ($pa->HasData()) {
            $data = $pa->HasData();
            foreach ($data as $item) {
                if (strpos($item["Description"], 'Addresses') > -1){

                    $Addresses .= '<li data-postcode="'.$Text.'" data-container="'.$item["Id"].'" data-type="'.$item["Type"].'"  class="postcodeItem sub_postcode">'.$item["Text"].' '.$item["Description"].'</a></li>';

                }else{
                    $Addresses .= '<li data-postcode="'.$Text.'" data-container="'.$item["Id"].'" data-type="'.$item["Type"].'" class="postcodeItem sub_postcode_singular">'.$item["Text"].' '.$item["Description"].'</li>';

                }
            }
        }
        $Addresses .= '</ul>';
        return $Addresses;
    }
    return "";
}
function macanta_get_valid_addresses($Text="",$Container="",$Country="") {
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    //$AppCountryCode = infusionsoft_get_app_account_profile()->message->address->country_code;
    $AppCountryCode = "";
    if($Country != ""){
        $CountryInfo = macanta_get_country_iso2($Country);
        if($CountryInfo){
            $AppCountryCode = $CountryInfo['code'];
        }
    }
    $Key = $_SERVER['LOQATE_API_KEY'];
    $Addresses = [];
    $Text = macanta_remove_country($Text);
    if($Text !== "" || $Container !==""){
        if($Container){
            $pa = new Capture_Interactive_Find_v1_00 ($Key,$Text, $Container,"","$AppCountryCode",100,"");
        }else{
            $pa = new Capture_Interactive_Find_v1_00 ($Key,$Text,"","","$AppCountryCode",100,"");
        }
        try {
            $pa->MakeRequest();
        } catch (Exception $e) {
            $Error = $e->getMessage();
            return ["Error"=>$Error];
        }
        if ($pa->HasData()) {
            $data = $pa->HasData();
            foreach ($data as $item) {
                $Description = (string) $item["Description"];
                if (strpos($Description, 'Addresses') > -1){
                    $DescriptionArr = explode('-',$Description);
                    $Addresses[] = ["id"=>(string) $item["Id"],"type"=>(string) $item["Type"],"class"=>"group", "text"=>(string) $item["Text"], "description"=>$DescriptionArr[0], "count"=>$DescriptionArr[1]];

                }else{
                    $Addresses[] = ["id"=>(string) $item["Id"],"type"=>(string) $item["Type"],"class"=>"item", "text"=>(string) $item["Text"], "description"=>(string) $item["Description"]];

                }
            }
        }
        return $Addresses;
    }
    return "";
}
function macanta_get_valid_address($SearchId=""){
    $Key = $_SERVER['LOQATE_API_KEY'];
    $pa = new Capture_Interactive_Retrieve_v1_00 ($Key,$SearchId,"","","","","","","","","","","","","","","","","","","","");
    try {
        $pa->MakeRequest();
    } catch (Exception $e) {
        $Error = $e->getMessage();
        return ["Error"=>$Error];
    }
    if ($pa->HasData())
    {
        $data = $pa->HasData();
        $AddressDetails['StreetAddress1'] = trim($data[0]["Line1"][0]);
        $AddressDetails['StreetAddress2'] = trim($data[0]["Line2"][0]);
        $AddressDetails['City'] = trim($data[0]["City"][0]);
        $AddressDetails['State'] = trim($data[0]["ProvinceName"][0]);
        $AddressDetails['Country'] = trim($data[0]["CountryName"][0]);
        $AddressDetails['PostalCode'] = trim($data[0]["PostalCode"][0]);
        $AddressDetails['FullData'] = $data;
    }
    return $AddressDetails;
}
function macanta_check_viewed_media_presentation($ContactId,$CurrentMedia=[]){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Title = $CurrentMedia['Title'];

    if(!$Title) return false;

    $CI->db->where('user_id',$ContactId);
    $CI->db->where('meta_key',"MediaPresentation");
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        $MediaPresentationRecord = json_decode($row->meta_value,true);
        if(isset($MediaPresentationRecord[$Title])){
            return true;
        }else{
            $MediaPresentationRecord[$Title] = $CurrentMedia;
            //Update Database
            $CI->db->where('user_id',$ContactId);
            $CI->db->where('meta_key','MediaPresentation');
            $DBdata['meta_value'] = json_encode($MediaPresentationRecord);
            $CI->db->update('users_meta', $DBdata);
            return false;
        }

    }else{
        $MediaPresentationRecord[$Title] = $CurrentMedia;
        //Insert into Database
        $DBdata['user_id'] = $ContactId;
        $DBdata['meta_key'] = 'MediaPresentation';
        $DBdata['meta_value'] = json_encode($MediaPresentationRecord);
        $CI->db->insert('users_meta', $DBdata);
        return false;

    }
}
function macanta_update_user_seession_data($session_name,$session_field,$session_value){
    $current_session_data = unserialize(macanta_get_user_seession_data($session_name,'session_data'));
    $current_session_data[$session_field] = $session_value;
    $CI =& get_instance();
    $UserSessions = [];
    $CI->db->where('session_name',$session_name);
    $UserSessions['session_data	'] = serialize($current_session_data);
    $CI->db->update('user_sessions',$UserSessions);
}
function macanta_get_user_seession_data_by_email($email,$session_field=''){
    $CI =& get_instance();
    $CI->db->where('login_email',$email);
    $query = $CI->db->get('user_sessions');
    $row = $query->last_row();
    if (isset($row))
    {
        $session_name = $row->session_name;
        $ttl = $row->session_ttl;
        $time = $row->session_started;
        $theTime = time();
        $lapse = $theTime - $time;
        if($ttl == 0){
            macanta_set_user_last_access($session_name);
            return $session_field == '' ? $row:$row->$session_field;
        }
        if ($lapse < $ttl) {
            macanta_set_user_last_access($session_name);
            return $session_field == '' ? $row:$row->$session_field;
        } else {
            macanta_remove_user_session($session_name);
            return false;
        }
    }
    return false;
}
function macanta_get_user_seession_data($session_name,$session_field=''){
    $CI =& get_instance();
    $CI->load->dbforge();
    $table = "user_sessions";
    $fields = array(
        'id' => array( 'type' => 'INT','auto_increment' => TRUE ),
        'infusionsoft_id' => array( 'type' => 'INT' ,'constraint' => 11, ),
        'login_email' => array( 'type' => 'TEXT' ,'null' => TRUE ),
        'last_access' => array( 'type' => 'INT' ,'constraint' => 11, ),
        'session_name' => array( 'type' => 'TEXT' ,'null' => TRUE ),
        'session_started' => array( 'type' => 'INT' ,'constraint' => 11, ),
        'session_ttl' => array( 'type' => 'INT' ,'constraint' => 11,'default' => 86400 ),
        'session_data' => array( 'type' => 'LONGTEXT' ,'null' => TRUE ),
        'recent_actions' => array( 'type' => 'VARCHAR' ,'constraint' => '500','default' => '[]' ),
    );
    $CI->dbforge->add_field($fields);
    $CI->dbforge->add_key('id', TRUE);
    $CI->dbforge->create_table($table, true);
    $CI->db->where('session_name',$session_name);
    $query = $CI->db->get('user_sessions');
    $row = $query->row();
    if (isset($row))
    {
        $ttl = $row->session_ttl;
        $time = $row->session_started;
        $theTime = time();
        $lapse = $theTime - $time;
        if($ttl == 0){
            macanta_set_user_last_access($session_name);
            return $session_field == '' ? $row:$row->$session_field;
        }
        if ($lapse < $ttl) {
            macanta_set_user_last_access($session_name);
            return $session_field == '' ? $row:$row->$session_field;
        } else {
            macanta_remove_user_session($session_name);
            return false;
        }
    }
    return false;
}
function macanta_get_stripe_customer_status(){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $DataPassed = [
        'AppName' => $CI->config->item('IS_App_Name')
    ];
    $PostData = http_build_query($DataPassed);
    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                "Content-type: application/x-www-form-urlencoded\r\n"
        ,
            'content' => $PostData
        )
    );
    $context  = stream_context_create($opts);
    return file_get_contents('https://getmacanta.org/payments/check_customer.php', false, $context);
}
function macanta_validate_feature($Feature){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    if($CI->config->item('MacantaFeatures')){
        $MacantaFeatures = json_decode($CI->config->item('MacantaFeatures'),true);
        if(isset($MacantaFeatures['app_features'][$Feature])){
            $Status = $MacantaFeatures['app_features'][$Feature]['Status'];
            $Return = false;
            switch ($Status){
                case 'trial':
                    $EndDate = $MacantaFeatures['app_features'][$Feature]['TrialEndDate'];
                    if(strtotime($EndDate." 08:00:00") > time()) $Return = 'enabled';
                    break;
                case 'enabled':
                    $Return = 'enabled';
                    break;
                default:

                    break;
            }
            return $Return;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
function macanta_get_app_features($AppName = ''){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $DataPassed = [
        'AppName' => $AppName != '' ? $AppName:$CI->config->item('IS_App_Name')
    ];
    $PostData = http_build_query($DataPassed);
    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                "Content-type: application/x-www-form-urlencoded\r\n"
        ,
            'content' => $PostData
        )
    );
    $context  = stream_context_create($opts);
    return file_get_contents('https://getmacanta.org/payments/app_features.php', false, $context);
}
function macanta_set_user_last_access($session_name){
    $CI =& get_instance();
    $CI->db->where('session_name',$session_name);
    $UserSessions['last_access'] = time();
    $CI->db->update('user_sessions',$UserSessions);
}
function macanta_has_user_last_action($session_name, $action){
    $recent_actions = json_decode(macanta_get_user_seession_data($session_name,'recent_actions'),true);
    return in_array($action, $recent_actions);
}
function macanta_remove_user_last_action($session_name, $action){
    $CI =& get_instance();
    $recent_actions = json_decode(macanta_get_user_seession_data($session_name,'recent_actions'),true);
    $key = array_search($action, $recent_actions);
    unset($recent_actions[$key]);
    $CI->db->where('session_name',$session_name);
    $UserSessions['recent_actions'] = json_encode($recent_actions);
    $CI->db->update('user_sessions',$UserSessions);
    return true;
}
function macanta_add_user_last_action($session_name, $action){
    $CI =& get_instance();
    $recent_actions = macanta_get_user_seession_data($session_name,'recent_actions');
    if($recent_actions!=false){
        $recent_actionArr = json_decode($recent_actions, true);
        $recent_actionArr[] = $action;
        $UserSessions['recent_actions'] = json_encode($recent_actionArr);
        $CI->db->where('session_name',$session_name);
        $CI->db->update('user_sessions',$UserSessions);
        return true;
    }else{
        return false;
    }

}
function macanta_record_user_session($UserSessions,$RecordLimit = 5){
    //Check number of record
    $CI =& get_instance();
    $CI->db->where('login_email',$UserSessions['login_email']);
    $query = $CI->db->get('user_sessions');
    $num_rows = $query->num_rows();
    if($num_rows > $RecordLimit){
        $num_to_delete = $num_rows - $RecordLimit;
        $num_to_delete++;
        $CI->db->where('login_email',$UserSessions['login_email']);
        $CI->db->limit($num_to_delete);
        $CI->db->delete('user_sessions');
    }
    $CI->db->insert('user_sessions', $UserSessions);
}
function macanta_remove_user_session($session_name){
    $CI =& get_instance();
    $CI->db->where('session_name',$session_name);
    $CI->db->delete('user_sessions');
}
function macanta_get_user_meta($InfusionsoftId){
    $meta = [];

    /* START CALL AGENT META */
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->db->where('key','CallCenterData');
    $query = $CI->db->get('config_data');
    $row = $query->row();
    if (isset($row)) {
        $CallCenterData = json_decode($row->value, true);
        if(isset($CallCenterData['Workers'])){
            foreach ($CallCenterData['Workers'] as $WorkerFriendlyName => $WorkerDetails){
                if ((int) $InfusionsoftId == (int) $WorkerDetails['attributes']['infusionsoftId']){
                    $meta['agent_details'] = $WorkerDetails;
                    break;
                }

            }
        }
    }
    /* EMD CALL AGENT META */


    return $meta;
}
function macanta_create_relationship($name,$desc,$groupId){
    if(trim(strtolower($name)) === '') return '';
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->db->where('key','ConnectorRelationship');
    $query = $CI->db->get('config_data');
    $row = $query->row();
    $NewId = macanta_generate_key('re_');

    if (isset($row)) {
        //{"Id":"re_478e4042","RelationshipName":"Owner","RelationshipDescription":"The owner of connected data"}
        $NewRelationships = $OldRelationships = json_decode($row->value, true);

        foreach ($OldRelationships as $OldRelationship){
            if(trim(strtolower($name)) === trim(strtolower($OldRelationship['RelationshipName']))){
                return $OldRelationship['Id'];
            }
        }
        $NewRelationships[] = ["Id"=>$NewId,"RelationshipName"=>$name,"RelationshipDescription"=>$desc];
        $DBdata['value'] = json_encode($NewRelationships);
        $CI->db->where('key','ConnectorRelationship');
        $CI->db->update('config_data',$DBdata);
    }else{
        $NewRelationships = [];
        $NewRelationships[] = ["Id"=>$NewId,"RelationshipName"=>$name,"RelationshipDescription"=>$desc];
        $DBdata['value'] = json_encode($NewRelationships);
        $DBdata['key'] = 'ConnectorRelationship';
        $CI->db->insert('config_data', $DBdata);

    }
    $toConnectedInfoSettings = ["Id"=> $NewId, "exclusive"=> "yes", "limit"=> ""];
    $CI->db->where('key','connected_info');
    $query = $CI->db->get('config_data');
    $row = $query->row();
    if (isset($row)) {
        $ConnectedInfoSettings = json_decode($row->value, true);
        $NewConnectedInfoSettings = $ConnectedInfoSettings;
        foreach ($ConnectedInfoSettings as $Key=>$Settings){
            if($Settings['id'] == $groupId) {
                $Settings['relationships'][] = $toConnectedInfoSettings;
                $NewConnectedInfoSettings[$Key] = $Settings;
                $DBdata = [];
                //print_r($NewConnectedInfoSettings[$Key]);
                $DBdata['value'] = json_encode($NewConnectedInfoSettings);
                $CI->db->where('key','connected_info');
                $CI->db->update('config_data',$DBdata);
                break;
            }
        }
    }
    return $NewId;
}
function macanta_check_value_by_expression($FieldName,$Fields,$Expression){
    $FieldName = strtolower($FieldName);
    $FieldName = strtolower(str_replace('_',' ',$FieldName));
    $field_value = strtolower(trim($Fields[$FieldName]['value']));
    if ($Fields[$FieldName]['field-type'] == 'Checkbox') {
        $field_valueArr = explode('|', $field_value);
        foreach ($field_valueArr as $field_value_item) {
            if($Fields[$FieldName]['field-type'] == 'Date' || $Fields[$FieldName]['field-type'] == 'DateTime'){
                $field_value = strtotime($field_value_item);
            }
            if (eval('return ' . $Expression . ";") == true) return true;
        }
    } else {
        if($Fields[$FieldName]['field-type'] == 'Date' || $Fields[$FieldName]['field-type'] == 'DateTime'){
            $field_value = strtotime($field_value);
        }
        if (eval('return ' . $Expression . ";") == true)  return true;
    }
    return false;
}
function macanta_array_update($newValue, $oldValue, &$modification, &$modified, $parent='',$override=false){
    $NewValue = $oldValue;
    foreach ($newValue as $propName => $propDetails){

        if(isset($oldValue[$propName])){
            if($oldValue[$propName] == $propDetails) continue;
            if($parent == ''){
                $parent = $propName;
            }else{
                $parent = $parent.'->'.$propName;
            }
            if(is_array($propDetails)){
                // check if multi dimensional array
                if(count($propDetails) != count($propDetails, 1)){
                    $NewValue[$propName] = macanta_array_update($propDetails, $oldValue[$propName],$modification,$modified,$parent);
                }
                // if not, just add it or merge to the array value
                else{
                    if(!is_array($NewValue[$propName])) $NewValue[$propName] = [];
                    $NewValue[$propName] = array_unique(array_merge($NewValue[$propName], $propDetails));
                    $modification[$parent] = ['from'=>$oldValue[$propName],'to'=>array_filter($NewValue[$propName])];
                    $modified = true;
                    $parent = '';
                }
            }else{
                $modification[$parent] = ['from'=>$oldValue[$propName],'to'=>$propDetails];
                $NewValue[$propName] = $propDetails;
                $modified = true;
                $parent = '';
            }
        }else{
            if($parent == ''){
                $parent = $propName;
            }else{
                $parent = $parent.'->'.$propName;
            }
            if(!isset($oldValue[$propName])) $oldValue[$propName] = [];
            $modification[$parent] = ['from'=>$oldValue[$propName],'to'=>$propDetails];
            $NewValue[$propName] = $propDetails;
            $modified = true;
            $parent = '';
        }
    }
    if($override == true){
        return $newValue;
    }else{
        return $NewValue;
    }

}
function macanta_get_agent_status($sid){
    $CI =& get_instance();
    $CI->db->where('key','CallCenterData');
    $query = $CI->db->get('config_data');
    $row = $query->row();
    $activityName = 'Offline';
    if (isset($row)) {
        $_theWorkspace = json_decode($row->value, true);

        foreach ($_theWorkspace['Workers'] as $WorkerName=>$WorkerDetails){
            if($WorkerDetails['sid'] == $sid){
                $activityName = $WorkerDetails['activityName'];
                break;
            }
        }
    }
    return $activityName;
}
function macanta_cd_record_history($Id, $Modification ,$Type='connected_data',$session_data=[]){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->dbforge();
    //Create Table If Not Exist
    $table = "connected_data_history";
    $fields = array(
        'id'        => array( 'type' => 'INT'),
        'item_id'   => array( 'type' => 'TEXT', 'null' => TRUE, ),
        'time'      => array( 'type' => 'TEXT', 'null' => TRUE, ),
        'update_by' => array( 'type' => 'TEXT', 'null' => TRUE, ),
        'type'      => array( 'type' => 'TEXT', 'null' => TRUE, ),
        'data'      => array( 'type' => 'TEXT', 'null' => TRUE, )
    );
    $CI->dbforge->add_field($fields);
    $CI->dbforge->create_table($table, true);

    if(sizeof($session_data) > 0){
        $details = json_decode($session_data['details'], true);
        $updated_by = $details['FirstName']." ".$details['LastName']." (".$details['Email']." - ".$session_data['userlevel'].")";
    }else{
        $updated_by = 'System API';
    }
    $DBData = array(
        "item_id"=>$Id,
        "time"=>date("Y-m-d H:i:s"),
        "update_by" =>$updated_by,
        "type"=>$Type,
        "data"=>json_encode($Modification)
    );
    $CI->db->insert($table, $DBData);
}
function macanta_generate_key($prefix='', $length=8){

    return $prefix.macanta_generate_unique_id($length);
}
function macanta_generate_unique_id($length = 8){
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

function macanta_uniqid($prefix='',$length = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    $length = $length - strlen($prefix);
    $length = $length < 3 ? 3:$length;
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($length / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
    } else {
        return macanta_generate_key($prefix, $length);
    }
    return $prefix.substr(bin2hex($bytes), 0, $length);
}
function shortenString($string, $maxlen = 30){
    if (strlen($string) >= $maxlen ) {
        $string =  substr($string, 0, $maxlen-5). " ... " . substr($string, -5);
    }
    return $string;
}
function macanta_search_connected_info($StrArr = [])
{
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    if (sizeof($StrArr) > 0){
        foreach ($StrArr as $SearchKey){
            $array = array(
                'value' => $SearchKey,
                'connected_contact' => $SearchKey
            );
            $CI->db->or_like($array);
        }

    }
    $query = $CI->db->get('connected_data');
    $UserConnectedInfo = [];
    foreach ($query->result() as $row) {
        $UserConnectedInfo[$row->group][$row->id] = [
            'value' => json_decode($row->value, true),
            'connected_contact' => json_decode($row->connected_contact, true),
            'history' => json_decode($row->history, true),
            'meta' => json_decode($row->meta, true),
            'status' => $row->status
        ];
    }
    return $UserConnectedInfo;
}
function macanta_get_hashtags($string) {

    /* Match hashtags */
    preg_match_all('/#(\w+)/', $string, $matches);

    /* Add all matches to array */
    foreach ($matches[0] as $match) {
        $keywords[] = $match;
    }

    return (array) $keywords;
}
function record_user_actvity($Contact){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');

    define('uDBHOST', '10.0.0.118');
    define('uDBUSER', 'macanta_apps');
    define('uDBPASS', 'wne44wdf03p95mbi');

    $MariaDB = new PDO('mysql:host='.uDBHOST.';', uDBUSER, uDBPASS);
    $MariaDB->query("USE MacantaRecords;");

    $Id = $Contact->Id;
    $AppName = $Contact->AppName;
    $Type = $Contact->UserType;
    $FirstName = $Contact->FirstName;
    $LastName = $Contact->LastName;
    $Email = strtolower($Contact->Email);
    $LoginDate = date('Y-m-d H:i:s');
    $TimeStamp	 = time();
    $query = "INSERT INTO macanta_user_track_records (ContactId,AppName,`Type`,FirstName,LastName,Email,LoginDate,`TimeStamp`) 
          VALUES ('$Id','$AppName','$Type','$FirstName','$LastName','$Email','$LoginDate','$TimeStamp')";
    $results = $MariaDB->query($query);
    return $results;
}
function set_macanta_user_success($LoginEmail){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CustomTabsEncoded = macanta_get_config('custom_tabs');
    $CustomTabSuccess = $CustomTabsEncoded ? 'yes':'';

    $TagJson = $CI->config->item('saved_search_restriction');
    $SavedSearchSuccess = $TagJson ? 'yes':'';

    $ConnectedInfo = macanta_get_config('connected_info');
    $ConnectedDataSuccess = $ConnectedInfo ? 'yes':'';

    $UserSuccess = [
        'AppName' => $CI->config->item('IS_App_Name'),
        'LoginEmail' => $LoginEmail,
        'SuccessData' => [
            "LoginSuccess" => "yes",
            "CustomTabSuccess" => $CustomTabSuccess,
            "SavedSearchSuccess" => $SavedSearchSuccess,
            "ConnectedDataSuccess" => $ConnectedDataSuccess
        ],
    ];
    $PostData['postdata'] = http_build_query($UserSuccess);
    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                "Content-type: application/x-www-form-urlencoded\r\n"
        ,
            'content' => $PostData['postdata']
        )
    );
    $context  = stream_context_create($opts);
    unset($ConnectedInfo);
    return file_get_contents('https://getmacanta.org/payments/macanta-customer-success.php', false, $context);
}
function checkVerifiedAddress($ContactId,$AddressType){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->db->where('user_id',$ContactId);
    $CI->db->where('meta_key',$AddressType);
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row))
    {
        return $row->meta_value == 'verified' ? true:false;
    }else{
        return false;
    }
}
function macanta_get_subdomain(){
    $theURL = $_SERVER['HTTP_HOST'];
    $parsedTheURL = parse_url($theURL);
    $theHost = explode('.', $parsedTheURL['path']);
    $theSubdomain = $theHost[0];
    return $theSubdomain;
}
function incrementFileName($file_path,$filename){
    if(count(glob($file_path.$filename))>0)
    {
        $file_ext = end(explode(".", $filename));
        $file_name = str_replace(('.'.$file_ext),"",$filename);
        $newfilename = $file_name.'-'.count(glob($file_path."$file_name*.$file_ext")).'.'.$file_ext;
        return $newfilename;
    }
    else
    {
        return $filename;
    }
}
function getGoogleFileInfo(){
    $client = new Google_Client();
    $client->setApplicationName($_SERVER['GOOGLE_API_NAME']);
    $client->setDeveloperKey($_SERVER['AIzaSyDyWOE8Kdz9mqDvEiZ-UZpEOTOSgeP95Nw']);
}
function createThumbnail($FileSourcePath, $ItemId, $RemoveSource=false,$FileContent='', $ThumbnailDir = "assets/custom_img/file_preview/"){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $im = new Imagick();
    $maxWidth = 64;
    $maxHeight = 64;
    $InvalidFiles =  ["avi","csv","doc","docx","exe","html","mp3","mp4","ppt","txt","xls","xlsx","zip","psd","pdf"];
    /*if (!file_exists(FCPATH.$ThumbnailDir)) {
        mkdir(FCPATH.$ThumbnailDir, 0777, true);
    }
    if (!file_exists(FCPATH.$ThumbnailDir.$ItemId."/")) {
        mkdir(FCPATH.$ThumbnailDir.$ItemId."/", 0777, true);
    }*/
    $IconDefault = FCPATH."assets/img/icons/file.png";
    $path_parts = pathinfo($FileSourcePath);
    if(empty($path_parts['extension'])){
        $content = get_headers($FileSourcePath,1);
        $content = array_change_key_case($content, CASE_LOWER);
        if ($content['content-disposition']) {
            $tmp_name = explode('=', $content['content-disposition']);
            if ($tmp_name[1]) {
                $realfilename = trim($tmp_name[1],'";\'');
                $path_parts = pathinfo($realfilename);
            }
        }
    }
    if(!empty($path_parts['extension'])){
        $path_parts['extension'] = strtolower($path_parts['extension']);
        if(!in_array($path_parts['extension'],$InvalidFiles)) {
            $image = $FileContent == '' ? @file_get_contents($FileSourcePath):$FileContent;
            $valid = true;
        }else{
            $Icon = FCPATH."assets/img/icons/".$path_parts['extension'].".png";
            if(is_file($Icon)){
                $image = @file_get_contents($Icon);
            }else{
                $image = @file_get_contents($IconDefault);
            }
            $valid = false;
        }
    }else{

        $Icon = '';
        $path_parts = parse_url($FileSourcePath);
        $path_parts['extension'] = strtolower($path_parts['extension']);
        /*Array
            (
                [scheme] => https
                [host] => docs.google.com
                [path] => /spreadsheets/d/1RA6qno4lNHt2aqtkveEw8VCHpoVfChOHhxrJCpj0TVw/edit
                [fragment] => gid=0
            )
        */
        //todo: Set Filenames


        if($path_parts['host'] == 'docs.google.com' ){
            if (strpos($path_parts['path'], "spreadsheets") !== false) {
                $Icon = FCPATH."assets/img/icons/google-sheet.png";
                $ThumbFileName = "Google Spreadsheet";
            }
            if (strpos($path_parts['path'], "document") !== false) {
                $Icon = FCPATH."assets/img/icons/google-doc.png";
                $ThumbFileName = "Google Document";
            }
        }
        if($path_parts['host'] == 'drive.google.com' ){
            $Icon = FCPATH."assets/img/icons/google-file.png";
            $ThumbFileName = "Google Drive File";
        }


        if(is_file($Icon)){
            $image = @file_get_contents($Icon);
        }else{
            $image = @file_get_contents($IconDefault);
        }

    }

    try{
        $im->readImageBlob($image);
    }
    catch(Exception $e)
    {
        $Icon = FCPATH."assets/img/icons/".$path_parts['extension'].".png";
        if(is_file($Icon)){
            $image = @file_get_contents($Icon);
        }else{
            $image = @file_get_contents($IconDefault);
        }
        $im->readImageBlob($image);
    }

    $im->setIteratorIndex(0);
    $im->setImageFormat("png24");
    $geo=$im->getImageGeometry();
    //print_r($geo);
    $width=$geo['width'];
    $height=$geo['height'];
    if($width > $height)
    {
        $scale = ($width > $maxWidth) ? $maxWidth/$width : 1;
    }
    else
    {
        $scale = ($height > $maxHeight) ? $maxHeight/$height : 1;
    }
    $newWidth = $scale*$width;
    $newHeight = $scale*$height;
    $im->setImageCompressionQuality(100);
    $im->resizeImage($newWidth,$newHeight,Imagick::FILTER_LANCZOS,1.1);


    $image_data = base64_encode($im->getImageBlob());
    //$handle = fopen($AbsolutePath, "w+");
    //$im->writeImageFile($handle);

    if($RemoveSource) unlink($FileSourcePath);

    return ['data'=>$image_data, 'valid'=>$valid, 'ThumbFileName'=>$ThumbFileName];
}
function shutdown_channel($channel, $connection)
{
    $channel->close();
    $connection->close();
}
function process_amqp_message($message)
{
    echo "\n--------\n";
    echo $message->body;
    echo "\n--------\n";
    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    // Send a message with the string "quit" to cancel the consumer.
    if ($message->body === 'quit') {
        $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
    }
}
function _trim($Data){
    if(gettype($Data) === "string") return trim($Data);
    return $Data;
}
function macanta_get_country_iso2($Country)
{
    $countries = array();
    $countries[] = array("code" => "AF", "name" => "Afghanistan", "d_code" => "+93");
    $countries[] = array("code" => "AL", "name" => "Albania", "d_code" => "+355");
    $countries[] = array("code" => "DZ", "name" => "Algeria", "d_code" => "+213");
    $countries[] = array("code" => "AS", "name" => "American Samoa", "d_code" => "+1");
    $countries[] = array("code" => "AD", "name" => "Andorra", "d_code" => "+376");
    $countries[] = array("code" => "AO", "name" => "Angola", "d_code" => "+244");
    $countries[] = array("code" => "AI", "name" => "Anguilla", "d_code" => "+1");
    $countries[] = array("code" => "AG", "name" => "Antigua", "d_code" => "+1");
    $countries[] = array("code" => "AR", "name" => "Argentina", "d_code" => "+54");
    $countries[] = array("code" => "AM", "name" => "Armenia", "d_code" => "+374");
    $countries[] = array("code" => "AW", "name" => "Aruba", "d_code" => "+297");
    $countries[] = array("code" => "AU", "name" => "Australia", "d_code" => "+61");
    $countries[] = array("code" => "AT", "name" => "Austria", "d_code" => "+43");
    $countries[] = array("code" => "AZ", "name" => "Azerbaijan", "d_code" => "+994");
    $countries[] = array("code" => "BH", "name" => "Bahrain", "d_code" => "+973");
    $countries[] = array("code" => "BD", "name" => "Bangladesh", "d_code" => "+880");
    $countries[] = array("code" => "BB", "name" => "Barbados", "d_code" => "+1");
    $countries[] = array("code" => "BY", "name" => "Belarus", "d_code" => "+375");
    $countries[] = array("code" => "BE", "name" => "Belgium", "d_code" => "+32");
    $countries[] = array("code" => "BZ", "name" => "Belize", "d_code" => "+501");
    $countries[] = array("code" => "BJ", "name" => "Benin", "d_code" => "+229");
    $countries[] = array("code" => "BM", "name" => "Bermuda", "d_code" => "+1");
    $countries[] = array("code" => "BT", "name" => "Bhutan", "d_code" => "+975");
    $countries[] = array("code" => "BO", "name" => "Bolivia", "d_code" => "+591");
    $countries[] = array("code" => "BA", "name" => "Bosnia and Herzegovina", "d_code" => "+387");
    $countries[] = array("code" => "BW", "name" => "Botswana", "d_code" => "+267");
    $countries[] = array("code" => "BR", "name" => "Brazil", "d_code" => "+55");
    $countries[] = array("code" => "IO", "name" => "British Indian Ocean Territory", "d_code" => "+246");
    $countries[] = array("code" => "VG", "name" => "British Virgin Islands", "d_code" => "+1");
    $countries[] = array("code" => "BN", "name" => "Brunei", "d_code" => "+673");
    $countries[] = array("code" => "BG", "name" => "Bulgaria", "d_code" => "+359");
    $countries[] = array("code" => "BF", "name" => "Burkina Faso", "d_code" => "+226");
    $countries[] = array("code" => "MM", "name" => "Burma Myanmar", "d_code" => "+95");
    $countries[] = array("code" => "BI", "name" => "Burundi", "d_code" => "+257");
    $countries[] = array("code" => "KH", "name" => "Cambodia", "d_code" => "+855");
    $countries[] = array("code" => "CM", "name" => "Cameroon", "d_code" => "+237");
    $countries[] = array("code" => "CA", "name" => "Canada", "d_code" => "+1");
    $countries[] = array("code" => "CV", "name" => "Cape Verde", "d_code" => "+238");
    $countries[] = array("code" => "KY", "name" => "Cayman Islands", "d_code" => "+1");
    $countries[] = array("code" => "CF", "name" => "Central African Republic", "d_code" => "+236");
    $countries[] = array("code" => "TD", "name" => "Chad", "d_code" => "+235");
    $countries[] = array("code" => "CL", "name" => "Chile", "d_code" => "+56");
    $countries[] = array("code" => "CN", "name" => "China", "d_code" => "+86");
    $countries[] = array("code" => "CO", "name" => "Colombia", "d_code" => "+57");
    $countries[] = array("code" => "KM", "name" => "Comoros", "d_code" => "+269");
    $countries[] = array("code" => "CK", "name" => "Cook Islands", "d_code" => "+682");
    $countries[] = array("code" => "CR", "name" => "Costa Rica", "d_code" => "+506");
    $countries[] = array("code" => "CI", "name" => "Côte d'Ivoire", "d_code" => "+225");
    $countries[] = array("code" => "HR", "name" => "Croatia", "d_code" => "+385");
    $countries[] = array("code" => "CU", "name" => "Cuba", "d_code" => "+53");
    $countries[] = array("code" => "CY", "name" => "Cyprus", "d_code" => "+357");
    $countries[] = array("code" => "CZ", "name" => "Czech Republic", "d_code" => "+420");
    $countries[] = array("code" => "CD", "name" => "Democratic Republic of Congo", "d_code" => "+243");
    $countries[] = array("code" => "DK", "name" => "Denmark", "d_code" => "+45");
    $countries[] = array("code" => "DJ", "name" => "Djibouti", "d_code" => "+253");
    $countries[] = array("code" => "DM", "name" => "Dominica", "d_code" => "+1");
    $countries[] = array("code" => "DO", "name" => "Dominican Republic", "d_code" => "+1");
    $countries[] = array("code" => "EC", "name" => "Ecuador", "d_code" => "+593");
    $countries[] = array("code" => "EG", "name" => "Egypt", "d_code" => "+20");
    $countries[] = array("code" => "SV", "name" => "El Salvador", "d_code" => "+503");
    $countries[] = array("code" => "GQ", "name" => "Equatorial Guinea", "d_code" => "+240");
    $countries[] = array("code" => "ER", "name" => "Eritrea", "d_code" => "+291");
    $countries[] = array("code" => "EE", "name" => "Estonia", "d_code" => "+372");
    $countries[] = array("code" => "ET", "name" => "Ethiopia", "d_code" => "+251");
    $countries[] = array("code" => "FK", "name" => "Falkland Islands", "d_code" => "+500");
    $countries[] = array("code" => "FO", "name" => "Faroe Islands", "d_code" => "+298");
    $countries[] = array("code" => "FM", "name" => "Federated States of Micronesia", "d_code" => "+691");
    $countries[] = array("code" => "FJ", "name" => "Fiji", "d_code" => "+679");
    $countries[] = array("code" => "FI", "name" => "Finland", "d_code" => "+358");
    $countries[] = array("code" => "FR", "name" => "France", "d_code" => "+33");
    $countries[] = array("code" => "GF", "name" => "French Guiana", "d_code" => "+594");
    $countries[] = array("code" => "PF", "name" => "French Polynesia", "d_code" => "+689");
    $countries[] = array("code" => "GA", "name" => "Gabon", "d_code" => "+241");
    $countries[] = array("code" => "GE", "name" => "Georgia", "d_code" => "+995");
    $countries[] = array("code" => "DE", "name" => "Germany", "d_code" => "+49");
    $countries[] = array("code" => "GH", "name" => "Ghana", "d_code" => "+233");
    $countries[] = array("code" => "GI", "name" => "Gibraltar", "d_code" => "+350");
    $countries[] = array("code" => "GR", "name" => "Greece", "d_code" => "+30");
    $countries[] = array("code" => "GL", "name" => "Greenland", "d_code" => "+299");
    $countries[] = array("code" => "GD", "name" => "Grenada", "d_code" => "+1");
    $countries[] = array("code" => "GP", "name" => "Guadeloupe", "d_code" => "+590");
    $countries[] = array("code" => "GU", "name" => "Guam", "d_code" => "+1");
    $countries[] = array("code" => "GT", "name" => "Guatemala", "d_code" => "+502");
    $countries[] = array("code" => "GN", "name" => "Guinea", "d_code" => "+224");
    $countries[] = array("code" => "GW", "name" => "Guinea-Bissau", "d_code" => "+245");
    $countries[] = array("code" => "GY", "name" => "Guyana", "d_code" => "+592");
    $countries[] = array("code" => "HT", "name" => "Haiti", "d_code" => "+509");
    $countries[] = array("code" => "HN", "name" => "Honduras", "d_code" => "+504");
    $countries[] = array("code" => "HK", "name" => "Hong Kong", "d_code" => "+852");
    $countries[] = array("code" => "HU", "name" => "Hungary", "d_code" => "+36");
    $countries[] = array("code" => "IS", "name" => "Iceland", "d_code" => "+354");
    $countries[] = array("code" => "IN", "name" => "India", "d_code" => "+91");
    $countries[] = array("code" => "ID", "name" => "Indonesia", "d_code" => "+62");
    $countries[] = array("code" => "IR", "name" => "Iran", "d_code" => "+98");
    $countries[] = array("code" => "IQ", "name" => "Iraq", "d_code" => "+964");
    $countries[] = array("code" => "IE", "name" => "Ireland", "d_code" => "+353");
    $countries[] = array("code" => "IL", "name" => "Israel", "d_code" => "+972");
    $countries[] = array("code" => "IT", "name" => "Italy", "d_code" => "+39");
    $countries[] = array("code" => "JM", "name" => "Jamaica", "d_code" => "+1");
    $countries[] = array("code" => "JP", "name" => "Japan", "d_code" => "+81");
    $countries[] = array("code" => "JO", "name" => "Jordan", "d_code" => "+962");
    $countries[] = array("code" => "KZ", "name" => "Kazakhstan", "d_code" => "+7");
    $countries[] = array("code" => "KE", "name" => "Kenya", "d_code" => "+254");
    $countries[] = array("code" => "KI", "name" => "Kiribati", "d_code" => "+686");
    $countries[] = array("code" => "XK", "name" => "Kosovo", "d_code" => "+381");
    $countries[] = array("code" => "KW", "name" => "Kuwait", "d_code" => "+965");
    $countries[] = array("code" => "KG", "name" => "Kyrgyzstan", "d_code" => "+996");
    $countries[] = array("code" => "LA", "name" => "Laos", "d_code" => "+856");
    $countries[] = array("code" => "LV", "name" => "Latvia", "d_code" => "+371");
    $countries[] = array("code" => "LB", "name" => "Lebanon", "d_code" => "+961");
    $countries[] = array("code" => "LS", "name" => "Lesotho", "d_code" => "+266");
    $countries[] = array("code" => "LR", "name" => "Liberia", "d_code" => "+231");
    $countries[] = array("code" => "LY", "name" => "Libya", "d_code" => "+218");
    $countries[] = array("code" => "LI", "name" => "Liechtenstein", "d_code" => "+423");
    $countries[] = array("code" => "LT", "name" => "Lithuania", "d_code" => "+370");
    $countries[] = array("code" => "LU", "name" => "Luxembourg", "d_code" => "+352");
    $countries[] = array("code" => "MO", "name" => "Macau", "d_code" => "+853");
    $countries[] = array("code" => "MK", "name" => "Macedonia", "d_code" => "+389");
    $countries[] = array("code" => "MG", "name" => "Madagascar", "d_code" => "+261");
    $countries[] = array("code" => "MW", "name" => "Malawi", "d_code" => "+265");
    $countries[] = array("code" => "MY", "name" => "Malaysia", "d_code" => "+60");
    $countries[] = array("code" => "MV", "name" => "Maldives", "d_code" => "+960");
    $countries[] = array("code" => "ML", "name" => "Mali", "d_code" => "+223");
    $countries[] = array("code" => "MT", "name" => "Malta", "d_code" => "+356");
    $countries[] = array("code" => "MH", "name" => "Marshall Islands", "d_code" => "+692");
    $countries[] = array("code" => "MQ", "name" => "Martinique", "d_code" => "+596");
    $countries[] = array("code" => "MR", "name" => "Mauritania", "d_code" => "+222");
    $countries[] = array("code" => "MU", "name" => "Mauritius", "d_code" => "+230");
    $countries[] = array("code" => "YT", "name" => "Mayotte", "d_code" => "+262");
    $countries[] = array("code" => "MX", "name" => "Mexico", "d_code" => "+52");
    $countries[] = array("code" => "MD", "name" => "Moldova", "d_code" => "+373");
    $countries[] = array("code" => "MC", "name" => "Monaco", "d_code" => "+377");
    $countries[] = array("code" => "MN", "name" => "Mongolia", "d_code" => "+976");
    $countries[] = array("code" => "ME", "name" => "Montenegro", "d_code" => "+382");
    $countries[] = array("code" => "MS", "name" => "Montserrat", "d_code" => "+1");
    $countries[] = array("code" => "MA", "name" => "Morocco", "d_code" => "+212");
    $countries[] = array("code" => "MZ", "name" => "Mozambique", "d_code" => "+258");
    $countries[] = array("code" => "NA", "name" => "Namibia", "d_code" => "+264");
    $countries[] = array("code" => "NR", "name" => "Nauru", "d_code" => "+674");
    $countries[] = array("code" => "NP", "name" => "Nepal", "d_code" => "+977");
    $countries[] = array("code" => "NL", "name" => "Netherlands", "d_code" => "+31");
    $countries[] = array("code" => "AN", "name" => "Netherlands Antilles", "d_code" => "+599");
    $countries[] = array("code" => "NC", "name" => "New Caledonia", "d_code" => "+687");
    $countries[] = array("code" => "NZ", "name" => "New Zealand", "d_code" => "+64");
    $countries[] = array("code" => "NI", "name" => "Nicaragua", "d_code" => "+505");
    $countries[] = array("code" => "NE", "name" => "Niger", "d_code" => "+227");
    $countries[] = array("code" => "NG", "name" => "Nigeria", "d_code" => "+234");
    $countries[] = array("code" => "NU", "name" => "Niue", "d_code" => "+683");
    $countries[] = array("code" => "NF", "name" => "Norfolk Island", "d_code" => "+672");
    $countries[] = array("code" => "KP", "name" => "North Korea", "d_code" => "+850");
    $countries[] = array("code" => "MP", "name" => "Northern Mariana Islands", "d_code" => "+1");
    $countries[] = array("code" => "NO", "name" => "Norway", "d_code" => "+47");
    $countries[] = array("code" => "OM", "name" => "Oman", "d_code" => "+968");
    $countries[] = array("code" => "PK", "name" => "Pakistan", "d_code" => "+92");
    $countries[] = array("code" => "PW", "name" => "Palau", "d_code" => "+680");
    $countries[] = array("code" => "PS", "name" => "Palestine", "d_code" => "+970");
    $countries[] = array("code" => "PA", "name" => "Panama", "d_code" => "+507");
    $countries[] = array("code" => "PG", "name" => "Papua New Guinea", "d_code" => "+675");
    $countries[] = array("code" => "PY", "name" => "Paraguay", "d_code" => "+595");
    $countries[] = array("code" => "PE", "name" => "Peru", "d_code" => "+51");
    $countries[] = array("code" => "PH", "name" => "Philippines", "d_code" => "+63");
    $countries[] = array("code" => "PL", "name" => "Poland", "d_code" => "+48");
    $countries[] = array("code" => "PT", "name" => "Portugal", "d_code" => "+351");
    $countries[] = array("code" => "PR", "name" => "Puerto Rico", "d_code" => "+1");
    $countries[] = array("code" => "QA", "name" => "Qatar", "d_code" => "+974");
    $countries[] = array("code" => "CG", "name" => "Republic of the Congo", "d_code" => "+242");
    $countries[] = array("code" => "RE", "name" => "Réunion", "d_code" => "+262");
    $countries[] = array("code" => "RO", "name" => "Romania", "d_code" => "+40");
    $countries[] = array("code" => "RU", "name" => "Russia", "d_code" => "+7");
    $countries[] = array("code" => "RW", "name" => "Rwanda", "d_code" => "+250");
    $countries[] = array("code" => "BL", "name" => "Saint Barthélemy", "d_code" => "+590");
    $countries[] = array("code" => "SH", "name" => "Saint Helena", "d_code" => "+290");
    $countries[] = array("code" => "KN", "name" => "Saint Kitts and Nevis", "d_code" => "+1");
    $countries[] = array("code" => "MF", "name" => "Saint Martin", "d_code" => "+590");
    $countries[] = array("code" => "PM", "name" => "Saint Pierre and Miquelon", "d_code" => "+508");
    $countries[] = array("code" => "VC", "name" => "Saint Vincent and the Grenadines", "d_code" => "+1");
    $countries[] = array("code" => "WS", "name" => "Samoa", "d_code" => "+685");
    $countries[] = array("code" => "SM", "name" => "San Marino", "d_code" => "+378");
    $countries[] = array("code" => "ST", "name" => "São Tomé and Príncipe", "d_code" => "+239");
    $countries[] = array("code" => "SA", "name" => "Saudi Arabia", "d_code" => "+966");
    $countries[] = array("code" => "SN", "name" => "Senegal", "d_code" => "+221");
    $countries[] = array("code" => "RS", "name" => "Serbia", "d_code" => "+381");
    $countries[] = array("code" => "SC", "name" => "Seychelles", "d_code" => "+248");
    $countries[] = array("code" => "SL", "name" => "Sierra Leone", "d_code" => "+232");
    $countries[] = array("code" => "SG", "name" => "Singapore", "d_code" => "+65");
    $countries[] = array("code" => "SK", "name" => "Slovakia", "d_code" => "+421");
    $countries[] = array("code" => "SI", "name" => "Slovenia", "d_code" => "+386");
    $countries[] = array("code" => "SB", "name" => "Solomon Islands", "d_code" => "+677");
    $countries[] = array("code" => "SO", "name" => "Somalia", "d_code" => "+252");
    $countries[] = array("code" => "ZA", "name" => "South Africa", "d_code" => "+27");
    $countries[] = array("code" => "KR", "name" => "South Korea", "d_code" => "+82");
    $countries[] = array("code" => "ES", "name" => "Spain", "d_code" => "+34");
    $countries[] = array("code" => "LK", "name" => "Sri Lanka", "d_code" => "+94");
    $countries[] = array("code" => "LC", "name" => "St. Lucia", "d_code" => "+1");
    $countries[] = array("code" => "SD", "name" => "Sudan", "d_code" => "+249");
    $countries[] = array("code" => "SR", "name" => "Suriname", "d_code" => "+597");
    $countries[] = array("code" => "SZ", "name" => "Swaziland", "d_code" => "+268");
    $countries[] = array("code" => "SE", "name" => "Sweden", "d_code" => "+46");
    $countries[] = array("code" => "CH", "name" => "Switzerland", "d_code" => "+41");
    $countries[] = array("code" => "SY", "name" => "Syria", "d_code" => "+963");
    $countries[] = array("code" => "TW", "name" => "Taiwan", "d_code" => "+886");
    $countries[] = array("code" => "TJ", "name" => "Tajikistan", "d_code" => "+992");
    $countries[] = array("code" => "TZ", "name" => "Tanzania", "d_code" => "+255");
    $countries[] = array("code" => "TH", "name" => "Thailand", "d_code" => "+66");
    $countries[] = array("code" => "BS", "name" => "The Bahamas", "d_code" => "+1");
    $countries[] = array("code" => "GM", "name" => "The Gambia", "d_code" => "+220");
    $countries[] = array("code" => "TL", "name" => "Timor-Leste", "d_code" => "+670");
    $countries[] = array("code" => "TG", "name" => "Togo", "d_code" => "+228");
    $countries[] = array("code" => "TK", "name" => "Tokelau", "d_code" => "+690");
    $countries[] = array("code" => "TO", "name" => "Tonga", "d_code" => "+676");
    $countries[] = array("code" => "TT", "name" => "Trinidad and Tobago", "d_code" => "+1");
    $countries[] = array("code" => "TN", "name" => "Tunisia", "d_code" => "+216");
    $countries[] = array("code" => "TR", "name" => "Turkey", "d_code" => "+90");
    $countries[] = array("code" => "TM", "name" => "Turkmenistan", "d_code" => "+993");
    $countries[] = array("code" => "TC", "name" => "Turks and Caicos Islands", "d_code" => "+1");
    $countries[] = array("code" => "TV", "name" => "Tuvalu", "d_code" => "+688");
    $countries[] = array("code" => "UG", "name" => "Uganda", "d_code" => "+256");
    $countries[] = array("code" => "UA", "name" => "Ukraine", "d_code" => "+380");
    $countries[] = array("code" => "AE", "name" => "United Arab Emirates", "d_code" => "+971");
    $countries[] = array("code" => "GB", "name" => "United Kingdom", "d_code" => "+44");
    $countries[] = array("code" => "US", "name" => "United States", "d_code" => "+1");
    $countries[] = array("code" => "UY", "name" => "Uruguay", "d_code" => "+598");
    $countries[] = array("code" => "UZ", "name" => "Uzbekistan", "d_code" => "+998");
    $countries[] = array("code" => "VU", "name" => "Vanuatu", "d_code" => "+678");
    $countries[] = array("code" => "VA", "name" => "Vatican City", "d_code" => "+39");
    $countries[] = array("code" => "VE", "name" => "Venezuela", "d_code" => "+58");
    $countries[] = array("code" => "VN", "name" => "Vietnam", "d_code" => "+84");
    $countries[] = array("code" => "WF", "name" => "Wallis and Futuna", "d_code" => "+681");
    $countries[] = array("code" => "YE", "name" => "Yemen", "d_code" => "+967");
    $countries[] = array("code" => "ZM", "name" => "Zambia", "d_code" => "+260");
    $countries[] = array("code" => "ZW", "name" => "Zimbabwe", "d_code" => "+263");
    foreach ($countries as $theCountry) {
        if ($theCountry['name'] == $Country) {
            return $theCountry;
        }
    }
    return false;
}
function macanta_get_state_format( $input, $format = 'abbr' ) {
    if( ! $input || empty( $input ) )
        return;

    $states = array (
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'DC'=>'District Of Columbia',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
    );

    foreach( $states as $abbr => $name ) {
        if ( preg_match( "/\b($name)\b/", ucwords( strtolower( $input ) ), $match ) )  {
            if( 'abbr' == $format ){
                return $abbr;
            }
            else return $name;
        }
        elseif( preg_match("/\b($abbr)\b/", strtoupper( $input ), $match) ) {
            if( 'abbr' == $format ){
                return $abbr;
            }
            else return $name;
        }
    }
    return;
}
function getInitialGeneratedToken($Appname = false){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $DataPassed = [
        'AppName' => $Appname ? $Appname:$CI->config->item('IS_App_Name')
    ];
    $PostData = http_build_query($DataPassed);
    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                "Content-type: application/x-www-form-urlencoded\r\n"
        ,
            'content' => $PostData
        )
    );
    $context  = stream_context_create($opts);
    return file_get_contents('https://getmacanta.org/payments/InitialGeneratedToken.php', false, $context);
}
function RestartConnectedDataServices(){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $MacantaApp = $CI->config->item('IS_App_Name');
    $SERVER_DIR = SERVICEPATH; // defined in index.php
    $SERVER_LOG = $SERVER_DIR.'apps';
    $stop_command = 'kill -9 ';
    $consumer = $SERVER_LOG . "/". $MacantaApp . '/consumer.pid';
    $trigger = $SERVER_LOG . "/". $MacantaApp . '/trigger.pid';
    $pidConsumer = trim(@file_get_contents($consumer));
    $pidTrigger = trim(@file_get_contents($trigger));
    exec($stop_command . $pidConsumer);
    exec($stop_command . $pidTrigger);
    exec('rm -f ' . $consumer);
    exec('rm -f ' . $trigger);
    sleep(2);
    // Lets try to restart again
    if (!is_dir($SERVER_LOG . '/' . $MacantaApp)) {
        mkdir($SERVER_LOG . '/' . $MacantaApp, 0755, true);
    }
    $extracommands = ' ';
    $service = 'consumer/'.$MacantaApp;
    $CLI = '/usr/bin/php -c '.$SERVER_DIR.'conf/php_service.ini '.$SERVER_DIR.'_cli.php ';
    $exec_string = $CLI . $service . ' ' . $extracommands . ' >> '.$SERVER_LOG.'/' . $MacantaApp . '/stdout.log & ';
    $exec_string2 = $CLI . 'trigger/'.$MacantaApp.' >> ' . $SERVER_LOG . '/' . $MacantaApp . '/trigger.log & ';
    $output = [];
    $output2 = [];
    exec($exec_string,$output);
    exec($exec_string2,$output2);
}
function macanta_create_table_connected_data_file_attachment(){
    header("Content-Type: text/plain");
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->load->dbforge();

    // CREATE TABLE IF NOT EXIST
    $CI->dbforge->add_field('id');
    $CI->dbforge->add_field("`item_id` varchar(30) NOT NULL");
    $CI->dbforge->add_field("`filename` text NOT NULL");
    $CI->dbforge->add_field("`thumbnail` longtext NOT NULL");
    $CI->dbforge->add_field("`download_url` text NOT NULL");
    $CI->dbforge->add_field("`file_size` int NOT NULL");
    $CI->dbforge->add_field("`timestamp` varchar(100) NOT NULL");
    $CI->dbforge->add_field("`b2_filename` text NOT NULL");
    $CI->dbforge->add_field("`b2_file_id` varchar(100) NOT NULL");
    $CI->dbforge->add_field("`b2_timestamp` varchar(100) NOT NULL");
    $CI->dbforge->add_field("`meta` longtext NOT NULL");
    return $CI->dbforge->create_table('connected_data_file_attachment', TRUE);
}
function macanta_b2_authorize_account($Credentials){
    /*
    stdClass Object
    (
        [absoluteMinimumPartSize] => 5000000
        [accountId] => b8bd3295b37b
        [allowed] => stdClass Object
            (
                [bucketId] => 8b485bad839209f56b63071b
                [bucketName] => dedicated-macanta-fileboxes
                [capabilities] => Array
                    (
                        [0] => listBuckets
                        [1] => listFiles
                        [2] => readFiles
                        [3] => shareFiles
                        [4] => writeFiles
                        [5] => deleteFiles
                    )

                [namePrefix] =>
            )

        [apiUrl] => https://api001.backblazeb2.com
        [authorizationToken] => 4_001b8bd3295b37b0000000001_01871eec_964c81_acct_81CY9TSyfhKDXxNDsTqdzozbGk0=
        [downloadUrl] => https://f001.backblazeb2.com
        [minimumPartSize] => 100000000
        [recommendedPartSize] => 100000000
    )
    */
    header("Content-Type: text/plain");
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');

    macanta_create_table_connected_data_file_attachment();

    $CacheFile = 'b2_authorize-'.$Credentials['B2_BUCKET_NAME'];
    $Authorize = json_decode(manual_cache_loader($CacheFile));
    if($Authorize == false) {
        $account_id = $Credentials['B2_APP_ID']; // Obtained from your B2 account page
        $application_key = $Credentials['B2_APP_KEY']; // Obtained from your B2 account page
        $credentials = base64_encode($account_id . ":" . $application_key);
        $url = "https://api.backblazeb2.com/b2api/v1/b2_authorize_account";
        $session = curl_init($url);
        // Add headers
        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Authorization: Basic " . $credentials;
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);  // Add headers
        curl_setopt($session, CURLOPT_HTTPGET, true);  // HTTP GET
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true); // Receive server response
        $Authorize = curl_exec($session);
        $Authorize = json_decode($Authorize);
        curl_close ($session);
        manual_cache_writer($CacheFile,json_encode($Authorize),86400); // valid for 24 hours
    }
    return $Authorize;
}
function macanta_b2_get_upload_url($Credentials){
    /*
    stdClass Object
        (
            [authorizationToken] => 4_001b8bd3295b37b0000000001_01871f32_28da4e_upld_jg02s2ELLqELXeI3_a3ywSAQ_lI=
            [bucketId] => 8b485bad839209f56b63071b
            [uploadUrl] => https://pod-000-1106-04.backblaze.com/b2api/v1/b2_upload_file/8b485bad839209f56b63071b/c001_v0001106_t0024
        )
    */
    header("Content-Type: text/plain");
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CacheFile = 'b2_upload_url-'.$Credentials['B2_BUCKET_NAME'];
    $UploadURL = json_decode(manual_cache_loader($CacheFile));
    if($UploadURL == false) {

        $Authorize = macanta_b2_authorize_account($Credentials);
        $api_url = $Authorize->apiUrl; // From b2_authorize_account call
        $auth_token = $Authorize->authorizationToken; // From b2_authorize_account call
        $bucket_id = $Credentials['B2_BUCKET_ID'];  // The ID of the bucket you want to upload to

        $session = curl_init($api_url .  "/b2api/v1/b2_get_upload_url");

        // Add post fields
        $data = array("bucketId" => $bucket_id);
        $post_fields = json_encode($data);
        curl_setopt($session, CURLOPT_POSTFIELDS, $post_fields);

        // Add headers
        $headers = array();
        $headers[] = "Authorization: " . $auth_token;
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($session, CURLOPT_POST, true); // HTTP POST
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
        $UploadURL = curl_exec($session); // Let's do this!
        $UploadURL = json_decode($UploadURL);
        curl_close ($session); // Clean up
        manual_cache_writer($CacheFile,json_encode($UploadURL),$ttl=86400); // valid for 24 hours
    }
    return $UploadURL;
}
function macanta_b2_upload($File,  $Folder, $NewFilename="", $CredentialName="FILEBOX", $String = ""){
    /*
     {
        "fileId" : "4_h4a48fe8875c6214145260818_f000000000000472a_d20140104_m032022_c001_v0000123_t0104",
        "fileName" : "typing_test.txt",
        "accountId" : "d522aa47a10f",
        "bucketId" : "4a48fe8875c6214145260818",
        "contentLength" : 46,
        "contentSha1" : "bae5ed658ab3546aee12f23f36392f35dba1ebdd",
        "contentType" : "text/plain",
        "fileInfo" : {
           "author" : "unknown"
        }
    }
     */
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Credentials =$CI->config->item($CredentialName);
    $UploadCredentials = macanta_b2_get_upload_url($Credentials);

    if($String == ""){
        $file_basename =  $NewFilename == "" ? basename($File):$NewFilename;
        $handle = fopen($File, 'r');
        $read_file = fread($handle,filesize($File));
        $sha1_of_file_data = sha1_file($File);
    }else{
        $file_basename =  str_replace(' ','_',$File);
        $read_file = $String;
        $sha1_of_file_data = sha1($String);
    }


    $upload_url = $UploadCredentials->uploadUrl; // Provided by b2_get_upload_url
    $upload_auth_token = $UploadCredentials->authorizationToken; // Provided by b2_get_upload_url
    $bucket_id = $UploadCredentials->bucketId;  // The ID of the bucket
    $content_type = "text/plain";

    $session = curl_init($upload_url);

    // Add read file as post field
    curl_setopt($session, CURLOPT_POSTFIELDS, $read_file);

    // Add headers
    $headers = array();
    $headers[] = "Authorization: " . $upload_auth_token;
    $headers[] = "X-Bz-File-Name: " . $CI->config->item('IS_App_Name')."/".$Folder."/".$file_basename;
    $headers[] = "Content-Type: " . $content_type;
    $headers[] = "X-Bz-Content-Sha1: " . $sha1_of_file_data;
    curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($session, CURLOPT_POST, true); // HTTP POST
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
    $server_output = curl_exec($session); // Let's do this!
    curl_close ($session); // Clean up
    return $server_output; // Tell me about the rabbits, George!
}
function macanta_b2_list_filenames($Folder,$File="", $CredentialName="FILEBOX", $root=false){
    header("Content-Type: text/plain");
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Credentials =$CI->config->item($CredentialName);
    $Authorize = macanta_b2_authorize_account($CredentialName);
    $api_url = $Authorize->apiUrl; // From b2_authorize_account call
    $auth_token = $Authorize->authorizationToken; // From b2_authorize_account call
    $bucket_id = $Credentials['B2_BUCKET_ID'];
    $maxFileCount = 10000;
    $session = curl_init($api_url .  "/b2api/v2/b2_list_file_names");

    // Add post fields : bucketId, startFileName, maxFileCount prefix, delimiter

    if($root == false){
        $prefix = $CI->config->item('IS_App_Name')."/";
        if(!empty($Folder)) {
            $prefix = $prefix.$Folder."/";
            if(!empty($File)) $prefix = $prefix.$File;
        }
    }else{
        $prefix = null;
    }

    $data = array("bucketId" => $bucket_id,"prefix"=>$prefix,"maxFileCount"=>$maxFileCount);
    $post_fields = json_encode($data);
    curl_setopt($session, CURLOPT_POSTFIELDS, $post_fields);

    // Add headers
    $headers = array();
    $headers[] = "Authorization: " . $auth_token;
    curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($session, CURLOPT_POST, true); // HTTP POST
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
    $server_output = curl_exec($session); // Let's do this!
    curl_close ($session); // Clean up
    //echo ($server_output); // Tell me about the rabbits, George!
    return $server_output;
}
function macanta_b2_download_file_by_name($Folder, $File, $CredentialName="FILEBOX", $URL_only=false){
    //header("Content-Type: text/plain");
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Credentials =$CI->config->item($CredentialName);
    $Authorize = macanta_b2_authorize_account($Credentials);
    $auth_token = $Authorize->authorizationToken; // From b2_authorize_account call
    $FilePath = $CI->config->item('IS_App_Name')."/".$Folder."/".str_replace(' ','_',$File);
    $download_url = $Authorize->downloadUrl; // From b2_authorize_account call
    $uri = $download_url . "/file/" . $Credentials['B2_BUCKET_NAME'] . "/" . $FilePath;
    if($URL_only == true){
        if($Credentials['B2_BUCKET_TYPE'] == "PRIVATE"){
            return $uri."?Authorization=".$auth_token;
        }else{
            return $uri;
        }

    }else{
        $session = curl_init($uri);
        if($Credentials['B2_BUCKET_TYPE'] == "PRIVATE"){
            // Add headers

            $headers = array();
            $headers[] = "Authorization: " . $auth_token;
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($session, CURLOPT_HTTPGET, true); // HTTP POST
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
        $server_output = curl_exec($session); // Let's do this!
        curl_close ($session); // Clean up
        return $server_output; // Tell me about the rabbits, George!
    }

}
function macanta_b2_download_file_by_fullname($Fullname, $CredentialName="FILEBOX", $URL_only=true, $bucket_name = "dedicated-macanta-fileboxes"){
    //header("Content-Type: text/plain");
    error_reporting(E_ERROR);
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $Credentials =$CI->config->item($CredentialName);
    $Credentials = macanta_b2_authorize_account($Credentials);
    $auth_token = $Credentials->authorizationToken; // From b2_authorize_account call
    $download_url = $Credentials->downloadUrl; // From b2_authorize_account call
    $uri = $download_url . "/file/" . $bucket_name . "/" . $Fullname; //$Fullname = 'qj311-dm/364/MyAvatar.jpg'

    if($URL_only == true){
        return $uri."?Authorization=".$auth_token;
    }else{
        $session = curl_init($uri);

        // Add headers
        $headers = array();
        $headers[] = "Authorization: " . $auth_token;
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($session, CURLOPT_HTTPGET, true); // HTTP POST
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
        $server_output = curl_exec($session); // Let's do this!
        curl_close ($session); // Clean up
        return $server_output; // Tell me about the rabbits, George!
    }

}
function sanitize_output($buffer) {

    $search = array(
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/' // Remove HTML comments
    );

    $replace = array(
        '>',
        '<',
        '\\1',
        ''
    );

    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}
function macanta_invalid_token(){
    $InfusionsoftRSStatus = macanta_get_config('InfusionsoftRSStatus');
    if($InfusionsoftRSStatus == false){
        return false;
    }else{
        if($InfusionsoftRSStatus == 'Invalid' || $InfusionsoftRSStatus == 'Unknown Token Error' ){
            return true;
        }else{
            return false;
        }
    }
}
function macanta_get_config($ConfigName,$Encoded = false){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->db->where('key',$ConfigName);
    $query = $CI->db->get('config_data');
    if(is_bool($query)){
        $last_query = $CI->db->last_query();
        $Error = $CI->db->error();
        echo "\n===================ERROR=====================\n";
        echo $last_query."\n";
        echo json_encode($Error)."\n";
        echo "=============================================\n\n";
        return false;
    }
    else{
        $row = $query->row();
        if (isset($row))
        {
            return $row->value;
        }else{
            return $Encoded ? '{}':false;
        }
    }
}
function macanta_db_record_exist($keyName,$keyValue,$table, $returnDetails = false){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    if(is_array($keyName) && is_array($keyValue)){
        foreach ($keyName as $index => $name){
            $CI->db->where($keyName[$index],$keyValue[$index]);
        }
    }
    else{
        $CI->db->where($keyName,$keyValue);
    }

    $query = $CI->db->get($table);
    $row = $query->row();
    if (isset($row))
    {
        if($returnDetails) return $row;
        return true;
    }else{
        return false;
    }
}
function macanta_get_install_info(){

    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $DataPassed = [
        'AppName' => $CI->config->item('IS_App_Name')
    ];
    print_r($DataPassed);
    $PostData = http_build_query($DataPassed);
    $opts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                "Content-type: application/x-www-form-urlencoded\r\n"
        ,
            'content' => $PostData
        )
    );
    $context  = stream_context_create($opts);
    return file_get_contents('https://getmacanta.org/payments/get_install_info.php', false, $context);
}
function macanta_migrate_centralised(){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');

    $table = 'config_data';
    $keyName = 'key';

    //------ API KEY : macanta_api_key ------//
    $keyValue = 'macanta_api_key';
    $macanta_api_key = macanta_db_record_exist($keyName,$keyValue,$table);
    if($macanta_api_key == false){
        $passwordFile = dirname(APPPATH).DIRECTORY_SEPARATOR."phpliteadmin.txt";
        $passwordArr = json_decode(file_get_contents($passwordFile),true);
        $value = $passwordArr['password'];
        $CI->config->set_item($keyValue,$value);
        $DBData['value'] = $value;
        $DBData['key'] = $keyValue;
        $CI->db->insert('config_data', $DBData);
    }else{
        echo  "Skip $keyValue \n";
    }
    //------ ==================================== ------//

    //------ Twilio.txt: macanta_caller_id ------//
    $keyValue = 'macanta_caller_id';
    $macanta_caller_id = macanta_db_record_exist($keyName,$keyValue,$table);
    if($macanta_caller_id == false){
        $ClientTwilioCredentials = json_decode(trim(file_get_contents(FCPATH."twilio.txt")));
        $value = $ClientTwilioCredentials->callerID;
        $CI->config->set_item($keyValue,$value);
        $DBData['value'] = $value;
        $DBData['key'] = $keyValue;
        $CI->db->insert('config_data', $DBData);

    }else{
        echo  "Skip $keyValue \n";
    }
    //------ ==================================== ------//

    //------ Custom Logo: macanta_custom_logo ------//
    $keyValue = 'macanta_custom_logo';
    $macanta_custom_logo = macanta_db_record_exist($keyName,$keyValue,$table);
    $SiteLogo = $CI->config->item('sitelogo');
    if($macanta_custom_logo == false && $SiteLogo){
        $LogoFile = FCPATH.'assets/custom_img/logo/'.$SiteLogo;
        if(is_file($LogoFile)){
            $imageData =  base64_encode(file_get_contents($LogoFile));
            $value = [
                'imageData' => $imageData,
                'filename' => $SiteLogo,
                'type' => mime_content_type($LogoFile)
            ];
            $DBData['value'] = json_encode($value);
            $DBData['key'] = $keyValue;
            $CI->db->insert('config_data', $DBData);
            unlink($LogoFile);
            $CI->db->where('key','sitelogo');
            $CI->db->delete('config_data');
        }
    }else{
        echo  "Skip $keyValue \n";
    }
    //------ ==================================== ------//

    //------ Install hash: macanta_install_hash ------//
    $keyValue = 'macanta_install_hash';
    $macanta_install_hash = macanta_db_record_exist($keyName,$keyValue,$table);
    if($macanta_install_hash == false){
        $installInfo = json_decode(macanta_get_install_info(), true);
        if(isset($installInfo['_macantaInstallHash']) && $installInfo['_macantaInstallHash'] != ''){
            $value = $installInfo['_macantaInstallHash'];
            $CI->config->set_item($keyValue,$value);
            $DBData['value'] = $value;
            $DBData['key'] = $keyValue;
            $CI->db->insert('config_data', $DBData);
        }
    }else{
        echo  "Skip $keyValue \n\n";
    }
    //------ ==================================== ------//

    //------ Create Tables: Infusionsoft ------//
    $SQLFile = APPPATH ."hooks/migrate.sql";
    if(is_file($SQLFile)){
        $SQLQuery = file_get_contents($SQLFile);
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);

        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
            //echo  $CI->db->last_query()."\n\n";
        }
    }
    //------ ==================================== ------//

    //------ Register Hooks : Infusionsoft Hooks ------//
    $AvailableHooks = [
        "appointment.add",
        "appointment.delete",
        "appointment.edit",
        "company.add",
        "company.delete",
        "company.edit",
        "contact.add",
        "contact.delete",
        "contact.edit",
        "contact.redact",
        "contactGroup.add",
        "contactGroup.applied",
        "contactGroup.delete",
        "contactGroup.edit",
        "contactGroup.removed",
        "invoice.add",
        "invoice.delete",
        "invoice.edit",
        "invoice.payment.add",
        "invoice.payment.delete",
        "invoice.payment.edit",
        "leadsource.add",
        "leadsource.delete",
        "leadsource.edit",
        "note.add",
        "note.delete",
        "note.edit",
        "opportunity.add",
        "opportunity.delete",
        "opportunity.edit",
        "opportunity.stage_move",
        "order.add",
        "order.delete",
        "order.edit",
        "product.add",
        "product.delete",
        "product.edit",
        "subscription.add",
        "subscription.delete",
        "subscription.edit",
        "task.add",
        "task.delete",
        "task.edit",
        "user.activate",
        "user.add",
        "user.edit"
    ];
    $hookUrl = $CI->config->item('base_url').'rest/v1/receive_hook';
    $keyValue = 'macanta_hook_subscriptions';
    $macanta_hook_subscriptions = macanta_db_record_exist($keyName,$keyValue,$table, true);
    if($macanta_hook_subscriptions !== false){
        $Hooks = json_decode($macanta_hook_subscriptions->value, true);
        $DBAction='update';
    }else{
        $Hooks = [];
        $DBAction='insert';
    }
    foreach ($AvailableHooks as $eventKey){
        if(!in_array($eventKey,$Hooks)){
            $CreateResult = infusionsoft_create_hook_subscriptions($eventKey,$hookUrl)->message;
            echo "$eventKey: ".$CreateResult->status."\n";
            if(isset($CreateResult->status) && $CreateResult->status == 'Verified'){
                $Hooks[] = $eventKey;
            }else{
                infusionsoft_delete_hook_subscription($CreateResult->key);
            }
        }
    }
    $DBData = [
        'value' => json_encode($Hooks)
    ];
    switch ($DBAction){
        case 'update':
            $CI->db->where('key','macanta_hook_subscriptions');
            $CI->db->update('config_data', $DBData);
            break;
        case 'insert':
            $DBData['key'] =  'macanta_hook_subscriptions';
            $CI->db->insert('config_data', $DBData);
            break;
    }

    //------ ==================================== ------//


}
function macanta_delete_hook_subscription(){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $SubscribedRestHooks = infusionsoft_get_hook_subscriptions()->message;
    $keyValue = 'macanta_hook_subscriptions';
    $table = 'config_data';
    $keyName = 'key';
    $Hooks = [];
    $macanta_hook_subscriptions = macanta_db_record_exist($keyName,$keyValue,$table, true);
    if($macanta_hook_subscriptions !== false){
        $Hooks = json_decode($macanta_hook_subscriptions->value, true);
    }
    if(sizeof($SubscribedRestHooks) > 0){
        foreach ($SubscribedRestHooks as $SubscribedRestHook){
            if(strpos($SubscribedRestHook->hookUrl, 'rest/v1/receive_hook') !== false){
                $eventKey = $SubscribedRestHook->eventKey;
                $status = $SubscribedRestHook->status;
                $key = $SubscribedRestHook->key;
                $HooksIndex = array_search($eventKey, $Hooks);
                unset($Hooks[$HooksIndex]);
                infusionsoft_delete_hook_subscription($key);
                $DBData = [
                    'value' => json_encode($Hooks)
                ];
                $CI->db->where('key','macanta_hook_subscriptions');
                $CI->db->update('config_data', $DBData);
                echo "$eventKey: deleted\n";
            }
        }
    }else{
        $DBData = [
            'value' => '[]'
        ];
        $CI->db->where('key','macanta_hook_subscriptions');
        $CI->db->update('config_data', $DBData);
    }
}
function macanta_logger($LogName,$Message){
    $CI =& get_instance();
    $CI->load->dbforge();

    $CI->dbforge->add_field('id');
    $CI->dbforge->add_field("`Name` text");
    $CI->dbforge->add_field("`Message` longtext");
    $CI->dbforge->add_field("`Created` text");
    // CREATE TABLE IF NOT EXIST
    $CI->dbforge->create_table('macanta_logs', TRUE);

    $DBData = [
        'Name' => $LogName,
        'Message' => $Message,
        'Created' => date('Y-m-d H:i:s')
    ];
    $CI->db->insert('macanta_logs',$DBData);

    $CI->db->where('Created <=', date('Y-m-d H:i:s', strtotime("-7 Days")));
    $CI->db->delete('macanta_logs');
}
function macanta_check_cd_query_table(){
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('ConnectedDataQuery'))
    {
        $SQLQuery = "
                CREATE TABLE IF NOT EXISTS `ConnectedDataQuery` (
                  `Id` int(15) NOT NULL AUTO_INCREMENT,
                  `QueryId` varchar(30) NOT NULL,
                  `Data` longtext NOT NULL,
                  `TagId` int(15) DEFAULT NULL,
                  `Status` varchar(30) NOT NULL,
                  `CreatedBy` varchar(100) NOT NULL DEFAULT 'system',
                  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `Update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`Id`),
                  UNIQUE KEY `QueryId` (`QueryId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_check_user_table()
{
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('MacantaUser'))
    {
        $SQLQuery = "
                CREATE TABLE `MacantaUser` (
                  `ContactId` int(11) NOT NULL,
                  `ContactLevel` varchar(50) NOT NULL,
                  `TemplateId` int(11) NOT NULL,
                  `Status` varchar(50) NOT NULL,
                  `Created` datetime NOT NULL,
                  `Updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                  KEY `ContactId` (`ContactId`),
                  KEY `TemplateId` (`TemplateId`),
                  CONSTRAINT `MacantaUser_ibfk_1` FOREIGN KEY (`ContactId`) REFERENCES `InfusionsoftContact` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
                  CONSTRAINT `MacantaUser_ibfk_2` FOREIGN KEY (`TemplateId`) REFERENCES `MacantaUserPermissionTemplates` (`TemplateId`) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                    ";

        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_check_userpermissiontemplate_table()
{
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('ConnectedDataAutomation'))
    {
        $SQLQuery = "
                CREATE TABLE `MacantaUserPermissionTemplates` (
                  `TemplateId` int(11) NOT NULL AUTO_INCREMENT,
                  `TemplateName` varchar(100) NOT NULL,
                  `TemplateDescription` text NOT NULL,
                  `TabAccess` longtext NOT NULL,
                  `SectionAccess` longtext NOT NULL,
                  `MQBAccess` longtext NOT NULL,
                  `Status` varchar(30) NOT NULL,
                  `Created` datetime NOT NULL,
                  `Updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`TemplateId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_check_cd_automation_table(){
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('ConnectedDataAutomation'))
    {
        $SQLQuery = "
                CREATE TABLE `ConnectedDataAutomation` (
                  `QueryId` varchar(30) NOT NULL,
                  `QueryLabel` text NOT NULL,
                  `QueryType` varchar(30) NOT NULL,
                  `ConnectedDataGroupName` varchar(100) NOT NULL,
                  `Data` longtext NOT NULL,
                  `Status` varchar(30) DEFAULT NULL,
                  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `Update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  UNIQUE KEY `QueryId` (`QueryId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_check_cd_automation_triggered_record_table(){
    $CI =& get_instance();
    macanta_check_cd_automation_table();
    if ( ! $CI->db->table_exists('ConnectedDataAutomationTriggerRecord'))
    {
        $SQLQuery = "
                CREATE TABLE `ConnectedDataAutomationTriggerRecord` (
                  `QueryId` text NOT NULL,
                  `LastTriggered` varchar(30) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_check_cd_automation_held_time_table(){
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('ConnectedDataAutomationHeldTime'))
    {
        $SQLQuery = "
                CREATE TABLE `ConnectedDataAutomationHeldTime` (
                  `QueryId` varchar(150) NOT NULL COMMENT 'TC-TA-ItemId-ContactId',
                  `ToExecute` longtext NOT NULL,
                  `TimeToRun` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `Status` enum('waiting','running') NOT NULL DEFAULT 'waiting',
                  PRIMARY KEY (`QueryId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_save_cd_query($Data){
    $CI =& get_instance();
    macanta_check_cd_query_table();
    if(macanta_db_record_exist('QueryId',$Data['QueryId'],'ConnectedDataQuery')){
        //Update
        $CI->db->where('QueryId',$Data['QueryId']);
        $saved = $CI->db->update('ConnectedDataQuery',$Data);
    }else{
        $saved = $CI->db->insert('ConnectedDataQuery',$Data);
    }
    return ['result'=>$saved,'query'=>$CI->db->last_query(),'error'=>$CI->db->error()];

}
function macanta_save_cd_automation($Data){
    $CI =& get_instance();
    macanta_check_cd_automation_table();
    if(macanta_db_record_exist('QueryId',$Data['QueryId'],'ConnectedDataAutomation')){
        //Update
        $CI->db->where('QueryId',$Data['QueryId']);
        $saved = $CI->db->update('ConnectedDataAutomation',$Data);
    }else{
        $saved = $CI->db->insert('ConnectedDataAutomation',$Data);
    }
    return ['result'=>$saved,'query'=>$CI->db->last_query(),'error'=>$CI->db->error()];

}
function macanta_get_trigger_conditions_by_active_automation_group($QueryId='',$ActiveOnly=true){
    $CI =& get_instance();
    macanta_check_cd_automation_table();
    macanta_check_cd_automation_triggered_record_table();
    macanta_check_cd_automation_held_time_table();
    if($QueryId !== '') $CI->db->where('QueryId',$QueryId);
    if ($ActiveOnly) $CI->db->where('Status','active');
    $CI->db->where('QueryType','AutomationGroups');
    $query = $CI->db->get('ConnectedDataAutomation');
    $IfThenPairGroup = [];
    foreach ($query->result() as $row) {
        $Data = json_decode($row->Data,true);
        foreach ($Data['IfCondition'] as $index => $IfConditionQueryId){
            if($Data['IfThenStatus'][$index] == 'inactive') continue; // skip if not active
            $IfThenPairGroup[$row->QueryLabel][] = [
                'IfQueryId'=> $IfConditionQueryId,
                'ThenQueryId' => $Data['ThenAction'][$index],
                'ThenWait' => $Data['ThenWait'][$index]
            ];
        }
    }
    $toProcess = [];
    foreach ($IfThenPairGroup as $Group => $IfThenPairs){
        foreach ($IfThenPairs as $IfThenPair){
            $CI->db->where('QueryId',$IfThenPair['IfQueryId']);
            $query = $CI->db->get('ConnectedDataAutomation');
            $return = $query->row();
            if (isset($return)) {
                $IfQuery = json_decode($return->Data);
            }

            unset($return);
            $CI->db->where('QueryId',$IfThenPair['ThenQueryId']);
            $query = $CI->db->get('ConnectedDataAutomation');
            $return = $query->row();
            if (isset($return)) {
                $ThenQuery = json_decode($return->Data);
            }
            $toProcess[$Group][] = ['If' => $IfQuery, 'Then' => $ThenQuery, 'Wait' => $IfThenPair['ThenWait']];
        }
    }
    return $toProcess;
}
function macanta_check_trigger_restart_after($IfQueryId, $RestartAfter){
    $CI =& get_instance();
    $CI->db->where('QueryId',$IfQueryId);
    $query = $CI->db->get('ConnectedDataAutomationTriggerRecord');
    $return = $query->row();
    if (isset($return)) {
        if(strtolower($RestartAfter) == 'never') return false;
        $LastTriggered = $return->LastTriggered;
        $theDate = date("Y-m-d", strtotime($RestartAfter));
        if ("1970-01-01" !== $theDate) {
            // valid data so compare
            if(strtotime($LastTriggered . " " . $RestartAfter) < time() ) return true;
            return false;
        }
        return false;
    }else{
        return true;
    }
}
function macanta_check_execute_automation_with_wait(){
    $CI =& get_instance();
    $CI->db->where('TimeToRun <=',date("Y-m-d H:i:s"));
    $CI->db->where('Status','waiting');
    $results = '';
    $query = $CI->db->get('ConnectedDataAutomationHeldTime');
    if ($query->result() && sizeof($query->result()) > 0){
        foreach ($query->result() as $row) {
            $ValidTriggers = unserialize($row->ToExecute);
            $DBData['Status'] = 'running';
            $CI->db->where('QueryId',$row->QueryId);
            $CI->db->update('ConnectedDataAutomationHeldTime',$DBData);
            $results =  macanta_execute_valid_triggers($ValidTriggers);
        }
    }
    if(!empty($results)){
        $return = json_encode($results)."\n\n";
    }else{
        $return = '';
    }
    return json_encode($return);
}
function macanta_check_automation_wait_time($WaitId, $ExecuteDetail, $WaitTime){
    //DROP TABLE `ConnectedDataAutomationHeldTime`;
    $CI =& get_instance();
    $CI->db->where('QueryId',$WaitId);
    $query = $CI->db->get('ConnectedDataAutomationHeldTime');
    $return = $query->row();
    if(empty($WaitTime)) return true;
    if (isset($return)) {
        return false;
    }else{
        //insert new QueryId
        $WaitIdArr = explode(':', $WaitId);
        $queryId = $WaitIdArr[0];
        $ToExecute[$queryId][] = $ExecuteDetail;
        $DBdata['QueryId'] = $WaitId;
        $DBdata['ToExecute'] = serialize($ToExecute);
        $DBdata['TimeToRun'] = date("Y-m-d H:i:s",strtotime($WaitTime));
        $CI->db->insert('ConnectedDataAutomationHeldTime', $DBdata);
        return false;
    }
}
function macanta_check_automation_process_table()
{
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('ConnectedDataAutomationProcess'))
    {
        $SQLQuery = "
                CREATE TABLE `ConnectedDataAutomationProcess` (
                  `Id` int(15) NOT NULL AUTO_INCREMENT,
                  `ProcessId` varchar(150) NOT NULL,
                  `Message` longtext NOT NULL,
                  `Created` datetime NOT NULL DEFAULT current_timestamp(),
                  `Status` varchar(30) NOT NULL,
                  `Priority` enum('yes','no') NOT NULL DEFAULT 'no',
                  PRIMARY KEY (`Id`),
                  UNIQUE KEY `ProcessId` (`ProcessId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_check_automation_group_to_process_table()
{
    $CI =& get_instance();
    if ( ! $CI->db->table_exists('ConnectedDataAutomationGroupToProcess'))
    {
        $SQLQuery = "
                CREATE TABLE `ConnectedDataAutomationGroupToProcess` (
                  `Id` int(15) NOT NULL AUTO_INCREMENT,
                  `Group` varchar(150) NOT NULL,
                  `Message` longtext NOT NULL,
                  `Offset` int(30) NOT NULL DEFAULT 0,
                  `Created` datetime NOT NULL DEFAULT current_timestamp(),
                  `Status` varchar(30) NOT NULL,
                  PRIMARY KEY (`Id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                ";
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_automation_insert_process($RecordId, $ToExecute = [], $Priority = false){
    $CI =& get_instance();
    $DBData = [
        'ProcessId' => $RecordId,
        'Message' => serialize($ToExecute),
        'Status' => 'queued'
    ];
    if($Priority) $DBData['Priority'] = 'yes';
    if(macanta_db_record_exist('ProcessId',$RecordId,'ConnectedDataAutomationProcess') == false){
        $CI->db->insert('ConnectedDataAutomationProcess', $DBData);
    }
}
function macanta_automation_insert_if_then_pairs_process($AutomationGroup, $IfThenPairs = []){
    $CI =& get_instance();
    macanta_check_automation_group_to_process_table();
    $DBData = [
        'Group' => $AutomationGroup,
        'Message' => json_encode($IfThenPairs),
        'Status' => 'queued'
    ];
    if(macanta_db_record_exist('Group',$AutomationGroup,'ConnectedDataAutomationGroupToProcess') == false){
        $CI->db->insert('ConnectedDataAutomationGroupToProcess', $DBData);
    }else{
        $DBData = [
            'Message' => json_encode($IfThenPairs)
        ];
        $CI->db->where("Group", $AutomationGroup);
        $CI->db->update('ConnectedDataAutomationGroupToProcess', $DBData);
    }
}
function macanta_get_running_atuomatuon_process($Table="ConnectedDataAutomationProcess", $Priority = false){
    $CI =& get_instance();
    $CI->db->where('Status','processing');
    if($Priority) $CI->db->where('Priority','yes');
    $query = $CI->db->get($Table);
    $RunningCount = $query->num_rows();
    return (int)$RunningCount;
}
function macanta_execute_automation_process($ThreadLimit = 10, $PriorityLimit = 5){
    $CI =& get_instance();
    $AppName = $CI->config->item('IS_App_Name');
    $ServicePath = "/var/www/macanta/shared/services/";

    /*Priority Part*/
    $CurrentRunning = macanta_get_running_atuomatuon_process('ConnectedDataAutomationProcess', true);
    $AvailableLimit = $PriorityLimit - $CurrentRunning;
    if($AvailableLimit > 0){
        $CI->db->where('Status','queued');
        $CI->db->where('Priority','yes');
        $CI->db->limit($AvailableLimit);
        $query = $CI->db->get('ConnectedDataAutomationProcess');
        $Log = [];
        foreach ($query->result() as $row) {
            $ProcessId = $row->ProcessId;
            $DBData = [
                'Status'=>'processing'
            ];
            $CI->db->where('ProcessId',$ProcessId);
            $CI->db->update('ConnectedDataAutomationProcess', $DBData);

            $service = 'execute/'.$AppName.'/'.$ProcessId;
            $exec_string2 = '/usr/bin/php '.$ServicePath.'proc.php '.$service.' >> '.$ServicePath.'apps/' . $AppName . '/execute_process.log & ';
            //file_put_contents(dirname(__FILE__)."/ExecString_".$CI->config->item('IS_App_Name').".txt","$exec_string2"."\n", FILE_APPEND);
            //file_put_contents(dirname(__FILE__)."/ExecString_".$CI->config->item('IS_App_Name').".txt","$ProcessId.."."\n", FILE_APPEND);
            exec($exec_string2,$output2);
            $Log[] = " Starting IfThen Process.. $ProcessId";
        }
    }

    /*Non-Priority Part*/
    $CurrentRunning = macanta_get_running_atuomatuon_process();
    $AvailableLimit = $ThreadLimit - $CurrentRunning;
    if($AvailableLimit <= 0){
        return "Thread Full!";
    }

    $CI->db->where('Status','queued');
    $CI->db->limit($AvailableLimit);
    $query = $CI->db->get('ConnectedDataAutomationProcess');
    $Log = [];
    foreach ($query->result() as $row) {
        $ProcessId = $row->ProcessId;
        $DBData = [
            'Status'=>'processing'
        ];
        $CI->db->where('ProcessId',$ProcessId);
        $CI->db->update('ConnectedDataAutomationProcess', $DBData);

        $service = 'execute/'.$AppName.'/'.$ProcessId;
        $exec_string2 = '/usr/bin/php '.$ServicePath.'proc.php '.$service.' >> '.$ServicePath.'apps/' . $AppName . '/execute_process.log & ';
        //file_put_contents(dirname(__FILE__)."/ExecString_".$CI->config->item('IS_App_Name').".txt","$exec_string2"."\n", FILE_APPEND);
        //file_put_contents(dirname(__FILE__)."/ExecString_".$CI->config->item('IS_App_Name').".txt","$ProcessId.."."\n", FILE_APPEND);
        exec($exec_string2,$output2);
        $Log[] = " Starting IfThen Process.. $ProcessId";
    }
    return $Log;
}
function macanta_check_group_process_service(){
    $CI =& get_instance();
    $AppName = $CI->config->item('IS_App_Name');
    if(defined('CMD') AND (CMD == 1) ){
        $CI->db->where('Status','processing');
        $query = $CI->db->get('ConnectedDataAutomationGroupToProcess');
        if ($query->num_rows() == 0){
            foreach ($query->result() as $row) {
                $Group = $row->Group;
                $service = 'group_execute/'.$AppName.'/'.base64_encode($Group);
                exec('ps -ef | awk \'$NF=="'.$service.'"  {print $2}\'',$ServiceResult);
                $ServiceArr = sizeof($ServiceResult) > 0 ? $ServiceResult[0]:0;
                if(!$ServiceArr){
                    $CI->db->where('Group',$Group);
                    $DBData = [
                        'Status' => 'queued'
                    ];
                    $CI->db->update("ConnectedDataAutomationGroupToProcess", $DBData);
                }
            }
        }
    }
}
function macanta_execute_automation_group_process($ThreadLimit = 3){
    $CI =& get_instance();
    $AGThreadLimit = $CI->config->item('AGThreadLimit');
    $ThreadLimit = $AGThreadLimit ? $AGThreadLimit:$ThreadLimit;
    macanta_check_automation_group_to_process_table();
    $AppName = $CI->config->item('IS_App_Name');
    $ServicePath = "/var/www/macanta/shared/services/";
    //macanta_check_group_process_service();
    $CurrentRunning = macanta_get_running_atuomatuon_process('ConnectedDataAutomationGroupToProcess');
    $AvailableLimit = $ThreadLimit - $CurrentRunning;

    if($AvailableLimit <= 0){
        return "Thread Full!";
    }

    $CI->db->where('Status','queued');
    $CI->db->limit($AvailableLimit);
    $query = $CI->db->get('ConnectedDataAutomationGroupToProcess');
    if ($query->num_rows() == 0){
        $DBData = [
            'Status'=>"queued"
        ];
        $CI->db->where('Status','waiting');
        $CI->db->update('ConnectedDataAutomationGroupToProcess', $DBData);
        $CurrentRunning = macanta_get_running_atuomatuon_process('ConnectedDataAutomationGroupToProcess');
        $AvailableLimit = $ThreadLimit - $CurrentRunning;
        $CI->db->where('Status','queued');
        $CI->db->limit($AvailableLimit);
        $query = $CI->db->get('ConnectedDataAutomationGroupToProcess');
    }
    $Log = '';
    foreach ($query->result() as $row) {
        $Group = $row->Group;
        $service = 'group_execute/'.$AppName.'/'.base64_encode($Group);
        $PID = get_pids($service);
        if($PID){
            //echo "Process Existing: $Group\n";
            continue;
        }
        $exec_string2 = '/usr/bin/php '.$ServicePath.'proc.php '.$service.' >> '.$ServicePath.'apps/' . $AppName . '/group_execute_process.log & ';
        exec($exec_string2,$output2);
        sleep(1); // giving time to produce the PID
        //echo "Process Starting: $Group\n";
        $Log .= "Process Starting: $Group\n";
    }
    return $Log;
}
function get_pids($service){
    $ConsumerOutput = [];
    exec('ps -ef | awk \'$NF=="'.$service.'"  {print $2}\'',$ConsumerOutput);
    $PID = sizeof($ConsumerOutput) > 0 ? $ConsumerOutput[0]:0;
    return $PID;
}
function macanta_log_file($Prefix,$Data){
    $CI =& get_instance();
    file_put_contents(dirname(__FILE__)."/{$Prefix}".$CI->config->item('IS_App_Name').".txt",$Data, FILE_APPEND);

}
function macanta_check_trigger_conditions_process($IfThenPairs, $Limit=50, $Offset=0){
    $CI =& get_instance();
    macanta_check_automation_process_table();
    macanta_check_automation_group_to_process_table();

    foreach ($IfThenPairs as $IfThenPair){
        $MacantaAction = $IfThenPair['Then'];
        $MacantaQuery = $IfThenPair['If'];
        $WaitTime = $IfThenPair['Wait'];
        $queryContact = $MacantaQuery['queryContact'];
        $Parameters = [];
        $CDFieldLogic = [];
        $Relationships = [];
        $Parsed = [];
        $connected_data =  strtolower($MacantaQuery['queryConnectedDataType']);
        foreach ($queryContact as $queryContactParameters){
            $Logic = $queryContactParameters['queryContactRelationshipFieldLogic'];
            $Logic = $Logic == "and" ? "and":"or";
            $Relationships[$Logic][] = strtolower($queryContactParameters['queryContactRelationship']);
        }
        //if($Logging) file_put_contents(dirname(__FILE__)."/MacantaQuery_".$CI->config->item('IS_App_Name').".txt",serialize($MacantaQuery)."\n\n", FILE_APPEND);

        foreach ($MacantaQuery['queryCDField'] as $QueryCDField){
            if(isset($QueryCDField['queryCDFieldValues'])){
                foreach ($QueryCDField['queryCDFieldValues'] as $queryCDFieldValue){
                    $Parameters[$QueryCDField['queryCDFieldName']][] = trim($QueryCDField['queryCDFieldOperator']." ".$queryCDFieldValue);
                }
            }
            else{
                $Parameters[$QueryCDField['queryCDFieldName']] = trim($QueryCDField['queryCDFieldOperator']." ".$QueryCDField['queryCDFieldValue']);
            }
            $CDFieldLogic[$QueryCDField['queryCDFieldName']]['logic'] = $QueryCDField['queryCDFieldLogic'];
        }
        foreach ($Parameters as $ParameterKeyName =>$ParameterDetails){
            if(is_array($ParameterDetails)){
                foreach ($ParameterDetails as $ParameterDetail){
                    $ParameterTemp = [$ParameterKeyName=>$ParameterDetail];
                    $ParsedTemp = infusionsoft_parse_criteria($ParameterTemp);
                    $Parsed[$ParameterKeyName][] = $ParsedTemp[$ParameterKeyName];
                }
            }
            else{
                $ParameterTemp = infusionsoft_parse_criteria([$ParameterKeyName=>$ParameterDetails]);
                $Parsed[$ParameterKeyName] = $ParameterTemp[$ParameterKeyName];
            }
        }
        if($MacantaQuery['queryId'] == 'tc_hsZB2UxYLdwgjLcI'){
            $Logging = true;
        }else{
            $Logging = false;
        }
        $ConnectedInfos = macanta_get_connected_info_by_groupname($connected_data,'','','',$MacantaQuery['queryCDField'], $MacantaQuery['queryId'],[],$Relationships, $Limit, $Offset, true, $Logging);
        if(sizeof($ConnectedInfos) == 0) {
            if($Logging) macanta_log_file("Expression_","No Connected Info Found! {$MacantaQuery['queryId']}"."\n");
            continue;
        }
        foreach ($ConnectedInfos[$connected_data] as $ItemId => $ItemDetails) {

            $ToExecute = [];

            //if(macanta_check_last_edited($ItemId) == false) continue;

            $ConnectedContact = $ItemDetails['connected_contact'];
            $Fields = $ItemDetails['fields'];
            $Priority = isset($ItemDetails['priority']) ? $ItemDetails['priority']:false;
            $SpecificIds = [];
            $FinalSpecificIds = [];
            $RecordIdTemp = $MacantaQuery['queryId'].':'.$MacantaAction['queryId'].':'.$ItemId;

            foreach ($ConnectedContact as $ContactId => $ContactDetails){
                $ContactPassed = false;
                $FailedNote = "";
                $ContactRelationshipsArr = $ContactDetails['relationships'];
                $RecordId = $RecordIdTemp.":".$ContactId;

                if(macanta_check_trigger_restart_after($RecordId, $MacantaQuery['queryRestartAfter']) == false) {
                    if($Logging) macanta_log_file("Expression_","{$RecordId} Restart Never"."\n");
                    continue;
                }; // skip if ealier then restart after

                if(macanta_db_record_exist('ProcessId',$RecordId,'ConnectedDataAutomationProcess') == true) {
                    if($Logging) macanta_log_file("Expression_","{$RecordId} Record Exist"."\n");
                    continue; // if existing in ConnectedDataAutomationProcess
                }

                $ExecuteDetail = ['ItemId'=>$ItemId,'contactId'=>$ContactId,'action'=>$MacantaAction];
                if(macanta_check_automation_wait_time($RecordId, $ExecuteDetail, $WaitTime) == false){
                    if($Logging) macanta_log_file("Expression_","{$RecordId} On Wating"."\n");
                    continue; //Skip if its in ConnectedDataAutomationHeldTime
                }

                $CI->db->select('Groups, CustomField');
                $CI->db->where('Id',$ContactId);
                $query = $CI->db->get('InfusionsoftContact');
                $Details = $query->row(0,'array');
                $ContactTags = explode(',',$Details['Groups']);
                $ContactTags = array_map('trim', $ContactTags);
                $ContactCustomFields = json_decode($Details['CustomField'],true);
                if($queryContact){
                    foreach ($queryContact as $Contact){
                        if(!is_object($Contact)) $Contact = (object) $Contact;

                        //file_put_contents(dirname(__FILE__)."/Contact_".$CI->config->item('IS_App_Name').".txt",serialize($Contact)."\n\n", FILE_APPEND);

                        if($Contact->queryContactRelationship == ''){
                            $ContactPassed = true;

                        }
                        else{
                            if($Contact->queryContactRelationshipFieldLogic == '' || $Contact->queryContactRelationshipFieldLogic == 'or'){
                                if($Contact->queryContactRelationship != ''){
                                    foreach ($ContactRelationshipsArr as $ContactRelationshipName){
                                        if($ContactRelationshipName == strtolower($Contact->queryContactRelationship)){
                                            $ContactPassed = true;
                                            break;
                                        }
                                    }
                                    if($ContactPassed){
                                        if($Contact->queryContactRelationshipCondition != ''){
                                            switch ($Contact->queryContactRelationshipCondition){
                                                case 'withAllTags':
                                                    $TagsArr = explode(',',$Contact->queryContactRelationshipTagsValue);
                                                    $TagsArr = array_map('trim', $TagsArr);
                                                    $TagDiff = array_diff($TagsArr,$ContactTags);
                                                    if(sizeof($TagDiff) > 0) {
                                                        $ContactPassed = false;
                                                        $FailedNote = "Missing Tags: ".json_encode($TagsArr);
                                                    }
                                                    break;
                                                case 'withoutAllTags':
                                                    $TagsArr = explode(',',$Contact->queryContactRelationshipTagsValue);
                                                    $TagsArr = array_map('trim', $TagsArr);
                                                    $TagDiff = array_diff($TagsArr,$ContactTags);
                                                    if($TagsArr != $TagDiff) {
                                                        $ContactPassed = false;
                                                        $FailedNote = "Has Tags: ".json_encode($TagsArr);
                                                    }
                                                    break;
                                                default:
                                                    $CustomFieldName = $Contact->queryContactRelationshipCondition;
                                                    if(isset($ContactCustomFields[$CustomFieldName])){

                                                    }
                                                    switch ($Contact->queryContactRelationshipOperator){
                                                        case '':
                                                            if($ContactCustomFields[$CustomFieldName] !=  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'is not':
                                                            if($ContactCustomFields[$CustomFieldName] ==  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'greater than':
                                                            if($ContactCustomFields[$CustomFieldName] <=  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'less than':
                                                            if($ContactCustomFields[$CustomFieldName] >=  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'between':
                                                            break;
                                                        case 'is null':
                                                            if($ContactCustomFields[$CustomFieldName] !=  '' ) $ContactPassed = false;
                                                            break;
                                                        case 'not null':
                                                            if($ContactCustomFields[$CustomFieldName] ==  '' ) $ContactPassed = false;
                                                            break;
                                                    }

                                                    if($ContactPassed == false) {
                                                        $FailedNote = "Custom FieldName Value: ! $CustomFieldName {$Contact->queryContactRelationshipConditionValue}";
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    else{
                                        $FailedNote = "$ContactRelationshipName != {$Contact->queryContactRelationship}";
                                    }

                                }
                            }
                            elseif($Contact->queryContactRelationshipFieldLogic == 'and'){
                                if($Contact->queryContactRelationship != ''){
                                    foreach ($ContactRelationshipsArr as $ContactRelationshipName){
                                        if($ContactRelationshipName != strtolower($Contact->queryContactRelationship)){
                                            $ContactPassed = false;
                                            $FailedNote = "$ContactRelationshipName != {$Contact->queryContactRelationship}";
                                            break;
                                        }
                                    }
                                    if($ContactPassed){
                                        if($Contact->queryContactRelationshipCondition != ''){
                                            switch ($Contact->queryContactRelationshipCondition){
                                                case 'withAllTags':
                                                    $TagsArr = explode(',',$Contact->queryContactRelationshipTagsValue);
                                                    $TagsArr = array_map('trim', $TagsArr);
                                                    $TagDiff = array_diff($TagsArr,$ContactTags);
                                                    if(sizeof($TagDiff) > 0) {
                                                        $ContactPassed = false;
                                                        $FailedNote = "Missing Tags: ".json_encode($TagsArr);
                                                    }
                                                    break;
                                                case 'withoutAllTags':
                                                    $TagsArr = explode(',',$Contact->queryContactRelationshipTagsValue);
                                                    $TagsArr = array_map('trim', $TagsArr);
                                                    $TagDiff = array_diff($TagsArr,$ContactTags);
                                                    if($TagsArr != $TagDiff) {
                                                        $ContactPassed = false;
                                                        $FailedNote = "Has Tags: ".json_encode($TagsArr);
                                                    }
                                                    break;
                                                default:
                                                    $CustomFieldName = $Contact->queryContactRelationshipCondition;
                                                    if(isset($ContactCustomFields[$CustomFieldName])){

                                                    }
                                                    switch ($Contact->queryContactRelationshipOperator){
                                                        case '':
                                                            if($ContactCustomFields[$CustomFieldName] !=  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'is not':
                                                            if($ContactCustomFields[$CustomFieldName] ==  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'greater than':
                                                            if($ContactCustomFields[$CustomFieldName] <=  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'less than':
                                                            if($ContactCustomFields[$CustomFieldName] >=  $Contact->queryContactRelationshipConditionValue) $ContactPassed = false;
                                                            break;
                                                        case 'between':
                                                            break;
                                                        case 'is null':
                                                            if($ContactCustomFields[$CustomFieldName] !=  '' ) $ContactPassed = false;
                                                            break;
                                                        case 'not null':
                                                            if($ContactCustomFields[$CustomFieldName] ==  '' ) $ContactPassed = false;
                                                            break;
                                                    }
                                                    if($ContactPassed == false) {
                                                        $FailedNote = "Custom FieldName Value: ! $CustomFieldName {$Contact->queryContactRelationshipConditionValue}";
                                                    }
                                                    break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                else{
                    $ContactPassed = true;
                }
                if($ContactPassed == true){
                    if($Logging) macanta_log_file("Expression_","Contact $ContactId of $ItemId Passed"."\n");
                    $ToExecute[$MacantaQuery['queryId']][] = $ExecuteDetail;
                }else{
                    if($Logging) macanta_log_file("Expression_","Contact $ContactId  of $ItemId Failed; Relationship: $FailedNote"."\n");

                }

            }
            //At this stage, if no contact passed Skipp it
            if(sizeof($ToExecute) == 0) continue;

            if(sizeof($Parsed) > 0){
                foreach ($Parsed as $ParsedFieldName => $ParsedDetails){
                    switch ($CDFieldLogic[$ParsedFieldName]['logic']){
                        case '':
                            $Logic = 'init';
                            break;
                        case 'or':
                            $Logic = 'or';
                            break;
                        case 'and':
                            $Logic = 'and';
                            break;
                        default:
                            $Logic = 'or';
                            break;
                    }
                    if(is_array($ParsedDetails)){
                        //todo: upload and ready for test
                        $SubFinalSpecificIds = [];
                        $SubSpecificIds = [];
                        foreach ($ParsedDetails as $ParsedDetail){
                            $Passed = infusionsoft_evaluate_parsed_criteria([$ParsedFieldName=>$ParsedDetail],$Fields,'Macanta Internal Automation A - '.$RecordIdTemp, $ItemId, $SubSpecificIds,$ConnectedContact, $queryContact, $Logging);
                            //CheckBox; always OR, so if $Passed == true, then break
                            foreach ($SubSpecificIds as $theContactId=>$theBool){
                                $SubFinalSpecificIds[$theContactId] = true;
                            }
                            if($Passed == true ) break;
                        }
                        if(sizeof($FinalSpecificIds) == 0 && sizeof($SpecificIds) > 0) {
                            $FinalSpecificIds = $SubFinalSpecificIds;
                        }else{
                            if($Logic == 'init' || $Logic == 'or' ){
                                //always OR
                                foreach ($SubFinalSpecificIds as $theContactId=>$theBool){
                                    if($theBool == true){
                                        $FinalSpecificIds[$theContactId] = true;
                                    }
                                }
                            }elseif ($Logic == 'and'){
                                //And Operation
                                foreach ($SubFinalSpecificIds as $theContactId=>$theBool){
                                    if($theBool == false){
                                        $FinalSpecificIds[$theContactId] = false;
                                    }
                                }
                            }
                        }
                        if($Logging) macanta_log_file("Expression_","Logic: ".$Logic."\n"."FinalSpecificIds: ".json_encode($FinalSpecificIds)."\n");

                    }
                    else{
                        $Passed = infusionsoft_evaluate_parsed_criteria([$ParsedFieldName=>$ParsedDetails],$Fields,'Macanta Internal Automation B - '.$RecordId, $ItemId, $SpecificIds, $ConnectedContact, $queryContact);
                        if(sizeof($FinalSpecificIds) == 0 && sizeof($SpecificIds) > 0) {
                            $FinalSpecificIds = $SpecificIds;
                        }else{
                            if($Logic == 'init' || $Logic == 'or'){
                                //OR Operation
                                foreach ($SpecificIds as $theContactId=>$theBool){
                                    if($theBool == true){
                                        $FinalSpecificIds[$theContactId] = true;
                                    }
                                }
                            }elseif ($Logic == 'and'){
                                //AND Operation
                                foreach ($SpecificIds as $theContactId=>$theBool){
                                    if($theBool == false){
                                        $FinalSpecificIds[$theContactId] = false;
                                    }
                                }
                            }
                        }
                    }
                    if($Logging) macanta_log_file("Expression_","Logic: ".$Logic."\n"."Final SpecificIds: ".json_encode($FinalSpecificIds)."\n");

                    $CDFieldLogic[$ParsedFieldName]['bool'] = $Passed;
                }
            }
            $Passed = $InitBool = true;
            foreach ($CDFieldLogic as $LogicFieldName=>$Val){
                if($Val['logic'] == '' && $Val['bool'] == false){
                    $Passed = $InitBool = false;
                }
                elseif($Val['logic'] == 'and' && $InitBool == false){
                    $Passed = false;
                    break;
                }
                elseif($Val['logic'] == 'and' && $Val['bool'] == false ){
                    $Passed = false;
                    break;
                }
                elseif($Val['logic'] == 'or' && $Val['bool'] == true ){
                    $Passed = true;
                    break;
                }
            }

            if ($Passed === true) {
                //Remove Specific Contacts that does not meet the criteria;
                if(sizeof($SpecificIds) > 0){
                    foreach ($ToExecute as $TheQueryId => $ExecuteItems){
                        foreach ($ExecuteItems as $ExecuteItemIndex => $ExecuteItem){
                            $theContactId = $ExecuteItem['contactId'];
                            if(!isset($FinalSpecificIds[$theContactId]) || $FinalSpecificIds[$theContactId] == false){
                                unset($ToExecute[$TheQueryId][$ExecuteItemIndex]);
                            }
                        }
                    }
                }

            }
            else{
                $ToExecute = [];
            }
            if(sizeof($ToExecute) > 0){
                macanta_automation_insert_process($RecordId, $ToExecute, $Priority);
            }
            macanta_execute_automation_process();
        }

    }
}
function macanta_check_last_edited($Id, $DateRef = '2020-05-07 15:00:00'){
    $CI =& get_instance();
    $CI->db->where('item_id', $Id);
    $CI->db->where('time >', $DateRef);
    $query = $CI->db->get('connected_data_history');
    if($query->num_rows() > 0) return true;

    return false;
}
function macanta_check_trigger_conditions(){
    $CI =& get_instance();
    macanta_check_automation_process_table();
    $toProcess = macanta_get_trigger_conditions_by_active_automation_group();
    try{
        foreach ($toProcess as $AutomationGroup =>$IfThenPairs){
            macanta_automation_insert_if_then_pairs_process($AutomationGroup, $IfThenPairs);
            macanta_execute_automation_group_process();
        }
    } catch (Exception $e) {
        $Error = $e->getMessage();
        file_put_contents(dirname(__FILE__)."/Error_".$CI->config->item('IS_App_Name').".txt",$Error."\n", FILE_APPEND);
    }
}
function macanta_execute_valid_triggers($ValidTriggers = []){
    $CI =& get_instance();
    $Return = [];
    if(sizeof($ValidTriggers) == 0) {
        macanta_check_trigger_conditions();
        return "Automation Process Queued";
    }
    foreach ($ValidTriggers as $TriggerId => $ActionDetailArr){
        //record the last trigger date
        $DBdata = [
            'LastTriggered' => date("Y-m-d H:i:s")
        ];
        foreach ($ActionDetailArr as $ActionDetails){
            $contactId = $ActionDetails['contactId'];
            if(!is_object($ActionDetails['action'])) $ActionDetails['action']  = (object) $ActionDetails['action'];
            $RecId = $TriggerId.':'.$ActionDetails['action']->queryId.':'.$ActionDetails['ItemId'].':'.$contactId;

            //Delete ConnectedDataAutomationHeldTime Record to reset Data
            $WaitId = $TriggerId.':'.$ActionDetails['action']->queryId.':'.$ActionDetails['ItemId'].':'.$contactId;


            if(macanta_db_record_exist('QueryId',$RecId,'ConnectedDataAutomationTriggerRecord') == false){
                $DBdata['QueryId'] = $RecId;
                $CI->db->insert('ConnectedDataAutomationTriggerRecord', $DBdata);
            }else{
                $CI->db->where('QueryId',$RecId);
                $CI->db->update('ConnectedDataAutomationTriggerRecord', $DBdata);
            }
            //execute trigger
            $webhookId = $ActionDetails['action']->queryConnectedDataHTTPPost;

            $connected_group = $ActionDetails['action']->queryConnectedDataType;
            $cd_guid = $ActionDetails['ItemId'];
            $update_fields = '';
            $connecteddata_id = '';
            $connected_relationship = trim($ActionDetails['action']->queryContact[0]->queryContactRelationship);
            $connected_contacts = [$contactId=>$connected_relationship];
            $PostData = [];
            $Data = [];
            $ConnectedInfos = macanta_get_connected_info_by_groupname($connected_group,$contactId,'',$cd_guid);
            $ConnectedInfoItemFields = $ConnectedInfos[strtolower($connected_group)][$cd_guid]['fields'];
            if(isset($ActionDetails['action']->queryCDField)){
                foreach ($ActionDetails['action']->queryCDField as $index => $FieldDetails){
                    if(!is_object($FieldDetails)) $FieldDetails  = (object) $FieldDetails;

                    $PostData[$FieldDetails->queryCDFieldName] = $FieldDetails->queryCDFieldValue;
                    $UserField = trim(html_entity_decode($FieldDetails->queryCDFieldName));
                    if(!empty($FieldDetails->queryCDFieldValues) && is_array($FieldDetails->queryCDFieldValues)){
                        $queryCDFieldValues = $FieldDetails->queryCDFieldValues;
                        $queryCDFieldValues = macanta_merged_fields($contactId, $queryCDFieldValues);
                        $queryCDFieldValues = macanta_cd_merged_fields($contactId, $queryCDFieldValues, $ConnectedInfoItemFields);
                        $Data[$UserField] = implode(',',$queryCDFieldValues);
                    }else{
                        $queryCDFieldValue = $FieldDetails->queryCDFieldValue;
                        if(isset($ConnectedInfos[strtolower($connected_group)][$cd_guid]['fields'][strtolower($UserField)])){
                            if($ConnectedInfos[strtolower($connected_group)][$cd_guid]['fields'][strtolower($UserField)]['field-type'] == 'Date' ||
                                $ConnectedInfos[strtolower($connected_group)][$cd_guid]['fields'][strtolower($UserField)]['field-type'] == 'DateTime' ){
                                $Format = $ConnectedInfos[strtolower($connected_group)][$cd_guid]['fields'][strtolower($UserField)]['field-type'] == 'Date' ? "Y-m-d":"Y-m-d H:i:s";
                                $theDate = date("Y-m-d", strtotime($queryCDFieldValue));
                                if ("1970-01-01" !== $theDate) {
                                    // valid data so compare

                                    /*if(date("H:i:s", strtotime($FieldDetails->queryCDFieldValue)) != "00:00:00" ){
                                        $theDate = date("Y-m-d H:i:s", strtotime($FieldDetails->queryCDFieldValue));
                                    }*/
                                    $theDate = date($Format, strtotime($queryCDFieldValue));
                                    $queryCDFieldValue = $theDate;
                                }
                            }
                        }
                        $queryCDFieldValue = macanta_merged_fields($contactId, $queryCDFieldValue);
                        $queryCDFieldValue = macanta_cd_merged_fields($contactId, $queryCDFieldValue, $ConnectedInfoItemFields);
                        $Data[$UserField]  = $queryCDFieldValue;
                    }


                }
            }

            $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false,$connected_group, $Data,$connected_contacts,false, $connecteddata_id, true, $cd_guid, false, false);
            //Execute HTTP POST
            $webhookResults = 'No Webhook Setup';
            if(!empty($webhookId)){

                $webhookResults = macanta_send_cd_automation_webhook($webhookId,$contactId,$ConnectedInfos,$cd_guid);
                macanta_logger('WebhookResults',json_encode($webhookResults));
            }
            $CI->db->where('QueryId',$WaitId);
            $CI->db->delete('ConnectedDataAutomationHeldTime');
            $Return[] = ['Action'=>$ActionDetails['action'],'Update'=>$Data,'Contacts'=>$connected_contacts,'Webhooks'=>$webhookResults];
        }


    }
    if(sizeof($Return) > 0)
    {
        //macanta_logger('TriggersAction',json_encode($Return));
    }else{
        return "No Action Triggered";
    }
    return $Return;
}
function macanta_cd_merged_fields($ContactId, $Content, $ConnectedInfoItemFields){
    $SearchArr = [];
    $ValuehArr = [];
    foreach ($ConnectedInfoItemFields as $LowercaseField => $Details){
        $SearchArr[]='~cd.'.$LowercaseField.'~';
        if(is_array($Details['value'])){
            if(isset($Details['value']['id_'.$ContactId])){
                $ValuehArr[] = $Details['value']['id_'.$ContactId];
            }else{
                $ValuehArr[] = $Details['default-value'];
            }
        }else{
            if(empty($Details['value'])){
                $ValuehArr[] = $Details['default-value'];
            }else{
                $ValuehArr[] = $Details['value'];
            }

        }
    }
    if(is_array($Content)){
        $NewContent = $Content;
        foreach ($Content as $Index => $ContentItem){
            //$ContentItem = strtolower($ContentItem);
            $ContentItem = str_replace('~CD','~cd',$ContentItem);
            $matches = [];
            preg_match_all("/~cd\.(.+?)~/", $Content, $matches);
            foreach ($matches[0] as $match){
                $lower_match = strtolower($match);
                $Content = str_replace($match,$lower_match,$Content);
            }
            $NewContent[$Index] = str_replace($SearchArr,$ValuehArr,$ContentItem);
            $NewContent[$Index] = preg_replace("/~cd\.(.+?)~/", "", $ContentItem);
        }
        $Content = $NewContent;
    }else{
        //$Content = strtolower($Content);
        $Content = str_replace('~CD','~cd',$Content);
        $matches = [];
        preg_match_all("/~cd\.(.+?)~/", $Content, $matches);
        foreach ($matches[0] as $match){
            $lower_match = strtolower($match);
            $Content = str_replace($match,$lower_match,$Content);
        }
        $Content = str_replace($SearchArr,$ValuehArr,$Content);
        $Content = preg_replace("/~cd\.(.+?)~/", "", $Content);
    }

    return $Content;
}
function macanta_merged_fields($ContactId, $Content){

    $Contact = infusionsoft_get_contact_by_id($ContactId, array('all'));
    $SearchArr = [];
    $ValuehArr = [];
    foreach ($Contact as $Key => $Value){
        $SearchArr[]='~Contact.'.$Key.'~';
        $ValuehArr[]=$Value;
    }
    if(is_array($Content)){
        $NewContent = $Content;
        foreach ($Content as $Index => $ContentItem){
            $NewContent[$Index] = str_replace($SearchArr,$ValuehArr,$ContentItem);
            $NewContent[$Index] = preg_replace("/~Contact\.(.+?)~/", "", $ContentItem);
        }
        $Content = $NewContent;
    }else{
        $Content = str_replace($SearchArr,$ValuehArr,$Content);
        $Content = preg_replace("/~Contact\.(.+?)~/", "", $Content);
    }

    return $Content;
}
function macanta_send_cd_automation_webhook($webhookId,$contactId,$ConnectedInfos,$cd_guid){
    macanta_create_table_RestHookOutbound();
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->db->where('QueryId',$webhookId);
    $query = $CI->db->get('ConnectedDataAutomation');
    $return = $query->row();
    $DataPassed = [];
    if (isset($return)) {
        $WebHookData = json_decode($return->Data,true);
        $queryPostURL = $WebHookData['queryPostURL'];
        $DataPassed['contactId'] = $contactId;
        $DataPassed['cd_guid'] = $cd_guid;
        $DataPassed['app_name'] = $CI->config->item('IS_App_Name');
        $DataPassed['connected_data'] = $WebHookData['queryConnectedDataType'];
        $ConnectedInfoItemFields = $ConnectedInfos[strtolower($DataPassed['connected_data'])][$cd_guid]['fields'];
        $DataPassed['relationship'] = empty($WebHookData['queryContact'][0]['queryContactRelationship']) ? 'any':$WebHookData['queryContact'][0]['queryContactRelationship'];
        $ContactInfo = infusionsoft_get_contact_by_id($contactId, array('all'));
        foreach ($WebHookData['queryCDField'] as $queryCDFields){
            $lowercaseFieldName = strtolower($queryCDFields['queryCDFieldName']);
            $FieldName = $queryCDFields['queryCDFieldName'];
            if(empty($queryCDFields['queryCDFieldValue']) && (!isset($queryCDFields['queryCDFieldValues']) || empty($queryCDFields['queryCDFieldValues']))){
                if(isset($ConnectedInfoItemFields[$lowercaseFieldName])){
                    if(empty($ConnectedInfoItemFields[$lowercaseFieldName]['value'])){
                        $DataPassed[$queryCDFields['queryCDFieldName']] = $ConnectedInfoItemFields[$lowercaseFieldName]['default-value'];
                    }else{
                        if(is_array($ConnectedInfoItemFields[$lowercaseFieldName]['value'])){
                            if(isset($ConnectedInfoItemFields[$lowercaseFieldName]['value']['id_'.$contactId])){
                                $DataPassed[$queryCDFields['queryCDFieldName']] = $ConnectedInfoItemFields[$lowercaseFieldName]['value']['id_'.$contactId];
                            }else{
                                $DataPassed[$queryCDFields['queryCDFieldName']] = $ConnectedInfoItemFields[$lowercaseFieldName]['default-value'];
                            }
                        }else{
                            $DataPassed[$queryCDFields['queryCDFieldName']] = $ConnectedInfoItemFields[$lowercaseFieldName]['value'];
                        }

                    }
                }else{
                    $DataPassed[$queryCDFields['queryCDFieldName']] = '';
                }
                if(isset($ContactInfo->{$FieldName})){
                    $DataPassed[$queryCDFields['queryCDFieldName']] = $ContactInfo->{$FieldName};
                }
            }
            else{
                if(!empty($queryCDFields['queryCDFieldValues']) && is_array($queryCDFields['queryCDFieldValues'])){
                    $queryCDFieldValues = macanta_merged_fields($contactId, $queryCDFields['queryCDFieldValues']);
                    $queryCDFieldValues = macanta_cd_merged_fields($contactId, $queryCDFieldValues, $ConnectedInfoItemFields);
                    $DataPassed[$queryCDFields['queryCDFieldName']] = implode(',',$queryCDFieldValues);
                }else{
                    $queryCDFieldValue = macanta_merged_fields($contactId, $queryCDFields['queryCDFieldValue']);
                    $queryCDFieldValue = macanta_cd_merged_fields($contactId, $queryCDFieldValue, $ConnectedInfoItemFields);
                    $DataPassed[$queryCDFields['queryCDFieldName']] = $queryCDFieldValue;
                }
            }

        }
        //print_r($DataPassed);
        $DBData = [
            'message' => json_encode($DataPassed),
            'postURL' => $queryPostURL,
            'status' => 'queued'

        ];
        $Result = $CI->db->insert('RestHookOutbound', $DBData);
        $Id = $CI->db->insert_id();
        return ['message'=>"Outbound Webhook Queued",'status'=>$Result,'RestHookOutboundID'=>$Id];
    }else return false;
}
function macanta_create_table_RestHookOutbound(){
    $CI =& get_instance();
    $SQLQuery ="CREATE TABLE `RestHookOutbound` (
              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `message` longtext COLLATE 'utf8mb4_general_ci' NOT NULL,
              `created` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP,
              `postURL` text COLLATE 'utf8mb4_general_ci' NOT NULL,
              `status` varchar(100) COLLATE 'utf8mb4_general_ci' NOT NULL,
              `results` longtext COLLATE 'utf8mb4_general_ci' NOT NULL
            ) COLLATE 'utf8mb4_general_ci';";

    if ( ! $CI->db->table_exists('RestHookOutbound'))
    {
        $SQLQueryArr = explode(';', $SQLQuery);
        array_pop($SQLQueryArr);
        foreach($SQLQueryArr as $statement){
            $statement = $statement . ";";
            $CI->db->query(trim($statement));
        }
    }
}
function macanta_get_cd_query($QueryId='', $ActiveOnly=false){
    $CI =& get_instance();
    macanta_check_cd_query_table();
    if($QueryId !== '') $CI->db->where('QueryId',$QueryId);
    if ($ActiveOnly) $CI->db->where('Status','active');
    $query = $CI->db->get('ConnectedDataQuery');
    $Data = [];
    foreach ($query->result() as $row) {
        $Data[] = json_decode($row->Data);
    }
    return $Data;
}
function macanta_match_wildcard( $wildcard_pattern, $haystack ) {
    $regex = str_replace(
        array("\*", "\?"), // wildcard chars
        array('.*','.'),   // regexp chars
        preg_quote($wildcard_pattern)
    );

    return preg_match('/^'.$regex.'$/is', $haystack);
}
function macanta_get_signature($Id){
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $CI->db->where('user_id',$Id);
    $CI->db->where('meta_key','EmailSignature');
    $query = $CI->db->get('users_meta');
    $row = $query->row();
    if (isset($row)) return $row->meta_value;
    return '';
}
function macanta_access_type($tags = ''){
    if(trim($tags) == '') return false;
    $CI =& get_instance();
    $CI->load->helper('rucksack_helper');
    $CI->load->helper('infusionsoft_helper');
    $tagsArr = explode(',',$tags);
    $access_level = json_decode($CI->config->item('access_level'), true);
    $access = [];
    //determine eccess level
    foreach($access_level as $userType => $tag){
        if(in_array($tag,$tagsArr)){
            $access[] = $userType;
        }
    }
    return sizeof($access > 0) ? implode(',',$access):false;
}
function macanta_get_data_reference_values($FieldId, $Force = false){
    $CI =& get_instance();
    $FieldData = manual_cache_loader('DataReferenceValues');
    if($FieldData){
        $PrimaryFieldData =  json_decode($FieldData,true);
        if(isset($PrimaryFieldData[$FieldId]) && $Force == false)
            return $PrimaryFieldData[$FieldId];
    }
    $CI->db->select('value');
    $CI->db->like('value',$FieldId);
    $query = $CI->db->get('connected_data');
    $PrimaryFieldData[$FieldId] = [];
    foreach ($query->result() as $row) {
        $row->value=json_decode($row->value);
        if(empty($row->value->$FieldId) || is_null($row->value->$FieldId)) continue;

        if(is_object($row->value->$FieldId))
        {
            foreach($row->value->$FieldId as $FieldVal)
            {
                $PrimaryFieldData[$FieldId][]=$FieldVal;
            }
        }
        else
        {
            $PrimaryFieldData[$FieldId][] = $row->value->$FieldId;
        }

    }
    manual_cache_writer('DataReferenceValues',json_encode($PrimaryFieldData),60); // valid for 30 secs
    return $PrimaryFieldData[$FieldId];
}