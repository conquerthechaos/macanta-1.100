<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 09/03/16
 * Time: 2:28 PM
 */
/*Get Dropbox Files*/
if (!function_exists('dropbox_get_files'))
{
    function dropbox_get_files($InfusionsoftID){
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $action = "dropbox_get_contact_files";
        $action_details = '{"contact_id":"'.$InfusionsoftID.'"}';
        $DropboxFiles = rucksack_request($action, $action_details );
        return $DropboxFiles;
    }
}
/*Get Dropbox Upload File*/
if (!function_exists('dropbox_upload_file'))
{
    function dropbox_upload_file($data){
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $Id = $data['ISId'];
        $filename = $data['filename'];
        $file_contents = $data['file_contents'];
        $action = "dropbox_save_file";
        $action_details = '{"contact_id":"'.$Id.'", "filename": "' . $filename . '", "file_contents": "' . $file_contents . '"}';
        rucksack_request($action, $action_details );
        return $action_details;
    }
}


