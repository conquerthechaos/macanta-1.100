<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Threads manager
 */

abstract class ThreadsTask
{
    protected $ppid; // The parent PID
    protected $pid; // This thread PID

    public function __construct ( ) {
        $this->ppid = posix_getpid();
    }

    public function fork ( )
    {
        $pid = pcntl_fork();
        if ( $pid == -1 )
            throw new Exception ("ThreadsManager#{$this->ppid}: Thread fork failed!");

        if ( $pid ) // We are in parent
            $this->pid = $pid;
        else // We are in child
            $this->run();
    }

    public function run ( ) {
        $this->pid = posix_getpid();
    }

    // Called from ThreadsManager when a thread is finished
    public function finish ( ) {
        return TRUE;
    }

    public function pid ( ) {
        return $this->pid;
    }
}

class ProcessTask extends ThreadsTask
{
    public $Data;
    public $AppName;
    public $ProcId;
    public $ScriptVersion;

    public function __construct ($JsonData,$AppName, $ProcId, $Version) {
        parent::__construct();
        $this->Data = $JsonData;
        $this->AppName = $AppName;
        $this->ProcId = $ProcId;
        $this->ScriptVersion = $Version;
    }
    public function run ( )
    {
        parent::run();
        ini_set('memory_limit', '2048M');
        $Log = "";
        $Log .= "Script Version: ".$this->ScriptVersion."\n";
        $Log .= "App Name: ".$this->AppName."\n";
        $Log .= "Environment: ".ENVIRONMENT."\n";
        $Log .= "CI_ENV: ".$_SERVER['CI_ENV']."\n";
        $Log .= "Date Processed: ".date("Y-m-d H:i:s")."\n";
        $Log .= "ProcId: ".$this->ProcId."\n";
        $Log .= "PID: ".$this->pid."\n";
        $Log .= "Data: \n";
        $Log .= base64_decode($this->Data)."\n";
        /*
        PROCESS HERE
        */


        $ProcResults = infusionsoft_connected_data_process($this->Data);

        // exit(0); disable to prevent daemon to stop working
        $Log .= "Process Results: \n";
        $Log .= json_encode($ProcResults)."\n";
        $Log .= "======================================\n";
        //if($ProcResults != false)
        file_put_contents(SERVICEPATH."apps/{$this->AppName}/process.log", $Log, FILE_APPEND);
        exec('kill -9 ' . $this->pid);
    }
}

class ThreadsManager
{

    protected $pool = array(); // Threads pool
    public $ProcId;
    public $AppName;
    public function __construct ($ProcId,$AppName) {

        $this->ProcId = $ProcId;
        $this->AppName = $AppName;
    }

    public function addTask ( $task ) {
        $this->pool[ ] = $task;
    }

    public function run ()
    {
        foreach ( $this->pool as $task )
            $task->fork();

        while ( TRUE )
        {
            $Message =  "[".date("Y-m-d H:i:s")."] >> TaskManager: Waiting for Process# ".$this->ProcId."..\n";
            file_put_contents(SERVICEPATH."apps/".$this->AppName."/stdout.log", $Message, FILE_APPEND);
            $pid = pcntl_wait( $status );
            if ( $pid == -1 ){
                $Message =  "[".date("Y-m-d H:i:s")."] >> TaskManager: Process# ".$this->ProcId." Done!\n\n";
                file_put_contents(SERVICEPATH."apps/".$this->AppName."/stdout.log", $Message, FILE_APPEND);
                break;
            }


            $this->finishTask( $pid );
        }

       // exit(0); disable to prevent daemon to stop working
    }

    public function finishTask ( $pid )
    {
        $task = $this->pidToTask( $pid );
        if ( ! $task )
            return FALSE;

        return $task->finish();
    }

    public function pidToTask ( $pid )
    {
        foreach ( $this->pool as $task )
            if ( $task->pid() == $pid )
                return $task;

        return NULL;
    }
}


?>