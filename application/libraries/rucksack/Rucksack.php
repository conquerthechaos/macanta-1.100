<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 30/01/2017
 * Time: 3:33 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('RUCKSACK_LIB', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('ROOT_PATH',dirname(dirname(dirname(RUCKSACK_LIB))).DIRECTORY_SEPARATOR);
require (RUCKSACK_LIB."includes/RucksackFacade.php");

class Rucksack extends RucksackFacade{
    public $LogConfig = array(
        "Errors" => true,
        "Messages" => false,
        "Results" => false
    );
    public $MasterResults;
    public $Applications = array(
        'InfusionsoftRS' => array(
            "master"=>true,     // true for master record, the ff. apps should be false
            "appKey" => "",     // no need because token is pre-generated and being refresh from http://rucksack.macanta.org/ automatically.
            "appSecret" => ""   // no need because token is pre-generated and being refresh from http://rucksack.macanta.org/ automatically.
        )
    );
    public $AuthorizedAction = array(
        'addContact',
        'applyTag',
        'removeTag',
        'countRecord',
        'createRecord',
        'updateRecord',
        'readRecord',
        'deleteRecord',
        'sendEmail',
        'getEmailTemplate',
        'getAllReportColumns',
        'getSavedSearchResultsAllFields',
        'getAppSetting',
        'getFormsHTML',
        'getWebFormMap',
        'addCustomField',
        'uploadFile',
        'restRetrieveCampaign',
        'restAddMultipleToCampaignSequence',
        'restRetrieveFile',
        'restEmailItem',
        'restEmailHistory',
        'restRetrieveFileLite',
        'restDeleteFile',
        'restRetrieveCampanyProfile',
        'restListHookSubscriptions',
        'restCreateHookSubscription',
        'restDeleteHookSubscription',
        'restAllEmailHistory'
    );
    public function __construct()
    {
        parent::__construct();
    }

    public function Call($Method, $Params){
        if(!in_array($Method, $this->AuthorizedAction)) return "Error: Unknown Action - ".$Method;
        $masterResults = $this->callMethod($Method, $Params);
        return $masterResults;
    }

}