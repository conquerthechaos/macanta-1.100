<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 02/03/16
 * Time: 6:59 PM
 */
// Follow standards
// Use Input Class
// Database Class

class Connecteddata extends CI_Model
{

    public $ItemQuery = "
        SELECT ConnectedDataGroup.GroupId,
               ConnectedDataGroup.Label AS GroupLabel,
               ConnectedDataItemFieldValue.ItemId,
               ConnectedDataContact.ContactId,
               InfusionsoftContact.FirstName,
               InfusionsoftContact.LastName,
               InfusionsoftContact.Email,
               ConnectedDataContact.RelationshipId,
               ConnectedDataRelationship.Label AS RelationshipName,
               ConnectedDataField.FieldId,
               ConnectedDataField.Label AS FieldLabel,
               ConnectedDataField.Type AS FieldType,
               ConnectedDataItemFieldValue.FieldValue,
               ConnectedDataSequence.InitialSequence,
               ConnectedDataSequence.NextSequence,
               ConnectedDataSequence.RestartAfter,
               ConnectedDataSequence.MoveAfter,
               ConnectedDataSequence.MoveWhen,
               ConnectedDataSequence.Parsed,
               ConnectedDataSequence.Status,
               ConnectedDataSequence.Meta
        FROM   ConnectedDataItem
        JOIN   ConnectedDataGroup ON ConnectedDataGroup.GroupId = ConnectedDataItem.GroupId
        JOIN   ConnectedDataItemFieldValue ON ConnectedDataItemFieldValue.ItemId = ConnectedDataItem.ItemId
        JOIN   ConnectedDataField ON ConnectedDataField.FieldId = ConnectedDataItemFieldValue.FieldId
        JOIN   ConnectedDataContact ON ConnectedDataContact.ItemId = ConnectedDataItemFieldValue.ItemId
        JOIN   ConnectedDataRelationship ON ConnectedDataRelationship.RelationshipId = ConnectedDataContact.RelationshipId
        JOIN   InfusionsoftContact ON InfusionsoftContact.Id = ConnectedDataContact.ContactId
        LEFT JOIN   ConnectedDataSequence ON ConnectedDataSequence.ContactId = ConnectedDataContact.ContactId AND ConnectedDataSequence.ItemId = ConnectedDataItemFieldValue.ItemId
    ";
    public $CD_Tables = [
        'ConnectedDataContact',
        'ConnectedDataField',
        'ConnectedDataFileAttachment',
        'ConnectedDataGroup',
        'ConnectedDataGroupRelationship',
        'ConnectedDataHistory',
        'ConnectedDataItem',
        'ConnectedDataItemFieldValue',
        'ConnectedDataRelationship',
        'ConnectedDataSequence'
    ];
    public $RecordPrefix = [
        'ConnectedDataContact' => '',
        'ConnectedDataField' => 'field_',
        'ConnectedDataFileAttachment' => '',
        'ConnectedDataGroup' => 'ci_',
        'ConnectedDataGroupRelationship' => '',
        'ConnectedDataHistory' => '',
        'ConnectedDataItem' => 'item_',
        'ConnectedDataItemFieldValue' => '',
        'ConnectedDataRelationship' => 're_',
        'ConnectedDataSequence' => ''
    ];


    public function __construct()
    {
        parent::__construct();
    }

    /*===============NEW FUNCTIONS===============================*/

    public function get_contact_by_id()
    {

    }

    public function generate_regular_groups($Group='')
    {
        $MacantaGroupTable = 'MacantaGroup';
        $MacantaFieldTable = 'MacantaField';

        //con_
        $ContactGroupId = macanta_uniqid('con_',24);
        $ContactFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'AccountId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address1Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address2Street1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address2Street2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address2Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address3Street1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address3Street2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Address3Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Anniversary',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'AssistantName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'AssistantPhone',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'BillingInformation',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Birthday',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'City',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'City2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'City3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Company',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'CompanyID',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'ContactNotes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'ContactType',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Country',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Country2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Country3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'CreatedBy',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Email',
                
                'Required' => 'yes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'EmailAddress2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'EmailAddress3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Fax1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Fax1Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Fax2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Fax2Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'FirstName',
                
                'Required' => 'yes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Groups',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'JobTitle',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'LastName',
                
                'Required' => 'yes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'LastUpdated',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'LastUpdatedBy',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Password',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone1Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'MiddleName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'OwnerID',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Nickname',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone2Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Leadsource',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'LeadSourceId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone2Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'PostalCode',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone1Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone3Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'PostalCode2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone5Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone5',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone5Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone4Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone4',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone3Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Phone4Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'PostalCode3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'ReferralCode',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'SpouseName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'State',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'State2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'State3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'StreetAddress1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'StreetAddress2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'TimeZone',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Title',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Suffix',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Username',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Validated',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'Website',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'ZipFour1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'ZipFour2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupId,
                'Label' => 'ZipFour3',
            ],
        ];

        //co_
        $CompanyGroupId = macanta_uniqid('co_',24);
        $CompanyFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'AccountId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address1Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address2Street1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address2Street2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address2Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address3Street1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address3Street2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Address3Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Anniversary',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'AssistantName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'AssistantPhone',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'BillingInformation',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Birthday',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'City',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'City2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'City3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Company',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'CompanyID',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'ContactNotes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'ContactType',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Country',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Country2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Country3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'CreatedBy',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Email',

                'Required' => 'yes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'EmailAddress2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'EmailAddress3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Fax1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Fax1Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Fax2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Fax2Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'FirstName',

                'Required' => 'yes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Groups',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'JobTitle',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'LastName',

                'Required' => 'yes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'LastUpdated',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'LastUpdatedBy',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Password',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone1Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'MiddleName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'OwnerID',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Nickname',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone2Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Leadsource',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'LeadSourceId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone2Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'PostalCode',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone1Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone3Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'PostalCode2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone5Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone5',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone5Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone4Ext',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone4',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone3Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Phone4Type',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'PostalCode3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'ReferralCode',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'SpouseName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'State',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'State2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'State3',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'StreetAddress1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'StreetAddress2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'TimeZone',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Title',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Suffix',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Username',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Validated',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'Website',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'ZipFour1',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'ZipFour2',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $CompanyGroupId,
                'Label' => 'ZipFour3',
            ],
        ];

        //actn_
        $ContactActionGroupId = macanta_uniqid('actn_',24);
        $ContactActionFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'Accepted',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'ActionDate',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'ActionDescription',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'ActionType',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'CompletionDate',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'ContactId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'CreatedBy',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'CreationDate',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'CreationNotes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'EndDate',
                'Type' => 'Date',
            ],

            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'IsAppointment',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'LastUpdated',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'LastUpdatedBy',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'Location',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'ObjectType',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'OpportunityId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'PopupDate',
                'Type' => 'Date',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'Priority',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactActionGroupId,
                'Label' => 'UserID',
            ],
        ];

        //congrp_
        $ContactGroupGroupId = macanta_uniqid('congrp_',24);
        $ContactGroupFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupGroupId,
                'Label' => 'GroupCategoryId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupGroupId,
                'Label' => 'GroupDescription',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupGroupId,
                'Label' => 'GroupName',
            ]
        ];

        //congrpassign_
        $ContactGroupAssignGroupId = macanta_uniqid('congrpassign_',24);
        $ContactGroupAssignFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupAssignGroupId,
                'Label' => 'ContactId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupAssignGroupId,
                'Label' => 'ContactGroup',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupAssignGroupId,
                'Label' => 'GroupId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupAssignGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date'
            ]
        ];

        //congrpcat_
        $ContactGroupCategoryGroupId = macanta_uniqid('congrpcat_',24);
        $ContactGroupCategoryFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupAssignGroupId,
                'Label' => 'CategoryDescription',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $ContactGroupAssignGroupId,
                'Label' => 'CategoryName',
            ]
        ];

        //formtab_
        $DataFormTabGroupId = macanta_uniqid('formtab_',24);
        $DataFormTabFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormTabGroupId,
                'Label' => 'FormId',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormTabGroupId,
                'Label' => 'TabName',
            ]
        ];

        //formgroup_
        $DataFormGroupGroupId = macanta_uniqid('formgroup_',24);
        $DataFormGroupFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormGroupGroupId,
                'Label' => 'Name',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormGroupGroupId,
                'Label' => 'TabId',
            ]
        ];

        //formfield_
        /*
        FormId
            -1 = Contact
            -3 = Referral Partner
            -4 = Opportunity
            -6 = Company
            -5 = Task/Note/Apt
            -9 = Order


        DataType
        Name                    Integer	Has Options
        Phone Number	        1	    no
        Social Security Number	2	    no
        Currency	            3	    no
        Percent	                4	    no
        State	                5	    no
        Yes/No	                6	    no
        Year	                7	    no
        Month	                8	    no
        Day of Week	            9	    no
        Name	                10	    no
        Decimal Number	        11	    no
        Whole Number	        12	    no
        Date	                13	    no
        Date/Time	            14	    no
        Text	                15	    no
        Text Area	            16	    no
        List Box	            17	    yes
        Website	                18	    no
        Email	                19	    no
        Radio	                20	    yes
        Dropdown	            21	    yes
        User	                22	    yes
        Drilldown	            23	    yes
        */
        $DataFormFieldGroupId = macanta_uniqid('formfield_',24);
        $DataFormFieldFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'DataType',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'DefaultValue',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'FormId',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'GroupId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'Label',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'ListRows',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'Name',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $DataFormFieldGroupId,
                'Label' => 'Values'
            ]
        ];

        //emailaddstat_
        /*
        Type
            SingleOptIn - This person has opted in but not confirmed their email address
            UnengagedMarketable - This person has been unengaged for a period of time
            DoubleOptin - This person has clicked an email confirmation link.
            Confirmed - This person has confirmed their email address.
            UnengagedNonMarketable - This person has been unengaged for too long a period of time to be marketed to
            NonMarketable - There is no evidence that this person has consented to receive marketing.
            Lockdown - This person was added while the app was locked down.
            Bounce - This person's email address has bounced too many times.
            HardBounce - This person's email address has hard bounced.
            Manual - This person has opted out of all email marketing.
            Admin - This person was manually opted out by an administrator.
            ListUnsubscribe - This person has opted out of all email marketing.
            Feedback - This person reported spam messages to his/her provider.
            Spam - This person provided feedback when opting out.
            Invalid - This email address failed the regular expression validation
        */
        $EmailAddStatusGroupId = macanta_uniqid('emailaddstat_',24);
        $EmailAddStatusFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailAddStatusGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailAddStatusGroupId,
                'Label' => 'Email',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailAddStatusGroupId,
                'Label' => 'LastClickDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailAddStatusGroupId,
                'Label' => 'LastOpenDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailAddStatusGroupId,
                'Label' => 'LastSentDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailAddStatusGroupId,
                'Label' => 'Type'
            ]
        ];

        //emailsent_
        $EmailSentGroupId = macanta_uniqid('emailsent_',24);
        $EmailSentFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'subject',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'headers',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'contact_id',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'sent_to_address',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'sent_to_cc_addresses',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'sent_to_bcc_addresses',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'sent_from_address',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'sent_from_reply_address',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'sent_date',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'received_date',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'opened_date',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'clicked_date',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'original_provider',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $EmailSentGroupId,
                'Label' => 'original_provider_id',
            ]
        ];

        //inv_
        $InvoiceGroupId = macanta_uniqid('inv_',24);
        $InvoiceFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'AffiliateId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'ContactId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'CreditStatus',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'Description',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'InvoiceTotal',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'InvoiceType',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'JobId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'LastUpdated',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'LeadAffiliateId'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'PayPlanStatus'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'PayStatus'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'ProductSold',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'PromoCode',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'RefundStatus',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'Synced',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'TotalDue',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $InvoiceGroupId,
                'Label' => 'TotalPaid',
                'Type' => 'Currency'
            ]
        ];

        //lead_
        $LeadGroupId = macanta_uniqid('lead_',24);
        $LeadFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'AffiliateId',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'ContactID',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'CreatedBy'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'DateCreated',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'DateInStage',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'EstimatedCloseDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'IncludeInForecast',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'LastUpdated',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'LastUpdatedBy'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'Leadsource'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'MonthlyRevenue',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'NextActionDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'NextActionNotes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'Objection',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'OpportunityNotes',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'OpportunityTitle'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'OrderRevenue',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'ProjectedRevenueHigh',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'ProjectedRevenueLow',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'StageID'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'StatusID'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadGroupId,
                'Label' => 'UserID'
            ]
        ];

        //leadsrccat_
        $LeadSourceCategoryGroupId = macanta_uniqid('leadsrccat_',24);
        $LeadSourceCategoryFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceCategoryGroupId,
                'Label' => 'Description'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceCategoryGroupId,
                'Label' => 'Name',
            ]
        ];

        //leadsrc_
        $LeadSourceGroupId = macanta_uniqid('leadsrc_',24);
        $LeadSourceFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'CostPerLead',
                'Type' => 'Currency'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'Description'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'EndDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'LeadSourceCategoryId'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'Medium'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'Message'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'Name'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'StartDate',
                'Type' => 'Date'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'Status'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $LeadSourceGroupId,
                'Label' => 'Vendor'
            ]
        ];


        //savedfltr_
        $SavedFilterGroupId = macanta_uniqid('savedfltr_',24);
        $SavedFilterFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $SavedFilterGroupId,
                'Label' => 'FilterName'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $SavedFilterGroupId,
                'Label' => 'ReportStoredName',
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $SavedFilterGroupId,
                'Label' => 'UserId'
            ]
        ];

        //stage_
        $StageGroupId = macanta_uniqid('stage_',24);
        $StageFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $StageGroupId,
                'Label' => 'StageName'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $StageGroupId,
                'Label' => 'StageOrder',
                'Type' => 'Number'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $StageGroupId,
                'Label' => 'TargetNumDays',
                'Type' => 'Number'
            ]
        ];

        //template_
        $TemplateGroupId = macanta_uniqid('template_',24);
        $TemplateFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $TemplateGroupId,
                'Label' => 'Categories'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $TemplateGroupId,
                'Label' => 'PieceTitle'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $TemplateGroupId,
                'Label' => 'PieceType'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $TemplateGroupId,
                'Label' => 'PieceContent'
            ]
        ];

        //user_
        $UserGroupId = macanta_uniqid('user_',24);
        $UserFields = [
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'City'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Email'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'EmailAddress2'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'EmailAddress3'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'FirstName'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'GlobalUserId'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'HTMLSignature'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'LastName'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'MiddleName'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Nickname'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Partner'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Phone1'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Phone1Ext'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Phone1Type'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Phone2'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Phone2Ext'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Phone2Type'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'PostalCode'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Signature'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'SpouseName'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'State'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'StreetAddress1'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'StreetAddress2'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Suffix'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'Title'
            ],
            [
                'FieldId' => macanta_uniqid('field_',24),
                'GroupId' => $UserGroupId,
                'Label' => 'ZipFour1'
            ]
        ];

        $RegularGroups = [
            'Contact' => ['properties' =>
                [
                    'GroupId' => $ContactGroupId,
                    'Label' => 'Contact',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $ContactFields
            ],
            'Company' => ['properties' =>
                [
                    'GroupId' => $CompanyGroupId,
                    'Label' => 'Company',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $CompanyFields
            ],
            'ContactAction' => ['properties' =>
                [
                    'GroupId' => $ContactActionGroupId,
                    'Label' => 'ContactAction',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $ContactActionFields
            ],
            'ContactGroup' => ['properties' =>
                [
                    'GroupId' => $ContactGroupGroupId,
                    'Label' => 'ContactGroup',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $ContactGroupFields
            ],
            'ContactGroupAssign' => ['properties' =>
                [
                    'GroupId' => $ContactGroupAssignGroupId,
                    'Label' => 'ContactGroupAssign',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $ContactGroupAssignFields
            ],
            'ContactGroupCategory' => ['properties' =>
                [
                    'GroupId' => $ContactGroupCategoryGroupId,
                    'Label' => 'ContactGroupCategory',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $ContactGroupCategoryFields
            ],
            'DataFormTab' => ['properties' =>
                [
                    'GroupId' => $DataFormTabGroupId,
                    'Label' => 'DataFormTab',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $DataFormTabFields
            ],
            'DataFormGroup' => ['properties' =>
                [
                    'GroupId' => $DataFormGroupGroupId,
                    'Label' => 'DataFormGroup',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $DataFormGroupFields
            ],
            'DataFormField' => ['properties' =>
                [
                    'GroupId' => $DataFormFieldGroupId,
                    'Label' => 'DataFormField',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $DataFormFieldFields
            ],
            'EmailAddStatus' => ['properties' =>
                [
                    'GroupId' => $EmailAddStatusGroupId,
                    'Label' => 'EmailAddStatus',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $EmailAddStatusFields
            ],
            'EmailSent' => ['properties' =>
                [
                    'GroupId' => $EmailSentGroupId,
                    'Label' => 'EmailSent',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $EmailSentFields
            ],
            'Invoice' => ['properties' =>
                [
                    'GroupId' => $InvoiceGroupId,
                    'Label' => 'Invoice',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $InvoiceFields
            ],
            'Lead' => ['properties' =>
                [
                    'GroupId' => $LeadGroupId,
                    'Label' => 'Lead',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $LeadFields
            ],
            'LeadSourceCategory' => ['properties' =>
                [
                    'GroupId' => $LeadSourceCategoryGroupId,
                    'Label' => 'LeadSourceCategory',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $LeadSourceCategoryFields
            ],
            'LeadSource' => ['properties' =>
                [
                    'GroupId' => $LeadSourceGroupId,
                    'Label' => 'LeadSource',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $LeadSourceFields
            ],
            'SavedFilter' => ['properties' =>
                [
                    'GroupId' => $SavedFilterGroupId,
                    'Label' => 'SavedFilter',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $SavedFilterFields
            ],
            'Stage' => ['properties' =>
                [
                    'GroupId' => $StageGroupId,
                    'Label' => 'Stage',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $StageFields
            ],
            'Template' => ['properties' =>
                [
                    'GroupId' => $TemplateGroupId,
                    'Label' => 'Template',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $TemplateFields
            ],
            'User' => ['properties' =>
                [
                    'GroupId' => $UserGroupId,
                    'Label' => 'User',
                    'Type' => 'regular',
                    'Status' => 'active',
                ],
                'fields' => $UserFields
            ]
        ];

        if($Group == ''){
            foreach ($RegularGroups as $RegularGroupName=>$RegularData){
                if(false == macanta_db_record_exist(['Label','Type'],[$RegularData['properties']['Label'],$RegularData['properties']['Type']],$MacantaGroupTable)){
                    $GroupInsertresult = $this->db->insert($MacantaGroupTable, $RegularData['properties']);
                    if($GroupInsertresult){
                        foreach ($RegularData['fields'] as $Fields){
                            $FieldInsertResult = $this->db->insert($MacantaFieldTable, $Fields);
                        }
                    }
                }
            }
        }elseif(isset($RegularGroups[$Group])){
            if(false == macanta_db_record_exist(['Label','Type'],[$RegularGroups[$Group]['properties']['Label'],$RegularGroups[$Group]['properties']['Type']],$MacantaGroupTable)){
                $GroupInsertresult = $this->db->insert($MacantaGroupTable, $RegularGroups[$Group]['properties']);
                if($GroupInsertresult){
                    foreach ($RegularGroups[$Group]['fields'] as $Fields){
                        $FieldInsertResult = $this->db->insert($MacantaFieldTable, $Fields);
                    }
                }
            }
        }else{
            return false;
        }
        return true;
    }

    public function macanta_get_fields($GroupId){
        $Fields = [];
        $this->db->where('GroupId', $GroupId);
        $query = $this->db->get('MacantaField');
        foreach ($query->result_array() as $ArrKey=>$FieldData) {
            $Fields[$FieldData['Label']] = $FieldData;
        }
        return $Fields;
    }

    /**
     * Find Record
     * @param array Contact Data
     * @param array Field Setting for a given GroupId
     * @return (ItemId)   bool,string
     */
    public function macanta_find_record($DataArr, $FieldArr){
        //check by email
        if(!empty($DataArr['Email'])){
            $this->db->where('FieldValue', $DataArr['Email']);
            $this->db->where('FieldId', $FieldArr['Email']['FieldId']);
            $query = $this->db->get('MacantaFieldValue');
            $row = $query->row();
            if (isset($row))
            {
                return $row->ItemId;
            }
        }
        if(!empty($DataArr['Id'])){
            $this->db->where('MetaName', 'InfusionsoftId');
            $this->db->where('MetaValue', $DataArr['Id']);
            $query = $this->db->get('MacantaItemMeta');
            $row = $query->row();
            if (isset($row))
            {
                return $row->ItemId;
            }
        }
        if(!empty($DataArr['IdLocal'])){
            $this->db->where('MetaName', 'InfusionsoftId');
            $this->db->where('MetaValue', $DataArr['IdLocal']);
            $query = $this->db->get('MacantaItemMeta');
            $row = $query->row();
            if (isset($row))
            {
                return $row->ItemId;
            }
        }

        // Attemp to use Phone1 FirstName and LastName to check duplicates, Pottential issue is having multiple thesame LastName
        /*if(!empty($DataArr['Phone1']) && !empty($DataArr['FirstName'])  && !empty($DataArr['LastName'])){
            $query = $this->db->select('*')->from('MacantaFieldValue')
                ->group_start()
                    ->where('FieldId', $FieldArr['Phone1']['FieldId'])
                    ->where('FieldValue', $DataArr['Phone1'])
                ->group_end()
                ->or_group_start()
                    ->where('FieldId', $FieldArr['FirstName']['FieldId'])
                    ->where('FieldValue', $DataArr['FirstName'])
                ->group_end()
                ->or_group_start()
                    ->where('FieldId', $FieldArr['LastName']['FieldId'])
                    ->where('FieldValue', $DataArr['LastName'])
                ->group_end()
                ->get();
            if (sizeof($query->result()) > 0){
                foreach ($query->result() as $row) {
                    // Logics Here
                }
            }
        }*/
        return false;
    }
    public function macanta_get_records($Table, &$ResultCount, $FieldValueCondition=[], $Limit = 1000, $Offset = 0)
    {
        if(sizeof($FieldValueCondition) > 0){
            foreach ($FieldValueCondition as $Field=>$Value){
                $this->db->where($Field, $Value);
            }
        }
        $this->db->limit($Limit, $Offset);
        $query = $this->db->get($Table);
        $ResultCount = $query->num_rows();
        return $query->result_array();
    }

    public function migrate_contact_from_local(){
        //GET Contact GroupId
        $this->db->where('Label', 'Contact');
        $this->db->where('Type', 'regular');
        $query = $this->db->get('MacantaGroup');
        $row = $query->row();
        if (isset($row))
        {
            $ResultCount = 1;
            $Limit = 1000;
            $Offset = 0;
            $ContactGroupId = $row->GroupId;

            //Get Fields Information From GroupId
            $Fields = $this->macanta_get_fields($ContactGroupId);
            while ($ResultCount >= 1){
                $results = $this->macanta_get_records('InfusionsoftContact', $ResultCount, [], $Limit, $Offset);
                $Offset = $Offset+$Limit;
                foreach ($results  as $ArrKey=>$FieldData) {
                    //CheckForExistingRecord
                    $ExistingRecord = $this->macanta_find_record($FieldData,$Fields);
                    if($ExistingRecord == false){
                        $ContactItemId = macanta_uniqid('item_',24);
                        //Add Record to MacantaItem
                        $MacantaItem = [
                            'ItemId' => $ContactItemId,
                            'GroupId' => $ContactGroupId,
                        ];
                        $this->db->insert('MacantaItem', $MacantaItem);

                        //Add records to MacantaFieldValue
                        foreach ($FieldData as $FieldName => $FieldValue){
                            if($FieldName == 'Origin') continue;
                            if($FieldName == 'Id' || $FieldName == 'IdLocal'){
                                if($FieldName == 'Id'){
                                    $MacantaItemMeta = [
                                        'ItemId'=>$ContactItemId,
                                        'MetaName' => 'InfusionsoftId',
                                        'MetaValue' => $FieldValue
                                    ];
                                    $this->db->insert('MacantaItemMeta', $MacantaItemMeta);
                                }
                            }elseif($FieldName == 'CustomField'){
                                $FieldValueArr = json_decode($FieldValue, true);
                                foreach ($FieldValueArr as $CusFieldName => $CusFielValue){
                                    $MacantaItemMeta = [
                                        'ItemId'=>$ContactItemId,
                                        'MetaName' => $CusFieldName,
                                        'MetaValue' => json_encode($CusFielValue)
                                    ];
                                    $this->db->insert('MacantaItemMeta', $MacantaItemMeta);
                                }
                            }else{
                                //todo: convert json dates to date text yyyy-mm-dd
                                if(empty($FieldValue)) continue;
                                $MacantaFieldValue =[
                                    'ItemId' => $ContactItemId,
                                    'FieldId' => $Fields[$FieldName]['FieldId'],
                                    'FieldValue' => $FieldValue
                                ];
                                $this->db->insert('MacantaFieldValue', $MacantaFieldValue);
                            }

                        }
                    }else{
                        //todo: import skipp or update
                    }

                }
            }



        }else{
            //Generate Group and its fields first
        }
        /*$query = $this->query('SELECT COUNT(*) AS numrows  FROM  MacantaItem');
        if ($query->num_rows() === 0)
        {
            $Count =  0;
        }else{
            $query = $query->row();
            $Count = $query->numrows;
        }*/

    }



















    /*===========================================================*/
    /**
     * Update Record
     * @param array $UpdateValues
     * @param string $Table
     * @return    bool,string
     */
    public function update_record($UpdateValues, $Table)
    {
        if (!in_array($Table, $this->CD_Tables)) {
            echo "ERROR: Unknown Table ({$Table}) ";
            return false;
        }
        $TableFields = $this->db->list_fields($Table);
        foreach ($UpdateValues as $FieldName => $FieldValue) {
            if (!in_array($FieldName, $TableFields)) {
                echo "ERROR: Unknown Field ({$FieldName}) ";
                return false;
            }
        }
        if ($Table == 'ConnectedDataField') {
            $this->db->where('FieldId', $UpdateValues['FieldId']);
        }
        if ($Table == 'ConnectedDataGroup') {
            $this->db->where('GroupId', $UpdateValues['GroupId']);
        }
        if ($Table == 'ConnectedDataItem') {
            $this->db->where('ItemId', $UpdateValues['ItemId']);
        }
        if ($Table == 'ConnectedDataRelationship') {
            $this->db->where('RelationshipId', $UpdateValues['RelationshipId']);
        }
        if ($Table == 'ConnectedDataHistory') {
            $this->db->where('ItemId', $UpdateValues['ItemId']);
        }

        if ($Table == 'ConnectedDataFileAttachment') {
            $this->db->where('ItemId', $UpdateValues['ItemId']);
            $this->db->where('B2FileId', $UpdateValues['B2FileId']);
        }
        if ($Table == 'ConnectedDataGroupRelationship') {
            $this->db->where('GroupId', $UpdateValues['GroupId']);
            $this->db->where('RelationshipId', $UpdateValues['RelationshipId']);
        }
        if ($Table == 'ConnectedDataItemFieldValue') {
            $this->db->where('ItemId', $UpdateValues['ItemId']);
            $this->db->where('FieldId', $UpdateValues['FieldId']);
        }
        if ($Table == 'ConnectedDataSequence') {
            $this->db->where('ContactId', $UpdateValues['ContactId']);
            $this->db->where('ItemId', $UpdateValues['ItemId']);
            $this->db->where('InitialSequence', $UpdateValues['InitialSequence']);
        }
        $UpdateResult = $this->db->update($Table, $UpdateValues);
        return $UpdateResult;
    }

    /**
     * Delete Record
     * @param array $DeleteValues
     * @param string $Table
     * @return    bool,string
     */
    public function delete_record($DeleteValues, $Table)
    {
        if (!in_array($Table, $this->CD_Tables)) {
            echo "ERROR: Unknown Table ({$Table}) ";
            return false;
        }
        if ($Table == 'ConnectedDataField') {
            $this->db->where('FieldId', $DeleteValues['FieldId']);
        }
        if ($Table == 'ConnectedDataGroup') {
            $this->db->where('GroupId', $DeleteValues['GroupId']);
        }
        if ($Table == 'ConnectedDataItem') {
            $this->db->where('ItemId', $DeleteValues['ItemId']);
        }
        if ($Table == 'ConnectedDataRelationship') {
            $this->db->where('RelationshipId', $DeleteValues['RelationshipId']);
        }
        if ($Table == 'ConnectedDataHistory') {
            $this->db->where('ItemId', $DeleteValues['ItemId']);
        }

        if ($Table == 'ConnectedDataFileAttachment') {
            $this->db->where('ItemId', $DeleteValues['ItemId']);
            $this->db->where('B2FileId', $DeleteValues['B2FileId']);
        }
        if ($Table == 'ConnectedDataGroupRelationship') {
            $this->db->where('GroupId', $DeleteValues['GroupId']);
            $this->db->where('RelationshipId', $DeleteValues['RelationshipId']);
        }
        if ($Table == 'ConnectedDataItemFieldValue') {
            $this->db->where('ItemId', $DeleteValues['ItemId']);
            $this->db->where('FieldId', $DeleteValues['FieldId']);
        }
        if ($Table == 'ConnectedDataSequence') {
            $this->db->where('ContactId', $DeleteValues['ContactId']);
            $this->db->where('ItemId', $DeleteValues['ItemId']);
            $this->db->where('InitialSequence', $DeleteValues['InitialSequence']);
        }
        $DeleteResult = $this->db->delete($Table);
        return $DeleteResult;
    }

    /**
     * Add Record
     * @param array $DeleteValues
     * @param string $Table
     * @return    bool,string
     */
    public function add_record($AddValues, $Table)
    {
        if (!in_array($Table, $this->CD_Tables)) {
            echo "ERROR: Unknown Table ({$Table}) ";
            return false;
        }
        if ($Table == 'ConnectedDataField') $AddValues['FieldId'] = macanta_uniqid($this->RecordPrefix[$Table]);
        if ($Table == 'ConnectedDataGroup') $AddValues['GroupId'] = macanta_uniqid($this->RecordPrefix[$Table]);
        if ($Table == 'ConnectedDataItem') $AddValues['ItemId'] = macanta_uniqid($this->RecordPrefix[$Table]);
        if ($Table == 'ConnectedDataRelationship') $AddValues['RelationshipId'] = macanta_uniqid($this->RecordPrefix[$Table]);
        $TableFields = $this->db->list_fields($Table);
        foreach ($AddValues as $FieldName => $FieldValue) {
            if (!in_array($FieldName, $TableFields)) {
                echo "ERROR: Connected Data Unknown {$FieldName} Field For Table {$Table}";
                return false;
            }
        }
        $AddResult = $this->db->insert($Table, $AddValues);
        return $AddResult;
    }

    /**
     * Group Get
     * @param string $GetValue
     * @param string $Group 'Label' or 'GroupId'
     * @return    array
     */
    public function get_group($Group = "")
    {


        $Query = " SELECT ConnectedDataGroup.*,
                          ConnectedDataRelationship.Label AS RelationshipName,
                          ConnectedDataRelationship.RelationshipId,
                          ConnectedDataRelationship.Description AS RelationshipDescription
                   FROM   ConnectedDataGroup 
                   JOIN   ConnectedDataGroupRelationship ON ConnectedDataGroupRelationship.GroupId = ConnectedDataGroup.GroupId 
                   LEFT JOIN   ConnectedDataRelationship ON ConnectedDataRelationship.RelationshipId = ConnectedDataGroupRelationship.RelationshipId
                   

        ";
        if (!empty($Group)) {
            $Query .= " 
            WHERE (ConnectedDataGroup.Label = '{$Group}' OR ConnectedDataGroup.GroupId = '{$Group}')  
            ORDER BY ConnectedDataGroup.Order,ConnectedDataGroup.Label
            ";
        } else {
            $Query .= " 
            ORDER BY ConnectedDataGroup.Order,ConnectedDataGroup.Label
            ";
        }
        $query = $this->db->query($Query);
        //return $query->result_object();
        return $this->get_group_structurize($query->result_object());
    }

    public function get_group_structurize($result_objects)
    {
        $structurized = [];
        foreach ($result_objects as $result_object) {
            $result_objectTemp = clone $result_object;
            unset($result_objectTemp->GroupId);
            unset($result_objectTemp->RelationshipName);
            unset($result_objectTemp->RelationshipId);

            if (!isset($structurized[$result_object->GroupId]))
                $structurized[$result_object->GroupId] = $result_objectTemp;

            if (!isset($structurized[$result_object->GroupId]->Fields))
                $structurized[$result_object->GroupId]->Fields = $this->get_fields_by_group($result_object->Label);

            if (isset($result_object->RelationshipId))
                $structurized[$result_object->GroupId]->Relationships[$result_object->RelationshipId] = [
                    'RelationshipName' => $result_object->RelationshipName,
                    'RelationshipDescription' => $result_object->RelationshipDescription
                ];
        }
        return $structurized;
    }

    /**
     * Get Fields by Group
     * @param string $Group 'GroupId'  or 'GroupName'
     * @return    array
     */
    public function get_fields_by_group($Group)
    {
        if (empty($GetValue)) return NULL;
        $Query = " SELECT ConnectedDataField.*
                   FROM   ConnectedDataField 
                   JOIN   ConnectedDataGroup ON ConnectedDataGroup.GroupId = ConnectedDataField.GroupId 
                   ";
        $Query .= " 
            WHERE (ConnectedDataGroup.Label = '" . $Group . "' OR ConnectedDataGroup.GroupId = '" . $Group . "') 
            ORDER BY ConnectedDataField.Order,ConnectedDataField.Label
        ";
        $query = $this->db->query($Query);
        return $this->get_fields_structurize($query->result_object());
    }

    public function get_fields_structurize($result_objects)
    {
        $structurized = [];
        $result_objectTemp = [];
        foreach ($result_objects as $result_object) {
            $result_objectTemp = clone $result_object;
            unset($result_objectTemp->GroupId);
            unset($result_objectTemp->FieldId);
            $structurized[$result_object->FieldId] = $result_objectTemp;
        }
        return $structurized;
    }

    /**
     * Get Relationship
     * @param string $GetValue
     * @param string $GetBy 'Label' or 'RelationshipId'
     * @return    array
     */
    public function get_relationship($GetValue = '', $GetBy = 'Label')
    {
        if (!empty($GetValue)) $this->db->where($GetBy, $GetValue);
        $query = $this->db->get('ConnectedDataRelationship');
        return $query->result_object();
    }

    /**
     * Get Items By Id
     * @param string $ItemId
     * @return    array
     */
    public function get_item_by_id($ItemId)
    {
        $Query = $this->ItemQuery;
        $Query .= "WHERE  ConnectedDataItem.ItemId = '" . $ItemId . "' ";
        $query = $this->db->query($Query);
        return $this->get_item_structurize($query->result_object());
    }

    public function get_item_structurize($result_objects)
    {
        $structurized = [];
        $Contacts = [];
        foreach ($result_objects as $result_object) {
            $structurized[$result_object->ItemId]['Fields'][$result_object->FieldId] = ['FieldLabel' => $result_object->FieldLabel, 'FieldValue' => $result_object->FieldValue, 'FieldType' => $result_object->FieldType];
            $structurized[$result_object->ItemId]['ConnectedContacts'][$result_object->ContactId] = [
                'FirstName' => $result_object->FirstName,
                'LastName' => $result_object->LastName,
                'Email' => $result_object->Email
            ];
            $structurized[$result_object->ItemId]['ConnectedContacts'][$result_object->ContactId]['Relationships'][$result_object->RelationshipId] = $result_object->RelationshipName;
            if (!empty($result_object->InitialSequence)) {
                $structurized[$result_object->ItemId]['ConnectedContacts'][$result_object->ContactId]['Sequences'][$result_object->InitialSequence] = [
                    'NextSequence' => $result_object->NextSequence,
                    'RestartAfter' => $result_object->RestartAfter,
                    'MoveAfter' => $result_object->MoveAfter,
                    'MoveWhen' => $result_object->MoveWhen,
                    'Parsed' => $result_object->Parsed,
                    'Status' => $result_object->Status,
                    'Meta' => $result_object->Meta,
                ];
            }
            if (!isset($Contacts[$result_object->ItemId])) {
                $Contacts[$result_object->ItemId] = $this->get_connected_contact($result_object->ItemId);
                foreach ($Contacts[$result_object->ItemId] as $ContactId => $Contactetails) {
                    if (!isset($structurized[$result_object->ItemId]['ConnectedContacts'][$ContactId]))
                        $structurized[$result_object->ItemId]['ConnectedContacts'][$ContactId] = $Contactetails;
                }
            }

        }
        return $structurized;
    }

    //todo: modify this to use new table structure
    public function get_connected_contact($ItemId)
    {
        $Query = "
        SELECT ConnectedDataContact.ContactId,
               ConnectedDataContact.RelationshipId,
               ConnectedDataRelationship.Label AS RelationshipName,
               InfusionsoftContact.FirstName,
               InfusionsoftContact.LastName,
               InfusionsoftContact.Email,
               ConnectedDataSequence.InitialSequence,
               ConnectedDataSequence.NextSequence,
               ConnectedDataSequence.RestartAfter,
               ConnectedDataSequence.MoveAfter,
               ConnectedDataSequence.MoveWhen,
               ConnectedDataSequence.Parsed,
               ConnectedDataSequence.Status,
               ConnectedDataSequence.Meta
        FROM   ConnectedDataContact
        JOIN   ConnectedDataRelationship ON ConnectedDataRelationship.RelationshipId = ConnectedDataContact.RelationshipId
        JOIN   InfusionsoftContact ON InfusionsoftContact.Id = ConnectedDataContact.ContactId
        LEFT JOIN   ConnectedDataSequence ON ConnectedDataSequence.ContactId = ConnectedDataContact.ContactId AND ConnectedDataSequence.ItemId = '{$ItemId}'
        WHERE ConnectedDataContact.ItemId = '{$ItemId}'
    ";
        $query = $this->db->query($Query);
        return $this->get_connected_contact_structurize($query->result_object());
    }

    public function get_connected_contact_structurize($result_objects)
    {
        $Contacts = [];
        foreach ($result_objects as $result_object) {
            $Contacts[$result_object->ContactId] = [
                'FirstName' => $result_object->FirstName,
                'LastName' => $result_object->LastName,
                'Email' => $result_object->Email
            ];
            $Contacts[$result_object->ContactId]['Relationships'][$result_object->RelationshipId] = $result_object->RelationshipName;
            if (!empty($result_object->InitialSequence)) {
                $Contacts[$result_object->ContactId]['Sequences'][$result_object->InitialSequence] = [
                    'NextSequence' => $result_object->NextSequence,
                    'RestartAfter' => $result_object->RestartAfter,
                    'MoveAfter' => $result_object->MoveAfter,
                    'MoveWhen' => $result_object->MoveWhen,
                    'Parsed' => $result_object->Parsed,
                    'Status' => $result_object->Status,
                    'Meta' => $result_object->Meta,
                ];
            }

        }
        return $Contacts;
    }

    /**
     * Get Items By Group
     * @param string $Group 'GroupId' or 'Label/Name'
     * @return    array
     */
    public function get_items_by_group($Group)
    {
        $Query = $this->ItemQuery;
        $Query .= "WHERE (ConnectedDataGroup.Label = '" . $Group . "' OR ConnectedDataGroup.GroupId = '" . $Group . "') ";
        $query = $this->db->query($Query);
        return $this->get_item_structurize($query->result_object());
    }

    /**
     * Get Items By Field Value
     * @param array $FieldValues
     *          $FieldValues = [
     * 'Premium:' => 'Select 1 [and/or] Select 2',
     * ];
     * @param string $Group (required) GroupName or GroupId
     * @return    array
     */
    public function get_items_by_field_value($FieldValues, $Group, $ContactId = '', $Limit = 0, $Offset = 0)
    {
        $Query = $this->ItemQuery;
        $Query .= "WHERE (ConnectedDataGroup.Label = '" . $Group . "' OR ConnectedDataGroup.GroupId = '" . $Group . "') ";
        $ConditionArr = [];
        if (sizeof($FieldValues) > 0) {
            foreach ($FieldValues as $FieldName => $Values) {
                $temp = [];
                $ConditionArr[] = " ConnectedDataField.Label = '{$FieldName}' ";
                $ValueArr = strpos($Values, ' and ') === false ? explode(' or ', $Values) : explode(' and ', $Values);
                foreach ($ValueArr as $Value) {
                    // This works on checkbox multiple selected `or` or `and`
                    $temp[] = " FIND_IN_SET('{$Value}', replace(ConnectedDataItemFieldValue.FieldValue, '|', ',')) > 0 ";
                }
                $ConditionArr[] = strpos($Values, ' and ') === false ? " (" . implode(' OR ', $temp) . ") " : " (" . implode(' AND ', $temp) . ") ";
            }
            $Conditions = implode(' AND ', $ConditionArr);
            if (trim($Conditions) != '')
                $Query .= ' AND ' . $Conditions;
        }

        if ($Limit != 0) $Query .= ' LIMIT ' . $Limit . ' ';
        if ($Offset != 0) $Query .= ' OFFSET ' . $Offset . ' ';
        $query = $this->db->query($Query);
        return $this->get_item_structurize($query->result_object());
    }
}