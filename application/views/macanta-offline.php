<?php
//This is the main macanta template wrapper.
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <title><?php echo $this->config->item('sitename'); ?></title>
    <meta content="<?php echo $this->config->item('sitename'); ?>" property="og:title">
    <meta content="Infusionsoft&reg; | Simplified" name="description">
    <meta content="Macanta" property="og:site_name">
    <meta content="product" property="og:type">
    <meta content="<?php echo $this->config->item('base_url');?>" property="og:url">
    <meta content="<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png" property="og:image">
    <meta content="<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png" name="twitter:image">
    <link rel='mask-icon' href='<?php echo $this->config->item('base_url');?>assets/img/macantaicon.png' color='#7BBE4A'>
    <link rel="shortcut icon" href="<?php echo $this->config->item('base_url');?>assets/ico/favicon.png?v=2">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url');?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url');?>assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url');?>assets/css/responsive.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url');?>assets/jquery-ui-1.11.4/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url');?>assets/css/macanta-offline.css" "="">
    <script type="application/javascript" >
        var ajax_url = "<?php echo base_url().'index.php/ajax'?>";
        var session_name = localStorage.getItem("session_name") || '';

    </script>

</head>
<body id="macanta-body" class="macanta-body">
<div class="container">
    <div class="row">
        <a class="col-md-3 col-lg-3 loginLogo">
            <img src="<?php echo $this->config->item('base_url');?>assets/img/macanta_logo.png" style=" " alt="">

        </a>
        <div class="MainPageTitle">
            <span>Macanta Offline Sync</span>
            <div class="btn-group">
<!--                <button type="button" class="btn btn-sync btn-default TableAll" onclick="macantaSync('TableAll')">Sync All</button>
-->                <!--<button type="button" class="btn btn-abort btn-default TableAll">Abort All</button>-->
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table table-hover sync-table">
                <thead>
                <tr>
                    <th>Infusionsoft Table</th>
                    <th>Sync Progress</th>
                    <th>Operation</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $AllTables = [];
                foreach ($tables as $ISTable => $MacantaTable){
                    $AllTables[]=$ISTable;
                    ?>
                    <tr class="tr-<?php echo $ISTable; ?>">
                        <td><?php echo $ISTable; ?> <span class="item-count"></span></td>
                        <td><div id="progressbar" class="hideThis"><div class="progress-label"></div></div>
                        </td>
                        <td  class="Operation">-</td>
                        <td  class="Status">Standby</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sync <?php echo $ISTable; ?>" onclick="macantaSync('<?php echo $ISTable; ?>')">Sync</button>
                                <!--<button type="button" class="btn btn-default btn-abort <?php /*echo $ISTable; */?>" disabled>Abort</button>-->
                            </div></td>
                    </tr>
               <?php }
                ?>

                </tbody>
            </table>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>assets/js/functions-offline.js"></script>
<script>
    var AjaxRequests=null;
    var AllTables = <?php echo json_encode($AllTables)?>;
    $(function() {
        monitorSync();
        var theInterval =  setInterval(function(){ monitorSync(); }, 1500);

    });
</script>
</body>
</html>