<?php
$access_level = json_decode($this->config->item('access_level'), true);
$tag = $access_level['staff'];
$MacantaUsers = infusionsoft_get_macanta_user();
?>
<div class="col-md-12 theNotePanel">
    <div class="row mainbox-top">
        <div class="col-md-12 tab-panel">
            <div class="panel panel-primary left-CustomTabs">
                <div class="panel-heading">

                    <h3 class="panel-title "><i class="fa fa-id-card"></i> Users Caller ID</h3>
                    <button type="button" class=" col-xs-1 btn btn-default  saveCallerIDs ShowVerifyPhoneWindow"><i class="fa fa-save"></i>
                         Add Caller ID
                    </button>
                </div>
                <div class="panel-body admin-panelBody MacantaUsersContainer ">
                    <div class="col-md-6 MacantaUsersListContainer">
                        <h4>Macanta Users List</h4>
                        <form id="MacantaUsersList" method="post" class="form-horizontal MacantaUsersList dynamic"
                              _lpchecked="1">
                            <ul class="itemContainer">

                                <div class="saved"></div>
                                <?php

                                foreach ($MacantaUsers as $MacantaUser) {
                                    $UsersCallerId[] = array()
                                    ?>
                                    <li class="form-group-item"
                                        data-guid="<?php echo $MacantaUser->Id ?>">
                                        <div class="UserItem">
                                            <span class="MacantaUserListTitle">
                                                <?php echo $MacantaUser->FirstName ?> <?php echo $MacantaUser->LastName ?>
                                                <br><small>Id: <?php echo $MacantaUser->Id ?> | <?php echo $MacantaUser->Email ?></small>
                                            </span>
                                        </div>

                                    </li>
                                    <?php
                                }
                                ?>

                            </ul>
                        </form>
                    </div>

                    <div class="col-md-6 CallSettingsContainer">
                        <h4>Available Caller ID's</h4>
                        <?php
                        $account_sid = $this->config->item('Twilio_Account_SID');
                        $auth_token = $this->config->item('Twilio_TOKEN');
                        try {
                            $client = new Twilio\Rest\Client($account_sid, $auth_token);
                            $CompanyCallerIds = $client->outgoingCallerIds->read();

                        } catch (Twilio\Exceptions\RestException $e) {
                            $CompanyCallerIds = [];
                            echo '<span style="    color: red; font-size: 14px; text-align: left; width: 100%; line-height: 16px !important; display: inherit;">Sorry, Twilio Communication System Down, Details:'.$e->getMessage().'</span>';

                        }
                        $CompanyCallerIdsTemp = [];
                        foreach ($CompanyCallerIds as $CallerId){
                            $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
                        }
                        ksort($CompanyCallerIdsTemp);
                        $CompanyCallerIds = [];
                        foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId){
                            $CompanyCallerIds[] = $CallerId;
                        }

                        $DefaultCallerId = $this->config->item('macanta_caller_id');
                        ?>
                        <div class="CallerIdItem">
                            <div class="form-group default">
                                <input class="AllowUserCallerId  "  type="checkbox" id="CallerIdDefault" name="CallerIdDefault"   autocomplete="off" disabled checked />
                                <div class="btn-group">
                                    <label for="CallerIdDefault" class="btn btn-default ">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        <span> </span>
                                    </label>
                                    <label for="CallerIdDefault" class="btn btn-default checkbox-label active ">
                                        <div class="MacantaCallerIdTitle">
                                            System Default
                                        </div>
                                        <div class="MacantaCallerIdNumber">
                                            <?php echo $DefaultCallerId ?  $DefaultCallerId:"No Default Caller ID"?>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <form id="MacantaCallerIdList" method="post" class="form-horizontal MacantaCallerIdList dynamic"
                              _lpchecked="1">
                            <input type="hidden" name="UserId"  class="UserId">
                            <ul class="itemContainer">
                                <?php
                                if($this->config->item('outbound_devices')){
                                    $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
                                }else{
                                    $SystemOuboundDevices = [];
                                }
                                foreach ($CompanyCallerIds as $CallerId) {
                                    if($DefaultCallerId == $CallerId->phoneNumber) continue;
                                    $friendly_name = $CallerId->friendlyName;
                                    $friendly_name = $CallerId->phoneNumber == $friendly_name ? "<em>No Label</em>":$friendly_name;
                                    $Id = $CallerId->sid;
                                    $data =  'data-callerid="'.$CallerId->phoneNumber.'"';
                                    $info = "hide";
                                    if(in_array($CallerId->phoneNumber, $SystemOuboundDevices)) $info = "";
                                    $isdevice = $info == "hide" ? 'no':'yes';
                                    $fwidth = $info == "hide" ? 'fwidth':'';
                                    ?>
                                    <li class="form-group-item" data-calleridtype="user-defined"  data-sid="<?php echo $CallerId->sid; ?>" data-guid="<?php echo $CallerId->phoneNumber; ?>">
                                        <div class="CallerIdItem">
                                            <div class="form-group success">
                                                <input class="AllowUserCallerId UserCallerIdToEnable" value="<?php echo $CallerId->phoneNumber; ?>" data-callerid="<?php echo $CallerId->phoneNumber; ?>" type="checkbox" name="CallerId" id="callerId-<?php echo $Id; ?>" autocomplete="off" disabled   />
                                                <div class="btn-group">
                                                    <label for="callerId-<?php echo $Id; ?>" class="btn btn-success toggleCallerId">
                                                        <span class="glyphicon glyphicon-ok"></span>
                                                        <span> </span>
                                                    </label>
                                                    <label class="btn btn-default checkbox-label active ">
                                                        <div class="MacantaCallerIdTitle <?php echo $Id; ?> <?php echo $fwidth; ?>" id="label<?php echo $Id; ?>" title="<?php echo $friendly_name ?>">
                                                        <?php echo $friendly_name ?>
                                                        </div>
                                                        <div class="MacantaCallerIdNumber  <?php echo $Id; ?> <?php echo $fwidth; ?>">
                                                            <div class="the-number">
                                                                <?php echo $CallerId->phoneNumber ?>
                                                            </div>
                                                            <div class="the-info">
                                                            <span class="info-as-device-<?php echo $Id; ?>">
                                                                    <a class="info-as-device <?php echo $info; ?>">Can be used as a call device</a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <i class="fa fa-pencil-square-o EditCallerIdLabelIcon CallerIDAction" aria-hidden="true"
                                                       data-sid="<?php echo $CallerId->sid ?>"
                                                       data-callerid="<?php echo $CallerId->phoneNumber ?>"
                                                       data-label="<?php echo $CallerId->friendlyName ?>"
                                                       data-isdevice="<?php echo $isdevice; ?>"></i>
                                                    <i class="fa fa-trash-o DeleteCallerIdIcon CallerIDAction" aria-hidden="true"
                                                       data-sid="<?php echo $CallerId->sid ?>"
                                                       data-phone="<?php echo $CallerId->phoneNumber ?>"
                                                    ></i>
                                                </div>
                                            </div>


                                        </div>

                                    </li>
                                    <?php
                                }
                                ?>

                            </ul>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
    </div>
</div>
<!--Modal For Caller Id Verification-->
<div id="CallerIdWindow" class="modal fade CallerIdWindow" tabindex="-1" role="dialog" aria-labelledby="CallerIdWindowLabel">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Caller Id Verification</h4>
            </div>
            <div class="modal-body">

                <div class="form-group validate-phone-div">
                    <label for="NumberToVerify"><span class="hidden-xs">Phone*</span></label>
                        <input type="tel" id="NumberToVerify" class="NumberToVerify" placeholder="e.g. +1 702 123 4567" required>
                        <input type="text" id="ExtToVerify" class="ExtToVerify" placeholder="Ext" style='width:100px'>
                    <br>
                    <label for="LabelToVerify"><span class="hidden-xs">Label*</span></label>
                        <input type="text"  class="LabelToVerify" placeholder="Company Office Phone" required>
                    <br>
                    <label for="LabelToCallDevice"><span class="hidden-xs">Can be used as an outbound call device</span></label>
                    <div class="material-switch pull-right">
                        <input id="LabelToCallDevice" name="LabelToCallDevice" type="checkbox"/>
                        <label for="LabelToCallDevice" class="label-success"></label>
                    </div>


                    <button class="btn btn-info" id="ButtonToVerify" class="ButtonToVerify" >
                        Verify Caller ID
                    </button>
                    <div class='phone-status'></div>
                    <div class="twilio-notice">
                        <p>Please enter the phone number, <strong>without a leading '0'</strong>,
                            you would like to use to call people from within macanta. When you click
                            'Verify Caller ID', we'll call the number you provide and ask you to
                            enter a 6-digit verification code. <a target='_blank'
                                                                  href='https://conquerthechaos.groovehq.com/knowledge_base/topics/verifying-caller-ids-behind-an-ivr-or-extension'>How
                                to verify caller ids behind an IVR or extension.</a></p>
                        <a href='https://www.twilio.com/' target='_blank'><img
                                    src='<?php echo base_url();?>assets/img/logos_powerdby_twilio.png'
                                    style="display:block;margin:20px auto 0 auto;width:135px"></a>
                    </div>
                </div>
                <div class="form-group phone-progress">
                    Initializing, Please wait..
                </div>

                <script>
                    var PhoneInput = $('#NumberToVerify');
                    PhoneInput.intlTelInput({
                        utilsScript: "<?php echo base_url();?>assets/format-phone/build/js/utils.js",
                        nationalMode: false,
                        preferredCountries: ["gb", "us"]
                    });

                    PhoneInput.keyup(function(){
                        var inputVal = $(this).val();
                        if(inputVal.charAt(0) !== "+"){
                            $(this).val("+" + inputVal);
                        }
                    })


                </script>







            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('text_close');?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Modal For Caller Id Edit-->
<div id="EditCallerIdWindow" class="modal fade EditCallerIdWindow" tabindex="-1" role="dialog" aria-labelledby="EditCallerIdWindowLabel">
    <div class="modal-dialog">
        <div class="modal-content EditCallerIdWindowContainer">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Caller Id</h4>
            </div>
            <div class="modal-body EditCallerIdWindowBody">

                <div class="form-group validate-phone-div">

                    <label for="LabelToVerify"><span class="hidden-xs">Label*</span></label>
                    <input data-sid="" data-callerid="" type="text" class="LabelToVerify"  required>
                    <br>
                    <label for="LabelToCallDeviceEdit"><span class="hidden-xs">Can be used as an outbound call device</span></label>
                    <div class="material-switch pull-right">
                        <input id="LabelToCallDeviceEdit" name="LabelToCallDevice" type="checkbox"/>
                        <label for="LabelToCallDeviceEdit" class="label-success"></label>
                    </div>


                    <button class="col-md-6 btn btn-info" id="SaveCallerIdLabel" class="SaveCallerIdLabel" >
                        Save Caller ID
                    </button>
                    <button class="col-md-6 btn btn-info" id="MakeDefaultCallerId" class="MakeDefaultCallerId" >
                        Make Default Caller ID
                    </button>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('text_close');?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
    var firstListed = $("#MacantaUsersList ul.itemContainer li");
    firstListed.first().trigger('click');
</script>