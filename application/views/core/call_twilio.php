<?php //print_r($Contact); ?>
<div class="col-lg-12 twillio-phone">
    <div class="btn-group  col-sm-8 " role="group" aria-label="...">
        <span id="callerid-select">Your Caller ID:</span>
        <span id="twilio_log">Twilio <?php echo $this->lang->line('text_status');?></span>
    </div>
    <div class="btn-group  col-sm-4 call" role="group" aria-label="...">
        <button type="button" class="btn btn-default phoneCall" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="fa fa-phone"></span> <?php echo $this->lang->line('text_start_phone_call');?>
        </button>
        <a type="button" class="btn btn-danger EndphoneCall">
            <span class="fa fa-phone"></span> <?php echo $this->lang->line('text_end_phone_call');?>
        </a>

        <ul class="dropdown-menu phoneCallItems col-sm-12" aria-labelledby="dropdownMenu1">
            <form lpformnum="1">
                <li>
                    <div class="input-group">
                        <span class="input-group-addon JustLabel" > <?php echo $this->lang->line('text_call_phone');?> 1: </span>
                        <input type="text" class="form-control Phone1 basic-url"
                               aria-describedby="basic-addon3"  value="<?php echo$Contact->Phone1; ?>"  readonly>
                        <span class="input-group-addon phoneLabel CallPhone" id="basic-addon3" data-phone="Phone1">
                            <a id="CallMobile" data-href="#Call" data-phonefield="<?php echo $Contact->Phone1Twilio; ?>" class="data-toggle-tab Phone1Twilio"><?php echo $this->lang->line('text_call');?></a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="input-group">
                        <span class="input-group-addon JustLabel"> <?php echo $this->lang->line('text_call_phone');?> 2: </span>
                        <input type="text" class="form-control Phone2 basic-url"
                               aria-describedby="basic-addon3" value="<?php echo$Contact->Phone2; ?>" readonly>
                        <span class="input-group-addon phoneLabel CallPhone" id="basic-addon3" data-Phone="Phone2">
                             <a  id="CallLandline" data-href="#Call" data-phonefield="<?php echo$Contact->Phone2Twilio; ?>" class="data-toggle-tab Phone2Twilio"><?php echo $this->lang->line('text_call');?></a>
                        </span>
                    </div>
                </li>
                <li><h4 class="orDial"><?php echo $this->lang->line('text_or');?> <?php echo $this->lang->line('text_dial_number');?>..</h4>

                    <div class="form-group col-sm-12 DialContainer" >
                        <div class="intl-tel-input">

                            <input type="text" id="DialedPhone" class="form-control DialedPhone"
                                   placeholder=" " data-phone="" autocomplete="off"></div>
                        <span id="valid-msg" class="hide">✓ <?php echo $this->lang->line('text_valid');?></span>
                        <span id="error-msg" class="hide"><?php echo $this->lang->line('text_invalid_number');?></span>
                    </div>

                    <button type="button" id="DialedBtn" class="btn btn-default col-sm-12 callbutton CallPhone data-toggle-tab DialContainer" data-href="#Call" data-phonefield="DialedPhone" disabled=""><?php echo $this->lang->line('text_call');?>
                    </button>

                </li>
            </form>
        </ul>
    </div>
</div>
<!--Modal For Call Recorded-->
<div id="CallRecording" class="modal fade CallRecording" tabindex="-1" role="dialog" aria-labelledby="CallRecordingLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Call Recording</h4>
            </div>
            <div class="modal-body">
                <audio controls id="callrecord">
                    <source src="" type="audio/ogg" class="crecording_ogg">
                    <source src="" type="audio/mpeg" class="crecording_mp3">
                    Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->