<?php
$MacantaQueries = macanta_get_cd_query();
?>
<div class="col-md-12 theNotePanel">
    <div class="row mainbox-top">
        <!--<div class="col-md-6">
            <div class="panel panel-primary left-Permissions">
                <div class="panel-heading">

                    <h3 class="panel-title "><i class="fa fa-key"></i> Access</h3>

                </div>
                <div class="panel-body admin-panelBody admin-panelBody-access">

                    <script>
                        getTagCatSelection("left-Permissions", "admin-panelBody-access");
                    </script>
                </div>
            </div>

        </div>-->
        <?php
        $password = $this->config->item('macanta_api_key');
        $NoteEditingPermissions = $this->config->item('note_editing_permission');
        ?>
        <div class="col-md-6 tab-panel left-tab-panel">
            <div class="panel panel-primary left-Permissions">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-lock" aria-hidden="true"></i> Security</h3>
                </div>
                <div class="panel-body admin-panelBody admin-panelBody-security">
                    <div class="form-group">
                        <label class="control-label securityLabel" for="inputSuccess1">HTTP Post Key</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"
                                                                                 aria-hidden="true"></i></span>
                            <input type="text" class="form-control PostKey" aria-describedby="basic-addon1" readonly
                                   value="<?php echo $password; ?>">
                            <span class="btn btn-primary input-group-addon copyPostKey">copy</span>
                        </div>
                        <span class="help-block" id="helpBlock2">
                            Use this key for sending HTTP Post from Infusionsoft.
                        </span>
                    </div>
                    <div class="form-group NoteEditingPermissionsContainer">
                        <label class="control-label securityLabel" for="NoteEditingPermissions">Note Editing
                            Permission</label>
                        <div class="input-group NoteEditingPermissionsBody">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"
                                                                                 aria-hidden="true"></i></span>
                            <input id="NoteEditingPermissions" class="form-control NoteEditingPermissions"
                                   aria-describedby="basic-addon1" value="<?php echo $NoteEditingPermissions; ?>">
                            <span class="btn btn-primary input-group-addon"
                                  onclick="saveNoteEditingPermissions();">Save</span>
                        </div>
                        <span class="help-block" id="helpBlock2">
                            Enter the tag id that will give users permission to edit contact notes.
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 tab-panel  right-tab-panel">
            <div class="panel panel-primary right-Permissions">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-search-plus"></i> Saved Searches</h3>
                    <button type="button" class=" col-xs-1 btn btn-default  refreshSavedSearch"><i
                                class="fa fa-refresh"></i>
                        Refresh
                    </button>
                </div>
                <div class="panel-body admin-panelBody admin-panelBody-savedsearch">
                    <script>
                        getSavedSearchTag("right-Permissions", "admin-panelBody-savedsearch");
                    </script>
                </div>
            </div>
        </div>
        <div class="col-md-12 tab-panel macanta-queries">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-search-plus"></i> Macanta Queries</h3>
                    <button type="button" class=" col-xs-1 btn btn-default addMacantaQuery" ><i class="fa fa-plus-circle"></i> Add Query
                    </button>
                </div>
                <div class="panel-body admin-panelBody admin-panelBody-macanta-queries">

                    <?php
                    foreach ($MacantaQueries as $MacantaQuery){
                        ?>
                        <div class="col-lg-12 col-md-12 macanta-query-item query-item-container-<?php echo $MacantaQuery->queryId; ?>">
                            <div class="col-lg-12 no-pad-left no-pad-right query-item">
                                <div class="col-lg-9 col-md-9  col-sm-9 no-pad-left no-pad-right ">
                                    <h3 class="col-lg-12 col-md-12 no-pad-left no-pad-right "><?php echo $MacantaQuery->queryName; ?> <small>( <?php echo $MacantaQuery->queryStatus ? "active":"inactive"; ?> )</small></h3>
                                    <h5 class="col-lg-12 col-md-12 no-pad-left no-pad-right "><?php echo $MacantaQuery->queryDescription; ?></h5>
                                </div>
                                <div class="col-lg-3 col-md-3  col-sm-3 no-pad-left no-pad-right ">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button data-queryid="<?php echo $MacantaQuery->queryId; ?>" type="button" class="btn btn-default editMacantaQuery">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>
                                        <button data-queryid="<?php echo $MacantaQuery->queryId; ?>" type="button" class="btn btn-default deleteMacantaQuery">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" disabled><i
                                                    class="glyphicon glyphicon-ban-circle"></i></button>
                                    </div>
                                    <span class="tag-info">Tag Id: <?php echo $MacantaQuery->tagId; ?></span>
                                </div>
                                <div class="col-lg-12 col-md-12  col-sm-12 no-pad-left no-pad-right macanta-query-item-short-info">
                                    <div class="col-lg-4 col-md-4  col-sm-4  no-pad-left">
                                        <strong>Connected Data Type: </strong><?php echo $MacantaQuery->queryConnectedDataType; ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4  col-sm-4">
                                        <strong>Contact Relationship: </strong>
                                        <?php
                                        $ContactRelationshipArr = [];
                                        foreach ($MacantaQuery->queryContact as $queryContact){
                                            $ContactRelationshipArr[] = $queryContact->queryContactRelationship;
                                        }
                                        echo implode(', ',$ContactRelationshipArr);
                                        ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4  col-sm-4">
                                        <strong>User Relationship: </strong>
                                        <?php
                                        $UserRelationshipArr = [];
                                        foreach ($MacantaQuery->queryUser as $queryUser){
                                            $UserRelationshipArr[] = $queryUser->queryUserRelationship;
                                        }
                                        echo implode(', ',$UserRelationshipArr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </div>


    </div>
    <div class="row">
    </div>
</div>
<?php
$MacantaQueryDeleteDialog = "
<div id=\"delete-query-dialog-confirm\" title=\"Delete Query.\">
    <p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>Are you sure you want to delete this Query?</p>
</div>
";
?>
<script type="text/javascript">
    var MacantaQueryDeleteDialog = <?php echo json_encode($MacantaQueryDeleteDialog); ?>;

    jQuery(document).ready(function($) {
        $('.macanta-query-fieldset #multiselect').multiselect_({
            search: {
                left: '<input type="text" name="q" class="form-control" placeholder="Search..." autocomplete="off" />',
                right: '<input type="text" name="q" class="form-control" placeholder="Search..."  autocomplete="off" />'
            },
            keepRenderingSort: true
        });
        MacantaCDconditionsInit($(".modal.macanta-query"),true);
        var queryForm = $(".modal form.queryForm");
        queryForm.on('submit',function(event){
            event.preventDefault();
            var FormValues = $(this).serializeArray();
            var ParsedValues = {};
            var queryCDFieldIndex = 0;
            var queryContactIndex = 0;
            var queryUserIndex = 0;
            var toIndex = 0;
            ParsedValues['queryCDField'] = [];
            ParsedValues['queryContact'] = [];
            ParsedValues['queryUser'] = [];
            /*ParsedValues['queryColumns'] = [];*/
            ParsedValues['to'] = [];
            $.each(FormValues,function (FieldIndex,FieldDetails) {
                switch (FieldDetails['name']) {
                    case 'queryId':
                        ParsedValues['queryId'] = FieldDetails['value'];
                        break;
                    case 'queryName':
                        ParsedValues['queryName'] = FieldDetails['value'];
                        break;
                    case 'queryStatus':
                        ParsedValues['queryStatus'] = FieldDetails['value'];
                        break;
                    case 'queryDescription':
                        ParsedValues['queryDescription'] = FieldDetails['value'];
                        break;
                    case 'queryConnectedDataType':
                        ParsedValues['queryConnectedDataType'] = FieldDetails['value'];
                        break;
                    case 'queryCDFieldLogic[]':
                        ParsedValues['queryCDField'][queryCDFieldIndex]= {'queryCDFieldLogic':FieldDetails['value']};
                        break;
                    case 'queryCDFieldName[]':
                        ParsedValues['queryCDField'][queryCDFieldIndex]['queryCDFieldName'] = FieldDetails['value'];
                        break;
                    case 'queryCDFieldOperator[]':
                        ParsedValues['queryCDField'][queryCDFieldIndex]['queryCDFieldOperator'] = FieldDetails['value'];
                        break;
                    case 'queryCDFieldValues[]':
                        if(typeof ParsedValues['queryCDField'][queryCDFieldIndex]['queryCDFieldValues'] === 'undefined'){
                            ParsedValues['queryCDField'][queryCDFieldIndex]['queryCDFieldValues'] = [];
                        }
                        ParsedValues['queryCDField'][queryCDFieldIndex]['queryCDFieldValues'].push( FieldDetails['value']);
                        break;
                    case 'queryCDFieldValue[]':
                        ParsedValues['queryCDField'][queryCDFieldIndex]['queryCDFieldValue'] = FieldDetails['value'];
                        queryCDFieldIndex++;
                        break;
                    case 'queryContactRelationshipFieldLogic[]':
                        ParsedValues['queryContact'][queryContactIndex]={'queryContactRelationshipFieldLogic': FieldDetails['value']};
                        break;
                    case 'queryContactRelationship[]':
                        ParsedValues['queryContact'][queryContactIndex]['queryContactRelationship'] = FieldDetails['value'];
                        break;
                    case 'queryContactRelationshipCondition[]':
                        ParsedValues['queryContact'][queryContactIndex]['queryContactRelationshipCondition'] = FieldDetails['value'];
                        break;
                    case 'queryContactRelationshipTagsValue[]':
                        ParsedValues['queryContact'][queryContactIndex]['queryContactRelationshipTagsValue'] = FieldDetails['value'];
                        break;
                    case 'queryContactRelationshipOperator[]':
                        ParsedValues['queryContact'][queryContactIndex]['queryContactRelationshipOperator'] = FieldDetails['value'];
                        break;
                    case 'queryContactRelationshipConditionValue[]':
                        ParsedValues['queryContact'][queryContactIndex]['queryContactRelationshipConditionValue'] = FieldDetails['value'];
                        queryContactIndex++;
                        break;
                    case 'queryUserRelationshipFieldLogic[]':
                        ParsedValues['queryUser'][queryUserIndex]= {'queryUserRelationshipFieldLogic': FieldDetails['value']};
                        break;
                    case 'queryUserRelationship[]':
                        ParsedValues['queryUser'][queryUserIndex]['queryUserRelationship'] = FieldDetails['value'];
                        break;
                    case 'queryUserOperator[]':
                        ParsedValues['queryUser'][queryUserIndex]['queryUserOperator'] = FieldDetails['value'];
                        break;
                    case 'queryUserId[]':
                        ParsedValues['queryUser'][queryUserIndex]['queryUserId'] = FieldDetails['value'];
                        queryUserIndex++;
                        break;
                    case 'to[]':
                        ParsedValues['to'][toIndex] = FieldDetails['value'];
                        toIndex++;
                    /*case 'queryColumns[]':
                        ParsedValues['queryColumns'][toIndex] = FieldDetails['value'];
                        toIndex++;*/

                }
            });
            console.log(ParsedValues);
            saveMacantaQuery(ParsedValues);
        });
    });
</script>