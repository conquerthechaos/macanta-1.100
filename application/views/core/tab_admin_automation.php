<div class="col-md-12 theNotePanel">
    <div class="row mainbox-top">
        <div class="tab-panel">
            <div class="panel panel-primary left-ConnectedInfos">
                <div class="panel-heading">

                    <h3 class="panel-title "><i class="fa fa-cogs"></i> CD Automation System</h3>
                </div>
                <div class="panel-body admin-panelBody CDAutomationContainer ">
                    <div class="col-md-12 col-lg-12 CDAutomationListContainer no-pad-left">
                        <ul class="nav nav-tabs itemContainer CDAutomationList no-pad-left">
                            <li class="form-group-item ui-sortable-handle active">
                                <div class="bullet-item">
                                    <a href="#AutomationGroups" data-toggle="tab" aria-expanded="false">
                                        <h3 class="CDAutomationListTitle" data-content="AutomationGroups">
                                            <i class="fa fa-cubes"></i>
                                            Automation Groups
                                        </h3>
                                    </a>

                                </div>
                            </li>
                            <li class="form-group-item ui-sortable-handle">
                                <div class="bullet-item">
                                    <a href="#TriggerConditions" data-toggle="tab" aria-expanded="false">
                                        <h3 class="CDAutomationListTitle" data-content="TriggerConditions">
                                            <i class="fa fa-microchip"></i>
                                            Triggers Condition
                                        </h3>
                                    </a>

                                </div>
                            </li>
                            <li class="form-group-item ui-sortable-handle">
                                <div class="bullet-item">
                                    <a href="#TriggerActions" data-toggle="tab" aria-expanded="false">
                                        <h3 class="CDAutomationListTitle" data-content="TriggerActions">
                                            <i class="fa fa-cog"></i>
                                            Trigger Actions
                                        </h3>
                                    </a>

                                </div>
                            </li>
                            <li class="form-group-item ui-sortable-handle">
                                <div class="bullet-item">
                                    <a href="#ActionWebhooks" data-toggle="tab" aria-expanded="false">
                                        <h3 class="CDAutomationListTitle" data-content="ActionWebhooks">
                                            <i class="fa fa-external-link-square"></i>
                                            Action Webhooks
                                        </h3>
                                    </a>

                                </div>
                            </li>
                        </ul>
                        <div class="tab-body col-md-12 col-lg-12 CDAutomationSettingsContainer">
                            <div class="tab-content">
                                <div id="AutomationGroups"
                                     class="tab-pane active col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right CDAutomationContent ">

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right ">
                                        <div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
                                            <h4>Automation Assembly</h4>
                                            <p>Simple!, Create your Automation by choosing your Trigger Conditions
                                                and Trigger Action you built.
                                                The System will take care and run it in the background.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="panel-body admin-panelBody DynamicTabContainer "
                                         data-key="AutomationGroups"
                                         data-label="Automation Group"
                                         data-template="AutomationGroupsContentTemplate"
                                         data-dbprefix="ag"
                                    >



                                        <div class="col-md-4 ListContainer">
                                            <input type="text" class="ItemQuickFilter" placeholder="Type to filter">
                                            <form method="post" class="form-horizontal List dynamic"
                                                  _lpchecked="1">
                                                <ul class="itemContainer">

                                                    <div id="saved" class="saved"></div>

                                                    <div id="dummy"></div>
                                                    <li class="form-group-item remove-button">
                                                        <div class="bullet-item">
                                                            <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                                            <button type="button"
                                                                    class=" col-xs-1 btn btn-default addTabButton ">Add
                                                                Automation Group
                                                            </button>
                                                        </div>

                                                    </li>

                                                </ul>
                                                <p class="note">Drag &amp; Drop the tabs above to rearrange them.</p>
                                            </form>
                                            <hr>


                                        </div>

                                        <div class="col-md-8 SettingsContainer">

                                            <div class="ContentHeader"></div>
                                            <div class="SettingsContainerPlaceholder"><i
                                                        class="glyphicon glyphicon-edit"></i> Please
                                                create or select Automation Groups
                                            </div>


                                        </div>
                                        <div class="col-md-12 footnote">
                                            <!-- <strong>Available Shortcodes: </strong><br>
                                             1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                                             2. More Shortcodes coming soon!..-->
                                        </div>
                                    </div>


                                </div>
                                <div id="TriggerConditions"
                                     class="tab-pane col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right CDAutomationContent ">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right ">
                                        <div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
                                            <h4>Create Connected Data Trigger Conditions</h4>
                                            <p>The System will make sure to match these condition to run the Trgger Actions .
                                            </p>
                                        </div>
                                    </div>
                                    <div class="panel-body admin-panelBody DynamicTabContainer "
                                         data-key="TriggerConditions"
                                         data-label="Trigger Condition"
                                         data-template="TriggerConditionsContentTemplate"
                                         data-dbprefix="tc">
                                        <div class="col-md-4 ListContainer">
                                            <input type="text" class="ItemQuickFilter" placeholder="Type to filter">
                                            <form method="post" class="form-horizontal List dynamic"
                                                  _lpchecked="1">
                                                <ul class="itemContainer">

                                                    <div id="saved" class="saved"></div>

                                                    <div id="dummy"></div>
                                                    <li class="form-group-item remove-button">
                                                        <div class="bullet-item">
                                                            <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                                            <button type="button"
                                                                    class=" col-xs-1 btn btn-default addTabButton ">Add
                                                                Triggers
                                                            </button>
                                                        </div>

                                                    </li>

                                                </ul>
                                                <p class="note">Drag &amp; Drop the tabs above to rearrange them.</p>
                                            </form>
                                            <hr>


                                        </div>

                                        <div class="col-md-8 SettingsContainer">



                                            <div class="ContentHeader"></div>
                                            <div class="SettingsContainerPlaceholder"><i
                                                        class="glyphicon glyphicon-edit"></i> Please
                                                create or select Trigger Condition
                                            </div>


                                        </div>
                                        <div class="col-md-12 footnote">
                                            <!-- <strong>Available Shortcodes: </strong><br>
                                             1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                                             2. More Shortcodes coming soon!..-->
                                        </div>
                                    </div>


                                </div>
                                <div id="TriggerActions"
                                     class="tab-pane col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right CDAutomationContent ">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right ">
                                        <div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
                                            <h4>Setup the Actions!</h4>
                                            <p>These Actions will be triggered and fire up once the system detected a connected data that meets the corresponding conditions.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="panel-body admin-panelBody DynamicTabContainer "
                                         data-key="TriggerActions"
                                         data-label="Trigger Action"
                                         data-template="TriggerActionsContentTemplate"
                                         data-dbprefix="ta">
                                        <div class="col-md-4 ListContainer">
                                            <input type="text" class="ItemQuickFilter" placeholder="Type to filter">
                                            <form method="post" class="form-horizontal List dynamic"
                                                  _lpchecked="1">
                                                <ul class="itemContainer">

                                                    <div id="saved" class="saved"></div>

                                                    <div id="dummy"></div>
                                                    <li class="form-group-item remove-button">
                                                        <div class="bullet-item">
                                                            <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                                            <button type="button"
                                                                    class=" col-xs-1 btn btn-default addTabButton ">Add
                                                                Trigger Actions
                                                            </button>
                                                        </div>

                                                    </li>

                                                </ul>
                                                <p class="note">Drag &amp; Drop the tabs above to rearrange them.</p>
                                            </form>
                                            <hr>


                                        </div>

                                        <div class="col-md-8 SettingsContainer">

                                            <div class="ContentHeader"></div>
                                            <div class="SettingsContainerPlaceholder"><i
                                                        class="glyphicon glyphicon-edit"></i> Please
                                                create or select Trigger Action
                                            </div>


                                        </div>
                                        <div class="col-md-12 footnote">
                                            <!-- <strong>Available Shortcodes: </strong><br>
                                             1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                                             2. More Shortcodes coming soon!..-->
                                        </div>
                                    </div>


                                </div>
                                <div id="ActionWebhooks"
                                     class="tab-pane col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right CDAutomationContent ">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right ">
                                        <div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
                                            <h4>Webhook on the fly!</h4>
                                            <p>Setup your webhook to fire up for every Trigger Actions.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="panel-body admin-panelBody DynamicTabContainer "
                                         data-key="ActionWebhooks"
                                         data-label="Action Webhook"
                                         data-template="ActionWebhooksContentTemplate"
                                         data-dbprefix="aw">
                                        <div class="col-md-4 ListContainer">
                                            <input type="text" class="ItemQuickFilter" placeholder="Type to filter">
                                            <form method="post" class="form-horizontal List dynamic"
                                                  _lpchecked="1">
                                                <ul class="itemContainer">

                                                    <div id="saved" class="saved"></div>

                                                    <div id="dummy"></div>
                                                    <li class="form-group-item remove-button">
                                                        <div class="bullet-item">
                                                            <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                                            <button type="button"
                                                                    class=" col-xs-1 btn btn-default addTabButton ">Add
                                                                Action Webhooks
                                                            </button>
                                                        </div>

                                                    </li>

                                                </ul>
                                                <p class="note">Drag &amp; Drop the tabs above to rearrange them.</p>
                                            </form>
                                            <hr>


                                        </div>

                                        <div class="col-md-8 SettingsContainer">
                                            <div class="ContentHeader"></div>
                                            <div class="SettingsContainerPlaceholder"><i
                                                        class="glyphicon glyphicon-edit"></i> Please
                                                create or select Action Webhook
                                            </div>


                                        </div>
                                        <div class="col-md-12 footnote">
                                            <!-- <strong>Available Shortcodes: </strong><br>
                                             1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                                             2. More Shortcodes coming soon!..-->
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 footnote">
                        <!-- <strong>Available Shortcodes: </strong><br>
                         1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                         2. More Shortcodes coming soon!..-->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
    </div>
</div>

<?php
$CDAutomationDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Delete CD Autmation.\">
    <p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>This CD Autmation will be removed.<br> Are you sure?</p>
</div>
";
$ConditionDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Deleting Condition.\">
    <p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>This Condition Item will be deleted.<br> Are you sure?</p>
</div>
";
?>
<script>
    var previousValue = "";
    var CDAutomationDeleteDialog = <?php echo json_encode($CDAutomationDeleteDialog); ?>;
    var ConditionDeleteDialog = <?php echo json_encode($ConditionDeleteDialog); ?>;

    renderIfThenSelectInit(ConnectedDataAutomationSettings);
    renderConnectedDataHTTPPostList(ConnectedDataAutomationSettings);
    MacantaDynamicTabInit('DynamicTabContainer');
    FormSubmitionInit($(".DynamicTabContent  form.queryForm"));
    $.each(ConnectedDataAutomationSettings, function (QueryType, theSettings) {
        theContainer = $("#"+QueryType+" div.DynamicTabContainer");
        var TheTemplateName = theContainer.attr('data-template');
        var TheLabel = theContainer.attr('data-label');
        var TheKey = theContainer.attr('data-key');
        var DBPrefix = theContainer.attr('data-dbprefix');
        var TheButton = theContainer.find("button.addTabButton");
        $.each(theSettings, function (QueryId, QueryDetails) {
            addTabButton(TheButton,theContainer,TheTemplateName, TheLabel, TheKey, DBPrefix, QueryDetails);
        });
        QuickFilterInit(theContainer);
        $("ul.itemContainer li.form-group-item:first",theContainer).trigger('click');
    });

</script>
