<?php
if (sizeof($ZuoraContacts) > 0 && !isset($ZuoraContacts['Error'])) {
    $ZuoraContact = end($ZuoraContacts);
    ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tab-panel no-pad-right no-pad-left ZuoraContactsContainer">
        <h2 class="oppmainlabel">Contact Information</h2>
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-4  no-pad-left">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left  no-pad-right">
                <ul class="zuora-contact_info-list">
                    <li class="info-item ContactInfoHideMobile"><span class="label">Name: </span><span
                                class=" zuora-FirstName zuora-info"><?php echo $ZuoraContact->__get('FirstName') ? $ZuoraContact->__get('FirstName') : "N/A"; ?></span>
                        <span class=" zuora-LastName zuora-info"><?php echo $ZuoraContact->__get('LastName') ? $ZuoraContact->__get('LastName') : ""; ?></span>
                    </li>
                    <li class="info-item  ContactInfoHideMobile"><span class="label">Work Email: </span><span
                                class="zuora-Email zuora-info"><?php echo $ZuoraContact->__get('WorkEmail') ? $ZuoraContact->__get('WorkEmail') : "N/A"; ?></span>
                    </li>
                    <li class="info-item  ContactInfoHideMobile"><span class="label">Mobile Phone: </span><span
                                class="zuora-Phone1 zuora-info"><?php echo $ZuoraContact->__get('MobilePhone') ? $ZuoraContact->__get('MobilePhone') : "N/A"; ?></span>
                    </li>
                    <li class="info-item  ContactInfoHideMobile"><span class="label">Home Phone: </span><span
                                class="zuora-Phone2 zuora-info"><?php echo $ZuoraContact->__get('HomePhone') ? $ZuoraContact->__get('HomePhone') : "N/A"; ?></span>
                    </li>

                </ul>

            </div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-4  no-pad-left">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left  no-pad-right">
                <ul class="zuora-contact_info-list">
                    <li class="info-item ContactInfoHideMobile"><span class="label">Address: </span><span
                                class=" zuora-FirstName zuora-info"><?php echo $ZuoraContact->__get('Address1') ? $ZuoraContact->__get('Address1') : "N/A"; ?><?php echo $ZuoraContact->__get('Address2') ? $ZuoraContact->__get('Address2') : ""; ?></span>
                    </li>
                    <li class="info-item  ContactInfoHideMobile"><span class="label">City: </span><span
                                class="zuora-Email zuora-info"><?php echo $ZuoraContact->__get('City') ? $ZuoraContact->__get('City') : "N/A"; ?></span>
                    </li>
                </ul>

            </div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-4  no-pad-left">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left  no-pad-right">
                <ul class="zuora-contact_info-list">
                    <li class="info-item  ContactInfoHideMobile"><span class="label">Country: </span><span
                                class="zuora-Phone2 zuora-info"><?php echo $ZuoraContact->__get('Country') ? $ZuoraContact->__get('Country') : "N/A"; ?></span>
                    </li>
                    <li class="info-item  ContactInfoHideMobile"><span class="label">PostalCode: </span><span
                                class="zuora-Phone2 zuora-info"><?php echo $ZuoraContact->__get('PostalCode') ? $ZuoraContact->__get('PostalCode') : "N/A"; ?></span>
                    </li>
                </ul>

            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right ZuoraAccountsContainer">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right ZuoraAccountsBody">
            <h2 class="oppmainlabel">Accounts Information</h2>
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3  no-pad-left">
                <h3 class="">Account Names</h3>
                <ul class="zuoraAccountList">
                    <!--<li> - No Accounts -</li>-->
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-9 col-lg-9  no-pad-left no-pad-right ">
                <h3 class="">Account Details</h3>
                <div class="ZuoraAccountContainer">
                    <div class="ZuoraAccountDetails">
                        <ul class="zuora-contact_info-list header">
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right">
                                <span class="label bold">Contact Id: </span>
                                <span class=" zuora-ContactId zuora-info"></span>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label bold">Account Number: </span>
                                <span class="zuora-AccountNumber zuora-info"></span>
                            </li>

                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Account Balance: </span>
                                <span class="zuora-AccountBalance zuora-info"></span>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Credit Balance: </span>
                                <span class="zuora-CreditBalance zuora-info"></span>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Total Invoice Balance: </span>
                                <span class="zuora-TotalInvoiceBalance zuora-info"></span>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Last Invoiced: </span>
                                <span class="zuora-LastInvoiced zuora-info"></span>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Contracted MRR: </span>
                                <span class="zuora-ContractedMRR zuora-info"></span>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Today's MRR: </span>
                                <span class="zuora-TodaysMRR zuora-info"></span>
                            </li>

                            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12 info-item2 no-pad-left  no-pad-right ">
                                <span class="label">Notes: </span>
                                <!--<span class="zuora-AccountNotes zuora-info"></span>-->
                                <pre class="zuora-AccountNotes zuora-info"></pre>
                            </li>

                        </ul>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right no-pad-left ProductCharges table-responsive">
                            <h3>Product & Charges</h3>


                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right no-pad-left ZuoraPayments table-responsive">
                            <h3>Payments</h3>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="HTML-Template ProductChargesItem col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-right ">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right ProductCargesLabels">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad-left  no-pad-left"><span class="prod-name">Product: <strong>Product Here</strong></span></div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad-left  no-pad-left"><span class="rate-plan">Rate Plan: <strong>Product Here</strong></span></div>
        </div>
        <table class="table table-hover table-bordered tbl-product-charges">
            <thead>
            <tr>
                <th>Charge Name</th>
                <th>Description</th>
                <th>Type</th>
                <th>Model</th>
                <th>Unit Price</th>
                <th>QTY.</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="ProductChargeName"></td>
                <td class="ProductChargeDesc"></td>
                <td class="ProductChargeType"></td>
                <td class="ProductChargeModel"></td>
                <td class="ProductChargeUnitPrice"></td>
                <td class="ProductChargeQty"></td>
                <td class="ProductChargeTotal"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="HTML-Template ZuoraPaymentItem col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-right ">
        <table class="table table-hover table-bordered tbl-zuora-payments">
            <thead>
            <tr>
                <th>Date</th>
                <th>Payment Number</th>
                <th>Payment Applied</th>
                <th>Type</th>
                <th>Applied To</th>
                <th>Refunds</th>
                <th>Rateplan Name</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="PaymentDate"></td>
                <td class="PaymentNumber"></td>
                <td class="PaymentApplied"></td>
                <td class="PaymentType"></td>
                <td class="PaymentAppliedTo"></td>
                <td class="PaymentRefunds"></td>
                <td class="PaymentRateplanName"></td>
                <td class="PaymentStatus"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <script>

        <?php
        $Contacts = [];
        foreach ($ZuoraContacts as $ZuoraContact) {
            $Contacts[] = $ZuoraContact->getVar();
        }
        ?>
        var ZuoraContacts = <?php echo json_encode($Contacts); ?>;
        getZuoraAccountDetails(ZuoraContacts);
    </script>
    <?php
} else {
    echo "<h2 class='noZuoraAccount'>Oops, This Contact does not have Zuora Account.</h2>";
    if (isset($ZuoraContacts['Error'])) echo "<span class='codeblock'>$ZuoraContacts[Error]</span>";

}
?>