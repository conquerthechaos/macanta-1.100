<?php
$ConnectedInfosEncoded = macanta_get_config('connected_info');
if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = '[]';
$ConnectedInfos = json_decode($ConnectedInfosEncoded, true);
$ConnectorRelationship = $this->config->item('ConnectorRelationship');
$ConnectorRelationshipArr = json_decode($ConnectorRelationship, true);
?>
<div class="col-md-12 theNotePanel">
    <div class="row mainbox-top">
        <div class="col-md-12 tab-panel">
            <div class="panel panel-primary left-ConnectedInfos">
                <div class="panel-heading">

                    <h3 class="panel-title "><i class="fa fa-info-circle"></i> Connected Information Manager</h3>
                    <button type="button" class=" col-xs-1 btn btn-default  saveConnectedInfos"><i
                                class="fa fa-save"></i>
                        Save Connected Info
                    </button>
                </div>
                <div class="panel-body admin-panelBody ConnectedInfoContainer ">
                    <div class="col-md-4 ConnectedInfoListContainer">

                        <form method="post" class="form-horizontal ConnectedInfoList dynamic"
                              _lpchecked="1">
                            <ul class="itemContainer">

                                <div class="saved"></div>
                                <script>
                                    renderConnectedInfoTitle();
                                </script>
                                <div id="dummy"></div>
                                <li class="form-group-item remove-button">
                                    <div class="bullet-item">
                                        <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                        <button type="button" class=" col-xs-1 btn btn-default addButton ">Add Connected
                                            Info.
                                        </button>
                                    </div>

                                </li>

                            </ul>
                            <p class="note">Drag &amp; Drop the item above to rearrange them.</p>
                        </form>
                    </div>

                    <div class="col-md-8 ConnectedInfoSettingsContainer">


                        <div class="ContentHeader"></div>
                        <div class="add-field-container col-md-12">
                            <button type="button"
                                    class=" col-xs-1 btn btn-default  saveConnectedInfosB <?php if (sizeof($ConnectedInfos) == 0) echo "hideThis" ?>">
                                <i class="fa fa-save"></i>
                                Save Connected Info
                            </button>
                        </div>
                        <div class="ConnectedInfoSettingsContainerPlaceholder"><i class="glyphicon glyphicon-edit"></i>
                            Please Create Connected Information
                        </div>


                    </div>
                    <div class="col-md-12 footnote">
                        <!-- <strong>Available Shortcodes: </strong><br>
                         1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                         2. More Shortcodes coming soon!..-->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
    </div>
</div>
<!-- Connector Template -->
<div id="ConnectedInfoContentTemplate" class="ConnectedInfoContent container-fluid" style="display: none;">
    <div class="row">
        <div class="col-xs-12 text-right">
            <a href="javaScript:void(0)" onclick="downloadConnectorCSV()" class="downloadConnectorCSV" title="Download CSV File"></a>
        </div>
        <div class="col-md-12  no-pad-left no-pad-right">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad-left">
                <div class="form-group ">
                    <label class="control-label requiredField" for="ConnectedInfoTitle">
                        Information Title <span class="asteriskField"> * </span>
                    </label>
                    <input class="form-control ConnectedInfoTitle" name="ConnectedInfoTitle"
                           placeholder="Type in your Information Title" type="text"
                           value=""/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad-left no-pad-right">
                <div class="form-group ">
                    <label class="control-label requiredField" for="ConnectedInfoTitle">
                        Item ID Custom Field<span class="asteriskField">  </span>
                    </label>
                    <select name="ItemIdCustomField"
                            id="<?php echo macanta_generate_key('infusionsoftCustomField_', 5); ?>"
                            class=" selectpicker form-control ItemIdCustomField"
                            data-size="false">

                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label class="control-label" for="ConnectedInfoContent">
                    Fields
                </label>
                <ul id="connectorsTable" data-guid="">
                </ul>


            </div>
            <div class="form-group buttonContainer">
                <button type="button" class="btn btn-default addConnectorField"><i class="fa fa-plus-square"
                                                                                   aria-hidden="true"></i> <span>Add A Field</span>
                </button>
            </div>

            <div class="form-group RelationshipOptions">
                <label class="control-label control-label-fullwidth">
                    Relationship Options
                </label>
                <div class="col-md-4 no-pad-left">
                    <h3 class="col-lg-12 no-pad-left no-pad-right no-margin">All Relationships:</h3>
                    <div class="connector-all-relationships-container">

                        <ol class="connector-all-relationships">
                            <?php
                            foreach ($ConnectorRelationshipArr as $Relationship) {

                                ?>
                                <li class="field-details"
                                    title="<?php echo $Relationship['RelationshipDescription'] ?>">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="AllRelationship"
                                                   data-relationid="<?php echo $Relationship['Id'] ?>"
                                                   value="<?php echo $Relationship['RelationshipName'] ?>">
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span class="checkbox-label"><?php echo $Relationship['RelationshipName'] ?></span>
                                        </label>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>

                    </div>

                </div>
                <div class="col-md-8 no-pad-left">
                    <h3 class="col-lg-12 no-pad-left no-pad-right no-margin">Available Relationships:</h3>
                    <div class="connector-relationships-container">
                        <div class="connector-relationships-title col-lg-6 no-pad-left no-pad-right">Name</div>
                        <div class="connector-relationships-title col-lg-6 no-pad-left no-pad-right">Can have multiple
                            items?
                            <small>Leave limit blank for unlimited</small>
                        </div>
                        <form class="FormRelationship">
                            <ol class="connector-relationships">

                            </ol>
                        </form>

                    </div>

                </div>

            </div>

        </div>
    </div>
    <form>
        <div class="form-group checkboxGroup">
            <label class="control-label"  >
                Show this Connected Data Tab all the time, with no restriction by contact or user
            </label>
            <input type="radio"     name="connectedDataVisibility"
                   class="form-control  connectedDataVisibility" value="ShowCDToAll"  checked />
        </div>
        <div class="form-group checkboxGroup">
            <label class="control-label customTabPermission"  >
                Show this Connected Data Tab for all contacts, but only to specific users
            </label>
            <input type="radio" name="connectedDataVisibility"
                   class="form-control  connectedDataVisibility"  value="ShowCDToAllUserSpecific" />
        </div>
    </form>
</div>
<li class="HTML-Template relationship-option field-details">
    <div class="col-lg-5 no-pad-left">
        <h3 class="RelationshipName"></h3>
    </div>
    <div class="col-lg-7 no-pad-left no-pad-right">
        <label>
            <input class="multiple-options" type="radio" name="" value="yes" checked="">
            No
        </label>
        <label>
            <input class="multiple-options" type="radio" name="" value="no">
            Yes
        </label>
        <label>
            Limit
            <input maxlength="3" max="999" min="0" type="number" name="MultipleLimit" value=""
                   class="form-control multiple_limit" disabled="">

        </label>
    </div>
</li>
<!-- Field Template -->
<li class="field-item field-item-template">
    <div class="field-item">
        <i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true"></i>
        <i class="fa fa-plus-square-o AddFieldItem" aria-hidden="true"></i>
        <span class="field-label">
            <span class="label-title">Please Enter Label</span>
        <span class="cd-field-id">ID: </span>
        </span>
        <span class="field-properties">
                                        Type: <span class="field-type"></span><br>
                                        Custom Field: <span class="is-custom-field"></span>
                                    </span>
    </div>

    <form class="FormFieldDetails">
        <input type="hidden" name="fieldId" value="">
        <ul class="connectorsFieldDetails">
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Label</h3>
                    <small>This is the name which will appear in before the field.</small>
                </div>
                <div class="col-lg-7">
                    <input type="text" id="<?php echo macanta_generate_key('fieldLabel_', 5); ?>" name="fieldLabel"
                           class="form-control field-input" value="" title=""
                           required="required">
                </div>
            </li>

            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Type</h3>
                </div>
                <div class="col-lg-7">
                    <select name="fieldType" id="fieldType" class="form-control">
                        <option value=""> -- Select One --</option>
                        <optgroup label="Basic">
                            <option value="Text">Text</option>
                            <option value="TextArea">Text Area</option>
                            <option value="Number">Whole Number</option>
                            <option value="Currency">Decimal Number</option>
                            <option value="Email">Email</option>
                            <option value="Password">Password</option>
                        </optgroup>
                        <optgroup label="Choice">
                            <option value="Select">Select</option>
                            <option value="Checkbox">Checkbox</option>
                            <option value="Radio">Radio Button</option>
                        </optgroup>
                        <optgroup label="Other">
                            <option value="Date">Date Picker</option>
                            <option value="DateTime">Date/Time Picker</option>
                            <option value="URL">URL</option>
                            <option value="Repeater" disabled="disabled">Repeater (coming soon)</option>
                        </optgroup>
                        <?php
                        $SharedFlag=0;
                        foreach($ConnectedInfos as $ShKey=>$SharedConnector)
                        {
                            if($key!=$ShKey)
                            {
                                foreach($SharedConnector['fields'] as $SharedFields)
                                {
                                    if($SharedFields['useAsPrimaryKey']=='yes')
                                    {
                                        if($SharedFlag==0)
                                        {
                                            echo '<optgroup label="Data Reference">';
                                        }
                                        ?>
                                        <option <?php if($theField['fieldType'] == $SharedFields['fieldId']) echo "selected"; else echo ""; ?> value="<?php echo $SharedFields['fieldId'];?>">
                                            <?php echo $SharedConnector['title'];?> (<?php echo $SharedFields['fieldLabel'];?>)
                                        </option>
                                        <?php
                                        if($SharedFlag==0)
                                        {
                                            echo '</optgroup>';
                                            $SharedFlag=1;
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Data Reference?</h3>
                </div>
                <div class="col-lg-7">
                    <label>
                        <input type="radio" id="<?php echo macanta_generate_key('useAsPrimaryKey_', 5); ?>"
                               name="useAsPrimaryKey" value="no" checked>
                        No
                    </label>
                    <label>
                        <input type="radio" id="<?php echo macanta_generate_key('useAsPrimaryKey_', 5); ?>"
                               name="useAsPrimaryKey" value="yes" >
                        Yes
                    </label>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Required?</h3>
                </div>
                <div class="col-lg-7">
                    <label>
                        <input type="radio" name="requiredField" value="no" checked="checked">
                        No
                    </label>
                    <label>
                        <input type="radio"  name="requiredField" value="yes">
                        Yes
                    </label>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Contact Specific?</h3>
                </div>
                <div class="col-lg-7">
                    <label>
                        <input type="radio" class="contactspecificField"
                               name="contactspecificField" value="no"
                               checked="checked"
                        >
                        No
                    </label>
                    <label>
                        <input type="radio" class="contactspecificField"
                               name="contactspecificField" value="yes"
                        >
                        Yes
                    </label>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Default Value</h3>
                    <small>Predefined value for this field</small>
                </div>
                <div class="col-lg-7">
                    <input type="text" id="defaultValue" name="defaultValue"
                           class="form-control  field-input" value=""
                           title="" required="required">
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Place Holder Text</h3>
                    <small>Text to show when the field is blank</small>
                </div>
                <div class="col-lg-7">
                    <input type="text" id="placeHolder" name="placeHolder" class="form-control field-input"
                           value=""
                           title="" required="required">
                </div>
            </li>

            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Helper Text</h3>
                    <small>Some information regarding this field if needed.</small>
                </div>
                <div class="col-lg-7">
                    <div class="field-content">
                        <textarea name="helperText" onkeyup="textAreaAdjust()" class="form-control field-input helperText" required="required"></textarea>
                    </div>
                    <div class="preview_content" style="display:none;"></div>
                    <a class="fa fa-eye markdown_preview" aria-hidden="true" title="Preview"></a><a class="fa fa-edit undo_markdown_preview" aria-hidden="true" title="Write" style="display:none;"></a> <a href="https://www.markdownguide.org/cheat-sheet" target="_blank" class="helperResourceurl">Markdown Guide</a>
                </div>
            </li>

            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Infusionsoft Custom Field</h3>
                    <small>Field to be updated in Infusionsoft</small>
                </div>
                <div class="col-lg-7">
                    <select name="infusionsoftCustomField" id="infusionsoftCustomField" class=" form-control"
                            data-size="false">
                    </select>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Use As List Header</h3>
                </div>
                <div class="col-lg-7">
                    <label>
                        <input type="radio" name="showInTable" value="no" checked="checked">
                        No
                    </label>
                    <label>
                        <input type="radio" name="showInTable" value="yes">
                        Yes
                    </label>
                    <label>
                        <input type="number" name="showOrder" value="0" class="ordernumber">
                        Order
                    </label>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Section Tag ID</h3>
                    <small>Some information regarding this field if needed.</small>
                </div>
                <div class="col-lg-7">
                    <input type="text" id="sectionTagId" name="sectionTagId" class="form-control field-input"
                           value=""
                           title="" required="required"></div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Sub-Section</h3>
                    <small>Group-name to group fields inside sections.</small>
                </div>
                <div class="col-lg-7">
                    <input type="text" id="fieldSubGroup" name="fieldSubGroup" class="form-control field-input"
                           value=""
                           placeholder="Group A"
                           title=""></div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Add Divider After This Field</h3>
                </div>
                <div class="col-lg-7">
                    <label>
                        <input type="radio" name="addDivider" value="no" checked="checked">
                        No
                    </label>
                    <label>
                        <input type="radio" name="addDivider" value="yes">
                        Yes
                    </label>
                </div>
            </li>
            <li class="field-details">
                <div class="col-lg-5">
                    <h3>Add Heading Text Above This Field</h3>
                </div>
                <div class="col-lg-7">
                    <input type="text" name="fieldHeadingText" class="form-control field-input"
                           value=""
                           title="" >
                </div>
            </li>
        </ul>
    </form>
</li>
<!-- Choices Template-->
<div class="choices-container hidenTemplate">
    <h4 class="choices-label">Enter Choices
        <small> - seperated by new line</small>
    </h4>
    <textarea name="fieldChoices" id="fieldChoices" class="form-control"></textarea>
</div>
<input type="hidden" id="CurrentConnectorId"/>
<input type="hidden" id="CurrentConnectorDownLoadType" value=""/>
<?php
$ConnectorDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Delete Connected Info.\">
    <p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>This Connected Information will be removed from this contact.<br> Are you sure?</p>
</div>
";
$FieldDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Deleting Field.\">
    <p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>This Field will be deleted.<br> Are you sure?</p>
</div>
";
?>
<script>
    renderConnectedInfoSettingsContainer();
    $("select[name=infusionsoftCustomField]").on('change', function () {
        console.log('infusionsoftCustomField Changed');
        var value = $(this).val();
        var parentLi = $(this).parents("li.field-item");
        parentLi.find("div.field-item span.is-custom-field").html(value)
    });
</script>
<script>
    var previousValue = "";
    var ConnectorDeleteDialog = <?php echo json_encode($ConnectorDeleteDialog); ?>;
    var FieldDeleteDialog = <?php echo json_encode($FieldDeleteDialog); ?>;
    $('.ConnectedInfoContent').css('display', 'none');
    if (!$("form.ConnectedInfoList ul li:eq( 0 )").hasClass('remove-button')) {
        $('form.ConnectedInfoList ul li:eq( 0 ) span.ConnectedInfoListTitle').trigger('click');
    }
    initButtonAddField();
    initAddRelationship();
    $('button.saveConnectedInfos, button.saveConnectedInfosB').once('click', function () {
        $('button.saveConnectedInfos, button.saveConnectedInfosB').attr("disabled", true);
        saveConnectedInfos();
    });
    var txtSelectHTML = '<option value=""> -- Select One --</option>';
    $.each(ContactCustomFields, function (GroupName,CustomField) {
        txtSelectHTML += '<optgroup label="'+GroupName+'">';
        $.each(CustomField, function(Label ,Items){
            txtSelectHTML += '<option data-subtext="Type: '+Items.DataType+'" value="'+Items.Name+'" title="'+Items.Label+'">'+Items.Label+'</option>';
        });
        txtSelectHTML += '</optgroup>';
        $("#ConnectedInfoContentTemplate select[name=ItemIdCustomField]").html(txtSelectHTML);
        $(".field-item-template select#infusionsoftCustomField").html(txtSelectHTML);
    });
</script>
