<?php
$Logging = $this->config->item('IS_App_Name') == 'no' ? true:false;
$CurrentDir = dirname(__FILE__) . "/";
$ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')):[];
$availableRelationships = [];
foreach ($ConnectorRelationship as $RelationshipItem){
    $availableRelationships[$RelationshipItem->Id] = ["RelationshipName"=>$RelationshipItem->RelationshipName,"RelationshipDescription"=>$RelationshipItem->RelationshipDescription];
}
?>
<!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ConnectedInfoPanel">-->
    <div class="panel panel-primary theConnectedInfoPanel">
        <div class="panel-heading "><h3 class="panel-title NoteName"><i class="fa fa-info"></i> <?php echo $title;?></h3></div>
        <div class="panel-body theConnectedInfoPanelBody">
            <ul class="nav nav-tabs">

                <li class=" active"><a class="no-pad-left " href="#connectedDetails" data-toggle="tab" aria-expanded="true"> Connected <?php echo $title;?></a></li>

                <li class=""><a class="no-pad-left " href="#connectedContactNotes" data-toggle="tab" aria-expanded="false"> Connected Contact Notes</a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="connectedDetails">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-left no-pad-right">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tab-panel no-pad-right no-pad-left">

                            <div class="table-responsive">
                                <table id="UserConnectorInfoTable" data-guid="<?php echo $ConnectorTab['id'] ?>" class="UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?> table table-hover table-striped table-bordered <?php echo $ConnectorTab['id'] ?>" cellspacing="0" >
                                    <thead>
                                    <?php
                                    $Order = [];
                                    $OrderId = [];
                                    foreach ($ConnectorTab['fields'] as $field){
                                        if($field["showInTable"] == "yes"){
                                            $key = (int) $field["showOrder"];
                                            if(isset($Order[$key])){
                                                while (isset($order[$key])){
                                                    $key++;
                                                }
                                                $Order[$key] = $field["fieldLabel"];
                                                $OrderId[$key] = $field["fieldId"];
                                            }else{
                                                $Order[$key] = $field["fieldLabel"];
                                                $OrderId[$key] = $field["fieldId"];
                                            }
                                        }
                                    }
                                    ksort($Order);
                                    ksort($OrderId);
                                    ?>
                                    <tr>
                                        <?php
                                        if(sizeof($Order) > 0){
                                            foreach ($Order as $field){
                                                echo "<th>$field</th>";
                                            }
                                        }else{
                                            $count = 0;
                                            foreach ($ConnectorTab['fields'] as $field){
                                                $count++;
                                                $OrderId[] = $field["fieldId"];
                                                echo "<th>$field[fieldLabel]</th>";
                                                if($count == 3) break;
                                            }
                                        }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($UserValue as $itemId => $ValuesArr){
                                        $UserField = $ValuesArr['value'];
                                        $ConnectedContact = $ValuesArr['connected_contact'];
                                        foreach ($ConnectedContact as $ContactId => $ContactDetails){
                                            $this->db->select('Groups');
                                            $this->db->where('Id',$ContactId);
                                            $query = $this->db->get('InfusionsoftContact');
                                            $row = $query->row();
                                            if (isset($row))
                                            {
                                                $Groups = $row->Groups;
                                                if ($Groups != '' && checkContactRestriction($Groups,$session_data)) unset($ConnectedContact[$ContactId]); // skip this contact when restricted
                                            }
                                        }
                                    /*if($this->config->item('IS_App_Name') == 'tr410' || $this->config->item('IS_App_Name') == 'qj311-dm' || $this->config->item('IS_App_Name') == 'martinneely' || $this->config->item('IS_App_Name') == 'staging')
                                    {*/
                                        echo "<tr class='infoItem' data-itemid='$itemId'>";
                                    /*}else{
                                        echo "<tr class='infoItem' data-itemid='$itemId' data-raw ='".str_replace("=","",base64_encode(json_encode($UserField)))."'  data-connectedcontacts ='".str_replace("=","",base64_encode(json_encode($ConnectedContact)))."'>";
                                    }*/

                                        $ShowItemId = true;
                                        foreach ($OrderId as $fieldName){
                                            $ItemId = '';
                                            foreach ($ConnectorTab['fields'] as $field){
                                                if($field['fieldId'] == $fieldName){
                                                    $Val = $field['defaultValue'];
                                                    break;
                                                }
                                            }
                                            if(isset($UserField[$fieldName])){
                                                if(is_array($UserField[$fieldName])){
                                                    $Val = isset($UserField[$fieldName]["id_".$ContactInfo->Id]) ? $UserField[$fieldName]["id_".$ContactInfo->Id]:$Val;
                                                }else{
                                                    $Val = $UserField[$fieldName];
                                                }
                                            }
                                            if($ShowItemId) $ItemId = '<div class="cd-item-id">ID:<span>'.$itemId.'</span></div>';
                                            echo "<td>$Val $ItemId</td>";
                                            $ShowItemId = false;
                                        }
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="col-xs-12 col-sm-12 col-md-5 col-lg-5  btn btn-default  link-connect-data" >
                                <i class="fa fa-plus-square-o"></i> Connect Existing Data
                            </button>
                            <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="col-xs-12 col-sm-12 col-md-5 col-lg-5  btn btn-default  addUserConnectedInfo">
                                <i class="fa fa-plus-square-o"></i> Add New <?php echo ucfirst($titleSingular) ?>
                            </button>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 marginTop no-pad-left no-pad-right ">
                        <h2 class="oppmainlabel">Information Details</h2>
                        <div  class="col-md-12 no-pad-left no-pad-right InformationDetailsContainer" >
                            <form class="FormUserConnectedInfo" data-itemid="" data-guid="<?php echo $ConnectorTab['id'] ?>">

                                <?php

                                $Sections = [];
                                $AlltagDetails = infusionsoft_get_tags();
                                $tagsArr = explode(',',$session_data['groups']);
                                $ViewedContactTagArr = explode(',',$ContactInfo->Groups);
                                echo "<!--";
                                print_r($ViewedContactTagArr);
                                echo "-->";
                                $TimeStarted = time();
                                foreach ($ConnectorTab['fields'] as $field){
                                    $tagIdValue = trim($field['sectionTagId']);
                                    if($tagIdValue == ''){
                                        $Sections['General'][] = $field;
                                    }else{
                                        $key = array_search($tagIdValue, array_column($AlltagDetails, 'Id'));
                                        $theTag[0] = $AlltagDetails[$key];
                                        $tagDetails = $theTag;
                                        if($session_data['userlevel'] == "administrator"){
                                            $Sections[$tagDetails[0]->GroupName][] = $field;
                                        }else{
                                            if(in_array($tagIdValue,$tagsArr)  || in_array($tagIdValue,$ViewedContactTagArr)) {
                                                if(in_array($tagIdValue,$tagsArr)  && in_array($tagIdValue,$ViewedContactTagArr)){
                                                    $Sections[$tagDetails[0]->GroupName][] = $field;
                                                }
                                            }else{
                                                $Sections[$tagDetails[0]->GroupName][] = $field;
                                            }
                                        }
                                    }
                                }
                                $TimeEnded = time();
                                $TimeLapse = $TimeEnded - $TimeStarted;
                                if($Logging)
                                    file_put_contents($CurrentDir . "_TabConnectedInfoTest_".$this->config->item('IS_App_Name').".txt", "foreach (ConnectorTab['fields']: ".$TimeLapse."\n\n" , FILE_APPEND);

                                ksort($Sections, SORT_NUMERIC);
                                if(isset($Sections['General']) && sizeof($Sections['General']) > 0){
                                    $Uncategorized = $Sections['General'];
                                    echo "<fieldset class='cd-fields collapsible CDSection Lazy-GeneralSection-_-".$ConnectorTab['id']." GeneralSection' data-group='".$ConnectorTab['id']."' data-sectionname='General' data-section='GeneralSection-_-".$ConnectorTab['id']."'>";
                                    echo "<legend>General</legend> <div class=\"form-group \" style='height: 100px;width: 100%'> </div>";

                                    echo "</fieldset>";
                                }
                                unset($Sections['General']);
                                //file_put_contents(dirname(__FILE__) . "/" . "_Sections_".$this->config->item('IS_App_Name').".txt", print_r($Sections, true));

                                foreach ($Sections as $SectionName=>$theFields){
                                    if(sizeof($theFields) == 0) continue;
                                    $Search = ['&',' ','$','/','\\'];
                                    $className = str_replace($Search,"",$SectionName);
                                    echo "<fieldset class='cd-fields collapsible CDSection collapsed Lazy-{$className}Section-_-".$ConnectorTab['id']." {$className}Section' data-group='".$ConnectorTab['id']."' data-sectionname='{$SectionName}' data-section='{$className}Section-_-".$ConnectorTab['id']."'>";
                                    echo "<legend>$SectionName</legend><div class=\"form-group \" style='height: 100px;width: 100%'> </div>";
                                    echo "</fieldset>";
                                }

                                ?>
                                <div class="col-md-12 no-pad-right container-relationships">
                                    <label><?php echo $ContactInfo->FirstName." ".$ContactInfo->LastName ?>'s Relationship</label>
                                    <div class="col-md-12 no-pad-right no-pad-left displayed-contact-relationship">
                                        <select
                                                data-contactid="<?php echo $ContactInfo->Id; ?>"
                                                class="multiselect-ui field-relationships form-control ConnectedContactRelationships ConnectedContactRelationships<?php echo $ContactInfo->Id; ?> addGroup"
                                                multiple="multiple"
                                                onchange="get_multi_select_values(this)"
                                                required
                                                disabled
                                        >
                                            <?php
                                            $ConnectorFields = macanta_get_config('connected_info');
                                            $ConnectorFields = $ConnectorFields ? json_decode($ConnectorFields, true):[];
                                            $RelationshipRules = [];
                                            foreach ($ConnectorFields as $FieldGroups){
                                                $RelationshipRules[$FieldGroups['id']] = $FieldGroups['relationships'];
                                            }
                                            $Allowed = [];
                                            foreach ($RelationshipRules[$ConnectorTab['id']] as $theRules){
                                                $Allowed[] = $theRules['Id'];
                                            }
                                            foreach ($availableRelationships as $Id=>$availableRelationship){
                                                if(in_array($Id,$Allowed))
                                                    echo '<option value="'.$Id.'" >'.$availableRelationship["RelationshipName"].'</option>';
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12 no-pad-right container-relationships">
                                    <label >Other Related Contacts</label>
                                    <ul class="relationship-list">

                                    </ul>
                                    <div class="col-md-12 no-pad-right">
                                        <button type="button" class="addGroup link-connect-contact" disabled> <i class="fa fa-plus-square-o"></i> Connect Other Contact </button>
                                    </div>

                                </div>


                                <div class="col-md-12 no-pad-right container-file-attachment-container container-file-attachment-container-<?php echo $ConnectorTab['id'] ?>">
                                    <div class="col-md-12 no-pad-right no-pad-left container-file-attachments">
                                        <label >File Attachments</label>
                                        <ul class="file-attachments-list">

                                        </ul>

                                        <input  data-guid="<?php echo $ConnectorTab['id'] ?>"   id="CDFileAttachments<?php echo $ConnectorTab['id'] ?>" class="CDFileAttachments<?php echo $ConnectorTab['id'] ?> CDFileAttachments btn btn-default" name="CDFileAttachments" type="file" disabled>

                                    </div>
                                </div>
                                <div class="col-lg-12 no-pad-right container-url-attachment-container">
                                    <div class="input-group" >
                                        <input type="text" class="form-control url-attachment" placeholder="Attach File By URL.." disabled>
                                        <span class="input-group-btn">
                                <a class="btn btn-default url-attachment-btn" disabled ><i class="fa fa-file-o"></i>   Attach File</a>
                              </span>
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->


                                <div class="col-md-12 no-pad-left no-pad-right">
                                    <input type="reset" style="display: none">
                                    <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="addGroup btn btn-danger deleteUserConnectedInfo " disabled>
                                        <i class="fa fa-chain-broken"></i> Unlink <?php echo $ContactInfo->FirstName ?>
                                    </button>
                                    <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="addGroup btn btn-warning cancelUserConnectedInfo hideThis" >
                                        <i class="fa fa-times"></i> Cancel
                                    </button>
                                    <button type="submit" data-guid="<?php echo $ConnectorTab['id'] ?>" class="addGroup btn btn-default saveUserConnectedInfo " disabled>
                                        <i class="fa fa-floppy-o"></i> Save
                                    </button>
                                </div>

                            </form>
                        </div>


                    </div>
                </div>
                <div class="tab-pane" id="connectedContactNotes">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-right no-pad-left marginTop-15 ConnectedContactsNoteContainer ConnectedContactsNoteContainer<?php echo $ConnectorTab['id'] ?>">
                        <table id="ConnectedContactsNote<?php echo $ConnectorTab['id'] ?>" class="ConnectedContactsNote table table-striped table-bordered" width="98%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Contact</th>
                                <th>Type</th>
                                <th>Title</th>
                                <th>Description</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>



        </div>
    </div>
<!--</div>-->
<script>
    $(document).ready(function() {

        $("fieldset.collapsible").collapsible({
            animation: true,
            speed: "medium",
            expanded: function ($fieldset) {
                if($fieldset.hasClass('section-loaded') === false){
                    console.log('Expanded');
                    console.log($fieldset.attr('data-sectionname'));
                    console.log($fieldset.attr('data-section'));
                    console.log($fieldset.attr('data-group'));
                    var theSection = $fieldset.attr('data-section');
                    var theSectionName = $fieldset.attr('data-sectionname');
                    lazy_load(theSection,theSectionName,"load_cd_section","core/tabs");
                }
                $fieldset.addClass('section-loaded');
            }
        });
        $("select.ConnectedContactRelationships").multiselect({
            numberDisplayed: 5,
            maxHeight: 200,
            allSelectedText:false
        });
        UserConnectorInfoTable['<?php echo $ConnectorTab['id'] ?>'] = $(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?>").DataTable( {
            "order": [[ 0, "desc" ]],
            "pageLength": 5,
            "paging":   true,
            "searching":   true,
            "destroy": true,
            "info":     false,
            "createdRow": function ( row, data, index ) {
                //$(row).attr('data-contactid',data[3]);
            }
        } )
            .on( 'page.dt', function () {
                $(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?> tr").removeClass('activeItem');
                console.log('Page Changed.');
            } )
            .on( 'draw', function () {
                var theForm = $("form.FormUserConnectedInfo[data-guid=<?php echo $ConnectorTab['id'] ?>]");
                var theItemId = theForm.attr('data-itemid');
                if(theItemId)
                    $(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?> tr[data-itemid="+theItemId+"]").addClass('activeItem');
            } );

        //makeCDListDataTable(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?>");
        FileAttachmentsIt('container-file-attachment-container-<?php echo $ConnectorTab['id'] ?>');
        if (typeof CustomTabFnCallback !== "undefined") {
            console.log("Calling CustomTabFnCallback();");
            CustomTabFnCallback();
        }

    } );
</script>