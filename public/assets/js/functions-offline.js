
function macantaSync(theTable) {
    if(theTable === 'TableAll'){
       $.each(AllTables, function (index,TableName) {
           macantaSync(TableName);
       })
    }else{
        let jsonData = {"controler":"offline","action":"initialize_sync","session_name":session_name,"data":{"table":theTable}};
        let successFn = function(e){};
        let errorFn = function(e){};
        let theButton = $(".tr-"+theTable).find("button.btn-sync."+theTable);
        if(!theButton.is(":disabled")){
            $(".tr-"+theTable).addClass("loading-overlay loading-center");
            $(".tr-"+theTable+" td").css("opacity", 0.1);
            ajaxRequester(jsonData, successFn, errorFn);
        }
    }

}

function monitorSync() {
    let jsonData = {"controler":"offline","action":"monitorSync","session_name":session_name,"data":{}};
    let successFn = function(e){
        $.each(e,function (tblName,Details) {
            let theTR =  $("tr.tr-"+tblName);
            let progressbar = $( "#progressbar", theTR ),
                progressLabel = $( ".progress-label", theTR );
            let local_count = '';
            if(Details.local_count){
                local_count = "<span style='color: red;'> *"+Details.local_count+"<span>";
            }
            $("span.item-count", theTR).html("<small>[ "+Details.count+" ] "+local_count+"<small> ");
            if(Details.details && Details.details !== 0){
                $(".tr-"+tblName).removeClass("loading-overlay loading-center");
                $(".tr-"+tblName+" td").css("opacity", 1);
                $("td.Operation", theTR).html(Details.details.Operation);
                $("td.Status", theTR).html(Details.details.Status);
                $("button.btn-sync."+tblName, theTR).prop('disabled', true);
                $("button.btn-abort."+tblName, theTR).prop('disabled', false);
                let theVal = (parseInt(Details.details.CurrentItem)/parseInt(Details.details.TotalItems)) * 100;
                progressbar.removeClass("hideThis");
                progressbar.progressbar();
                progressbar.progressbar( "value", Math.ceil(theVal) );
                progressLabel.text( progressbar.progressbar( "value" ) + "%" );
                if(theVal === 100){
                    $("button.btn-sync."+tblName, theTR).prop('disabled', false);
                    $("button.btn-abort."+tblName, theTR).prop('disabled', true);
                }

            }else{
                $(".tr-"+tblName).removeClass("loading-overlay loading-center");
                $(".tr-"+tblName+" td").css("opacity", 1);
                progressbar.addClass("hideThis");
                $("td.Operation", theTR).html("-");
                $("td.Status", theTR).html('Standby');
                $("button.btn-sync."+tblName, theTR).prop('disabled', false);
                $("button.btn-abort."+tblName, theTR).prop('disabled', true);
            }

        })
    };
    let errorFn = function(e){};
    if(AjaxRequests && AjaxRequests.readyState != 4){
        console.log('waiting..')
    }else{
        AjaxRequests =  ajaxRequester(jsonData, successFn, errorFn);
    }
}
function ajaxRequester(jsonData, successFn, errorFn){
    //var jsonData = {"controler":"core/common","action":"getWebform","data":{"app":app, "formid":formid}};
    return $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e)
        {
            successFn(e);
        },
        error: function (e) {
            errorFn(e);
        }

    });

}